import copy
import functools
import logging
import multiprocessing
import os
import sys
import webbrowser

import numpy as np
from PySide2 import QtCore
from PySide2.QtCore import QThread, QTranslator, Qt
from PySide2.QtWidgets import QMainWindow, QFileDialog, QAction, QMessageBox, QSplitter, \
    QApplication, QInputDialog

from mefap.gui.license_dialog import LicenseDialog
# from mefap.evaluation.noise_layouts import NoisePropagationLayout
from mefap.evaluation.noise_propagation.factory_wall_corner_cells import wall_and_corner_cells
from mefap.evaluation.quamfab_workflow import quamfab_workflow
from mefap.factory.factory_model import FactoryModel
from mefap.factory.reset_layout import reset_layout
from mefap.gui.calculation_output import CalculationOutput
from mefap.gui.collapsible_dialog import CollapsibleDialog
from mefap.gui.compare_dialog import CompareDialog
from mefap.gui.evaluation_dialog import EvaluationDialog
from mefap.gui.extended_settings_dialog import ExtendedSettingsDialog
from mefap.gui.facility_dialog import FacilityDialog
from mefap.gui.factory_all_layouts_dialog import FactoryAllLayoutsDialog
from mefap.gui.factory_change_size_dialog import FactoryChangeSizeDialog
from mefap.gui.factory_scene_layout import FactoryLayoutScene
from mefap.gui.factory_set_size_dialog import FactorySetSizeDialog
from mefap.gui.factory_size_dialog import FactorySizeDialog
from mefap.gui.factory_table import FactoryTable
from mefap.gui.factory_view import FactoryView
from mefap.gui.generated.MeFaPMainView import Ui_main_window
from mefap.gui.optimize_dialog import OptimizeDialog
from mefap.gui.start_dialog import StartDialog
from mefap.gui.util.calculation_output_reciever import CalculationOutputReceiver
from mefap.gui.util.gui_multi_proc import start_multi_p, save_multi_result
from mefap.gui.util.write_stream import WriteStream
from mefap.gui.weight_dialog import WeightDialog
from mefap.optimization.deletePath import delete_all_paths
from mefap.optimization.positionPath import try_path_planning_for_layout
from mefap.xlparser.xlInputParser import parse_xl


class MainView(QMainWindow):
    """Main window of program"""

    def __init__(self, translator) -> None:
        super().__init__()
        # initialise logging
        logging.basicConfig(level=logging.INFO)

        self.started = False
        # loading Ui
        self.ui = Ui_main_window()
        self.ui.setupUi(self)
        self.translator: QTranslator = translator

        # setting up factory
        self.factory = FactoryModel()

        # initialise factory visualisation
        self.factory_scene = FactoryLayoutScene(self.factory, parent=self)

        self.graphic_view_factory = FactoryView(self)
        self.sidebar = CollapsibleDialog(self.factory)
        self.splitter = QSplitter(parent=self.ui.widget_2)
        self.ui.horizontal_layout.addWidget(self.splitter)
        self.splitter.addWidget(self.sidebar)
        self.splitter.addWidget(self.graphic_view_factory)
        self.splitter.setSizes([100, 300])
        self.graphic_view_factory.setScene(self.factory_scene)

        # gives function to menu bar
        # factory data
        self.ui.action_machinery.triggered.connect(self.open_machinerydialog)
        self.ui.action_data_cf.triggered.connect(self.open_cf_flow_dialog)
        self.ui.action_data_mf.triggered.connect(self.open_mf_flow_dialog)
        self.ui.action_media_requirement.triggered.connect(self.open_media_table_dialog)
        self.ui.action_enivroment.triggered.connect(self.open_environmental_table_dialog)
        # factory size
        self.ui.action_excluded_areas.triggered.connect(self.open_change_excluded_area)
        self.ui.action_factory_size.triggered.connect(self.open_change_size)
        # optimize
        self.ui.action_weighting.triggered.connect(self.open_weightingdialog)
        self.ui.action_optimize.triggered.connect(self.open_optimizedialog)
        # menu
        self.ui.action_new_layout.triggered.connect(self.open_factorysizedialog)
        self.ui.action_load_layout.triggered.connect(self.load_file)
        self.ui.action_save.triggered.connect(self.save_file)
        self.ui.action_save_as.triggered.connect(self.save_file_as)
        self.ui.action_import_excel.triggered.connect(self.import_excel)
        self.ui.action_import_excel.setEnabled(False)
        self.ui.action_result.triggered.connect(self.open_eval_table)
        self.ui.action_result.setEnabled(False)
        self.ui.action_comparison.triggered.connect(self.open_comparison_view)
        self.ui.action_comparison.setEnabled(False)
        self.ui.action_extended_settings.triggered.connect(self.open_extended_settings)
        self.ui.action_all_layouts.setEnabled(False)
        self.ui.action_all_layouts.triggered.connect(self.open_all_layouts_dialog)
        self.ui.action_help.triggered.connect(self.open_helpdialog)
        self.ui.action_license.triggered.connect(self.open_license)

        self.ui.push_button_optimize.clicked.connect(self.optimize)
        self.ui.push_button_evaluate.clicked.connect(self.evaluate)

        self.ui.pushButton_reset.clicked.connect(self.reset_layout)
        self.ui.pushButton_recalculate_paths.clicked.connect(self.recalculate_paths)

        # self.ui.action_english.triggered.connect(functools.partial(self.change_language, 'mefap/resources/i18n/en-GB'))
        # self.ui.action_german.triggered.connect(functools.partial(self.change_language, 'mefap/resources/i18n/de-DE'))

        # File path for saving files
        self.file_path = ""

    def open_startdialog(self) -> None:
        """
        Opens the StartDialog
        :return: None
        """
        logging.info("Open StartDialog")
        start_dialog = StartDialog(self)
        start_dialog.setModal(True)
        start_dialog.show()
        result = start_dialog.exec_()
        if result:
            pass
        else:
            logging.info("Exit from StartDialog")
            sys.exit()

    def open_license(self) -> int:
        logging.info("Opening License Dialog")
        license_dialog = LicenseDialog()
        license_dialog.setModal(True)
        license_dialog.show()
        return license_dialog.exec_()

    def open_weightingdialog(self, calc=False) -> int:
        """
        Opens Dialog for weighting factors for optimization.py.
        :return: None
        """
        logging.info("Opening Weighting Dialog")
        weight_dialog = WeightDialog(self.factory, calc=calc)
        weight_dialog.setModal(True)
        weight_dialog.show()
        return weight_dialog.exec_()

    def open_helpdialog(self) -> None:
        """
        Opens a manual for the program.
        :return: None
        """
        logging.info("Opening Help")
        application_path = os.path.dirname(os.path.abspath(__file__))
        webbrowser.open_new('file://' + os.path.join(application_path, 'mefap/resources/MeFaP_Userguide.pdf'))

    def open_factorysizedialog(self, parentdialog=None, new=True) -> None:
        """
        Opens Dialog to get factory size of the new factory layout
        :param parentdialog: Another Dialog where this method is called from
        :return: None
        """
        self.deinit_controls()
        old_fac = self.factory
        if new:
            self.factory = FactoryModel()
        logging.info("Opening FactorySizeDialog")
        size_dialog = FactorySizeDialog(self.factory)
        size_dialog.setModal(True)
        size_dialog.show()
        result = size_dialog.exec_()
        logging.info("FactorySizeDialog closed")
        if result:
            if parentdialog:
                parentdialog.accept()
            self.factory.initialize_content()
            self.factory_scene = FactoryLayoutScene(self.factory, parent=self)
            self.graphic_view_factory.setScene(self.factory_scene)
            self.graphic_view_factory.fitInView(0, 0, self.factory.layout.shape[
                1] * self.factory.cell_zoom, self.factory.layout.shape[
                                                    0] * self.factory.cell_zoom,
                                                Qt.KeepAspectRatio)
            self.open_factorysetsizedialog()
        else:
            if not new:
                self.open_startdialog()
            else:
                self.factory = old_fac
                self.init_controls()

    def open_factorysetsizedialog(self) -> None:
        """
        Opens Dialog to set remove squares from factory
        :return: None
        """
        logging.info("Opening FactorySetSizeDialog")
        set_size_dialog = FactorySetSizeDialog(self.factory)
        set_size_dialog.setModal(True)
        set_size_dialog.show()
        set_size_dialog.fit_view()
        result = set_size_dialog.exec_()
        if result:
            if self.open_excel_ask() == QMessageBox.Yes:
                self.load_excel()
            else:
                self.first_init()
        else:
            self.factory.reset_rows_columns()
            self.open_factorysizedialog(new=False)

    def open_excel_ask(self) -> None:
        """
        Opens a dialog where the user gets asked if he want to load date from excel.
        :return: None
        """
        ask_box = QMessageBox()
        ask_box.setModal(True)
        ask_box.setText(self.tr("Do you want to load data from an Excel sheet?"))
        ask_box.setStandardButtons(QMessageBox.Yes | QMessageBox.No)
        ask_box.setDefaultButton(QMessageBox.Yes)
        return ask_box.exec_()

    def open_optimizedialog(self, calc=False) -> int:
        """
        Opens Dialog to set remove squares from factory
        :return: None
        """
        logging.info("Opening OptimizeDialog")
        optimize_dialog = OptimizeDialog(self.factory, calc=calc)
        optimize_dialog.setModal(True)
        optimize_dialog.show()
        return optimize_dialog.exec_()

    def open_mf_flow_dialog(self) -> None:
        """
        Opens Dialog which shows the material flow matrix
        :return: None
        """
        logging.info("Opening MaterialFlowDialog")
        mf_dialog = FactoryTable(self.factory, self)
        mf_dialog.setModal(True)
        mf_dialog.ui.tab_widget.setCurrentIndex(0)
        mf_dialog.show()
        if mf_dialog.exec_():
            self.refresh()
            self.factory_scene.show_layout()

    def open_cf_flow_dialog(self) -> None:
        """
        Opens Dialog which shows the communication flow matrix
        :return: None
        """
        logging.info("Opening CommunicationFlowDialog")
        cf_dialog = FactoryTable(self.factory, self)
        cf_dialog.setModal(True)
        cf_dialog.ui.tab_widget.setCurrentIndex(1)
        cf_dialog.show()
        if cf_dialog.exec_():
            self.refresh()
            self.factory_scene.show_layout()

    def open_media_table_dialog(self) -> None:
        logging.info("Opening MediaTableDialog")
        cf_dialog = FactoryTable(self.factory, self)
        cf_dialog.setModal(True)
        cf_dialog.ui.tab_widget.setCurrentIndex(3)
        cf_dialog.show()
        if cf_dialog.exec_():
            self.refresh()
            self.factory_scene.show_layout()

    def open_environmental_table_dialog(self) -> None:
        logging.info("Opening EnvironmentalTableDialog")
        cf_dialog = FactoryTable(self.factory, self)
        cf_dialog.setModal(True)
        cf_dialog.ui.tab_widget.setCurrentIndex(4)
        cf_dialog.show()
        if cf_dialog.exec_():
            self.refresh()
            self.factory_scene.show_layout()

    def open_machinerydialog(self) -> None:
        """
        Opens Dialog which shows the communication flow matrix
        :return: None
        """
        logging.info("Opening MachineryDialog")
        machinery_dialog = FactoryTable(self.factory, self)
        machinery_dialog.setModal(True)
        machinery_dialog.ui.tab_widget.setCurrentIndex(2)
        machinery_dialog.show()
        if machinery_dialog.exec_():
            self.refresh()
            self.factory_scene.show_layout()

    def open_eval_table(self) -> None:
        logging.info("Opening Evaluation Table")
        eval_dialog = EvaluationDialog(self.factory.quamfab, self.factory.quamfab_min,
                                       self.factory.weighting)
        eval_dialog.setModal(True)
        eval_dialog.show()
        eval_dialog.exec_()

    def open_all_layouts_dialog(self) -> None:
        logging.info("Opening AllLayoutsDialog")
        all_layouts_dialog = FactoryAllLayoutsDialog(self.factory)
        all_layouts_dialog.setModal(True)
        all_layouts_dialog.show()
        if all_layouts_dialog.exec_():
            self.refresh()
            self.factory_scene.show_layout()

    def open_change_excluded_area(self) -> None:
        """
        Opens a dialog where the user can change the excluded area's.
        :return: None
        """
        logging.info("Changing excluded area")
        set_size_dialog = FactorySetSizeDialog(copy.deepcopy(self.factory))
        set_size_dialog.setModal(True)
        set_size_dialog.show()
        set_size_dialog.fit_view()
        if set_size_dialog.exec_():
            self.factory.layout = set_size_dialog.factory.layout
            self.factory.facility_list = set_size_dialog.factory.facility_list
            self.factory.path_list = set_size_dialog.factory.path_list
            try_path_planning_for_layout(self.factory, True)
            self.refresh()
            self.factory_scene.show_layout()

    def open_change_size(self) -> None:
        """
        Opens a dialog where the user can change the size of the factory.
        :return: None
        """
        logging.info("Opening change factory size dialog")
        change_size_dialog = FactoryChangeSizeDialog(self.factory)
        change_size_dialog.setModal(True)
        change_size_dialog.show()
        if change_size_dialog.exec_():
            x = int((change_size_dialog.ui.spinBox_factory_width.value() - (
                    change_size_dialog.ui.spinBox_factory_width.value() % self.factory.cell_size)) / self.factory.cell_size)
            y = int((change_size_dialog.ui.spinBox_factory_length.value() - (
                    change_size_dialog.ui.spinBox_factory_length.value() % self.factory.cell_size)) / self.factory.cell_size)
            if x < self.factory.rows or y < self.factory.columns:
                errordialog = QMessageBox(self)
                errordialog.setStandardButtons(QMessageBox.StandardButtons(QMessageBox.Ok | QMessageBox.Cancel))
                errordialog.setText(self.tr("The new factory size is smaller than the previous one!"))
                errordialog.setInformativeText(
                    self.tr("Some facilities may get deleted.\nAll previously optimised layouts will get deleted!"))
                errordialog.setIcon(QMessageBox.Warning)
                if errordialog.exec_() == QMessageBox.Ok:
                    self.factory.delete_all_from_layouts_data()
                    self.ui.action_result.setEnabled(False)
                    self.ui.action_comparison.setEnabled(False)
                    self.ui.action_all_layouts.setEnabled(False)
                    self.factory.resize_factory(x, y)
                    self.refresh()
                    self.factory_scene.show_layout()
            else:
                if x != self.factory.rows or y != self.factory.columns:
                    errordialog = QMessageBox(self)
                    errordialog.setStandardButtons(QMessageBox.StandardButtons(QMessageBox.Ok | QMessageBox.Cancel))
                    errordialog.setText(self.tr("The new factory size differs from the the previous one!"))
                    errordialog.setInformativeText(self.tr("All previously optimised layouts will get deleted!"))
                    errordialog.setIcon(QMessageBox.Warning)
                    if errordialog.exec_() == QMessageBox.Ok:
                        self.factory.delete_all_from_layouts_data()
                        self.ui.action_result.setEnabled(False)
                        self.ui.action_comparison.setEnabled(False)
                        self.ui.action_all_layouts.setEnabled(False)
                        self.factory.resize_factory(x, y)
                        self.refresh()
                        self.factory_scene.show_layout()

    def refresh(self) -> None:
        self.factory_scene.reset()
        self.sidebar.update_sidebar()
        self.factory_scene.init_grid()
        self.factory_scene.load_facilities()
        self.factory_scene.show_layout()
        self.deinit_controls()
        self.init_controls()
        self.sidebar.set_facilities_table(self.factory_scene.facilities)

    def open_comparison_view(self) -> None:
        logging.info("Opening Comparison View")
        c = CompareDialog(self.factory)
        c.showMaximized()
        c.fit_view()
        c.exec_()
        self.refresh()

    def open_extended_settings(self) -> None:
        logging.info("Opening Extended Settings")
        settings = ExtendedSettingsDialog(self.factory)
        settings.setModal(True)
        settings.show()
        settings.exec_()
        self.refresh()

    def init_controls(self) -> None:
        """
        This method initialising controls elements. E.g. for showing
        media availability or restrictions.
        :return: None
        """
        logging.info("Init controls")
        self.sidebar.factory = self.factory
        # self.sidebar.set_facilities_table(self.factory_scene.facilities)
        self.sidebar.update_sidebar()
        # layout
        self.ui.action_standard.triggered.connect(self.factory_scene.show_layout)
        # info
        self.ui.action_cell_characteristics.triggered.connect(
            self.factory_scene.show_cell_characteristics)
        # light
        self.ui.action_light.triggered.connect(self.factory_scene.show_light)
        # areas
        self.ui.action_restrictive_area.triggered.connect(self.factory_scene.show_restrictive_area)
        # mf & cf
        self.ui.action_mf.triggered.connect(self.factory_scene.show_mf)
        self.ui.action_cf.triggered.connect(self.factory_scene.show_cf)
        for media in self.factory.media_availability:
            action = QAction(media.name, self.factory_scene)
            action.triggered.connect(functools.partial(
                self.factory_scene.show_media, media))
            self.ui.menu_views.addAction(action)
        if self.factory.restrictions:
            self.ui.action_height_profile.triggered.connect(
                functools.partial(self.factory_scene.show_restriction,
                                  self.factory.restrictions[2]))
            self.ui.action_cover_capacity.triggered.connect(
                functools.partial(self.factory_scene.show_restriction,
                                  self.factory.restrictions[1]))
            self.ui.action_floor_capacity.triggered.connect(
                functools.partial(self.factory_scene.show_restriction,
                                  self.factory.restrictions[0]))
            self.ui.action_noise_matrix.triggered.connect(self.show_noise_matrix)
        if self.factory:
            self.ui.action_import_excel.setEnabled(True)
        if self.factory.quamfab.weight_sum:
            self.ui.action_result.setEnabled(True)
            self.ui.action_comparison.setEnabled(True)
            self.ui.action_all_layouts.setEnabled(True)

    def show_noise_matrix(self) -> None:
        """
        Show noise scene and activating noise legend.
        :return: None
        """
        if self.factory_scene:
            self.factory_scene.show_noise_matrix()
            self.sidebar.add_noise_legend()

    def deinit_controls(self) -> None:
        """
        If a new factory is loaded or created the old controls(from init_controls) need
        to deinitialised, because the functionalityy is connect with the old factory.
        :return: None
        """
        logging.info("Deinit Controls")
        self.sidebar.clear_facilities_table()
        self.sidebar.clear_area_table()
        action: QAction
        for action in reversed(self.ui.menu_views.actions()):
            if action.text() == "":
                break
            self.ui.menu_views.removeAction(action)
        self.ui.action_result.setEnabled(False)
        self.ui.action_comparison.setEnabled(False)
        self.ui.action_all_layouts.setEnabled(False)
        self.ui.action_import_excel.setEnabled(False)

    def load_excel(self) -> None:
        """
        Opens file dialog for loading the excel file with information about the factory and
        opens a Dialog with all parsed information. Furthermore it fills the ressource list
        at the main window.
        :return: None
        """
        fname = QFileDialog.getOpenFileName(self, 'Open File', '.', "Excel files (*.xlsm)")
        if fname[0]:
            logging.info("Load File {}".format(fname[0]))
            parse_xl(fname[0], self.factory)
            logging.info("Excel parsed successful.")
            logging.info(self.factory)
            factory_table_cf = FactoryTable(self.factory, self, on_start=True)
            factory_table_cf.show()
            factory_table_cf.ui.tab_widget.setCurrentIndex(2)
            result = factory_table_cf.exec_()
            if result:
                self.first_init()
        else:
            logging.info("FileDialog closed")
            self.open_factorysetsizedialog()

    def first_init(self) -> None:
        """
        Inits the programm for the first time, after a new factory get created.
        :return: None
        """
        self.file_path = ""
        self.factory.set_restrictions_to_max()
        if len(self.factory.facility_list) > 0:
            self.factory.generate_colors()
            self.allocate_sink_source(self.factory.facility_list)
        self.init_controls()
        self.factory_scene.clear()
        self.factory_scene.init_grid()
        self.sidebar.set_facilities_table(self.factory_scene.facilities)
        self.sidebar.set_area_table()
        self.factory.optimization.set_default(len(self.factory.facility_list))

    def import_excel(self) -> None:
        """
        Imports data from excel sheet and integrates it into the factory.
        :return: None
        """
        fname = QFileDialog.getOpenFileName(
            self, "Open File", ".", "Excel files (*.xlsm)"
        )
        if fname[0]:
            logging.info("Import Excel {}".format(fname[0]))
            import_factory = FactoryModel()
            parse_xl(fname[0], import_factory)
            logging.info("Excel parsed successful.")
            names = [fac.name for fac in self.factory.facility_list]
            new_names = [fac.name for fac in import_factory.facility_list]
            to_delete = []
            for facility in import_factory.facility_list:
                if facility.name in names:
                    while True:
                        inputd = QInputDialog()
                        for i in range(2, 1000):
                            new_name = facility.name + str(i)
                            if new_name not in names and new_name not in new_names:
                                inputd.setTextValue(new_name)
                                break
                        inputd.setOkButtonText(self.tr("Add"))
                        inputd.setWindowTitle(self.tr("Facility {} already exist.".format(facility.name)))
                        inputd.setLabelText(
                            self.tr("Change the name of {} or it won't get imported!\nNew Name:".format(facility.name)))
                        inputd.setInputMode(QInputDialog.TextInput)
                        ok = inputd.exec_()
                        text = inputd.textValue()
                        if ok:
                            if text not in names and text not in new_names:
                                new_names.remove(facility.name)
                                new_names.append(text)
                                facility.name = text
                                break
                        else:
                            to_delete.append(facility.name)
                            break
            for name in to_delete:
                import_factory.delete_facility_complete(name, not_init=True)
            factory_table_cf = FactoryTable(import_factory, self, on_start=True)
            factory_table_cf.show()
            factory_table_cf.ui.tab_widget.setCurrentIndex(2)
            result = factory_table_cf.exec_()
            if result:
                if len(import_factory.facility_list) > 0 and self.allocate_sink_source(import_factory.facility_list):
                    self.factory.import_factory(import_factory)
                    self.factory.generate_colors()
                    self.refresh()

    def optimize(self) -> None:
        """
        This method will be called if the user clicks the calculate button.
        This function is going to call the backend to run the optimization of the factory.
        :return: None
        """
        if self.open_weightingdialog() and self.open_optimizedialog(calc=True):
            if self.factory.parameter['multicore'] > os.cpu_count():
                cores = os.cpu_count() - 1
            else:
                cores = self.factory.parameter['multicore']
            backup_old = sys.stdout
            queue = multiprocessing.Queue()
            error_queue = multiprocessing.Queue()
            out_stream = WriteStream(queue)
            sys.stdout = out_stream
            dlg = CalculationOutput(cores)
            dlg.show()

            thread = QThread()
            my_receiver = CalculationOutputReceiver(queue, cores)
            my_receiver.new_message_signal.connect(dlg.append_text)
            my_receiver.moveToThread(thread)
            thread.started.connect(my_receiver.run)
            thread.start()

            # define outputs of processes
            output = multiprocessing.Queue()

            logging.info("Starting optimization")

            self.factory.layout = np.copy(self.factory.layout)
            self.factory.facility_list = copy.deepcopy(self.factory.facility_list)
            self.factory.path_list = copy.deepcopy(self.factory.path_list)
            self.factory.quamfab = copy.deepcopy(self.factory.quamfab)

            if __name__ == '__main__':
                processes = [
                    multiprocessing.Process(target=start_multi_p,
                                            args=((self.factory, out_stream, output, core, error_queue),))
                    for core in range(cores)]
                # start processes
                for p in processes:
                    p.start()
                    logging.info("Process {} started".format(p.pid))

                if not dlg.exec_():
                    for p in processes:
                        p.terminate()
                        out_stream.write("Done!")
                    thread.quit()
                    thread.wait()
                    sys.stdout = backup_old
                else:
                    results = []

                    for p in processes:
                        while not output.empty():
                            results.append(output.get())

                        while not error_queue.empty():
                            errordialog = QMessageBox(self)
                            errordialog.setText(self.tr("An error occured during optimization!"))
                            errordialog.setInformativeText(str(error_queue.get()))
                            errordialog.setStandardButtons(QMessageBox.Ok)
                            errordialog.setIcon(QMessageBox.Critical)
                            errordialog.exec_()
                        p.join()
                        logging.info("Process {} joined.".format(p.pid))

                    thread.quit()
                    thread.wait()
                    sys.stdout = backup_old
                    if results:
                        save_multi_result(self.factory, results)

                        if self.open_comparison_view():
                            self.ui.action_result.setEnabled(True)
                            self.ui.action_comparison.setEnabled(True)
                            self.ui.action_all_layouts.setEnabled(True)
        self.factory.parameter['no_path_planning'] = False

    def evaluate(self) -> None:
        """
        The method will call the backend evaluation function to evaluate the actual factory layout.
        For each facility the sink and source position must be defined.
        :return: None
        """
        for facility in self.factory.facility_list:
            if facility not in [x.attributes for x in self.factory_scene.facilities]:
                errordialog = QMessageBox(self)
                errordialog.setText(self.tr("Not enough information!"))
                errordialog.setInformativeText(self.tr("All facilities must be placed before evaluation!"))
                errordialog.setIcon(QMessageBox.Warning)
                errordialog.exec()
                return
        if self.open_weightingdialog(calc=True):
            logging.info("Starting evaluation")

            """
            for facility in self.factory.facility_list:
                if not hasattr(facility, 'noise'):
                    pass  # facility.noise = NoisePropagationLayout(self.factory)

            if not hasattr(self.factory, 'noise_wall_list') or not hasattr(self.factory, 'noise_corner_list'):
                self.factory.noise_wall_list, self.factory.noise_corner_list = wall_and_corner_cells(self.factory.layout)
            """

            self.factory.layout = np.copy(self.factory.layout)
            self.factory.facility_list = copy.deepcopy(self.factory.facility_list)
            self.factory.path_list = copy.deepcopy(self.factory.path_list)
            self.factory.quamfab = copy.deepcopy(self.factory.quamfab)

            quamfab_workflow(self.factory)

            """
            # delete NoisePropagation_Layouts
            for facility in self.factory.facility_list:
                del facility.noise

            # delete wall and corner cell lists
            del self.factory.noise_wall_list
            del self.factory.noise_corner_list
            """

            self.factory.add_layout_data()
            eval_dialog = EvaluationDialog(self.factory.quamfab, self.factory.quamfab_min,
                                           self.factory.weighting)
            eval_dialog.setModal(True)
            eval_dialog.show()
            eval_dialog.exec_()
            self.ui.action_result.setEnabled(True)
            self.sidebar.update_sidebar()
            self.refresh()

    def load_file(self, parentdialog) -> None:
        """
        The method will Open the QFileDialog where user can selected a file
        to load the factory from.
        :param parentdialog: Another Dialog where this method can be called from.
        :return: None
        """
        fname = QFileDialog.getOpenFileName(self, 'Open File', '.', "JSON file (*.json)")
        if fname[0]:
            logging.info("Load factory file")
            if parentdialog:
                parentdialog.accept()

            self.factory.load_factory(fname[0])
            logging.info(self.factory)
            self.deinit_controls()
            self.factory_scene = FactoryLayoutScene(self.factory, parent=self)
            self.graphic_view_factory.setScene(self.factory_scene)
            self.graphic_view_factory.fitInView(0, 0, self.factory.layout.shape[
                1] * self.factory.cell_zoom, self.factory.layout.shape[0] * self.factory.cell_zoom,
                                                Qt.KeepAspectRatio)
            self.file_path = fname[0]
            self.init_controls()
            self.factory_scene.init_grid()
            self.factory_scene.load_facilities()
            self.sidebar.set_facilities_table(self.factory_scene.facilities)
            self.sidebar.set_area_table()
        else:
            logging.info("FileDialog closed")

    def save_file(self) -> None:
        """
        If a path for saving the factory is already given, this method will directly save the
        factory. Otherwise the save as FileDialog will be opened.
        :return: None
        """
        if self.file_path == "":
            self.save_file_as()
        else:
            logging.info("Saving file")
            self.factory.save_factory(self.file_path)

    def save_file_as(self) -> None:
        """
        This method is opening the QFileDialog for specifying the place for saving the factory.
        :return: None
        """
        logging.info("Saving file as")
        fname = QFileDialog.getSaveFileName(caption="Save as", filter='*.json')
        if fname[0] != '':
            self.file_path = fname[0]
            self.save_file()

    def allocate_sink_source(self, facility_list, load_excel=False) -> int:
        """
        After loading the excel file the user has to specify all sink and source positions.
        This method opens a Dialog where the user has to specify them.
        :param facility_list: List of facilities for allocating sink and source.
        :return: Result of exec dialog
        """
        logging.info("Opening FacilityDialog")
        facility_dialog = FacilityDialog(facility_list, self.factory.cell_size,
                                         self.factory.cell_zoom)
        facility_dialog.setModal(True)
        facility_dialog.show()
        facility_dialog.fit_view_after_layout_loading()
        result = facility_dialog.exec_()
        if load_excel and not result:
            self.factory.reset_rows_columns()
            self.open_factorysizedialog(new=False)
        return result

    def change_language(self, path_to_lang) -> None:
        """
        Changes language of gui to the given language file.
        :param path_to_lang: Path to language file.
        :return: None
        """
        self.translator.load(path_to_lang)
        self.ui.retranslateUi(self)
        self.translate_ui()

    def translate_ui(self) -> None:
        """
        Updates the language of gui elements.
        :return: None
        """
        id = self.splitter.indexOf(self.sidebar)
        self.sidebar.deleteLater()
        self.sidebar = CollapsibleDialog(self.factory)

        self.splitter.replaceWidget(id, self.sidebar)

    def reset_layout(self) -> None:
        """
        This methods resets the current layout of the factory.
        :return:
        """
        errordialog = QMessageBox(self)
        errordialog.setStandardButtons(QMessageBox.StandardButtons(QMessageBox.Ok | QMessageBox.Cancel))
        errordialog.setText(self.tr("Do you really want to reset the current layout?"))
        errordialog.setInformativeText(self.tr("All facilties except the fixed ones, will be deleted!"))
        errordialog.setIcon(QMessageBox.Warning)
        errordialog.show()
        if errordialog.exec_() == QMessageBox.Ok:
            reset_layout(self.factory)
            self.refresh()

    def recalculate_paths(self) -> None:
        """
        Recalculate all path in actual layout.
        :return: None
        """
        delete_all_paths(self.factory)
        try_path_planning_for_layout(self.factory, True)
        self.refresh()

    def load_content(self) -> None:
        """
        This method loads external content. This method is necessary because if a program in Windows is packaged
        in a single .exe file, external content can only be accessed after the program has been completely initialized.
        :return:
        """


if __name__ == '__main__':
    multiprocessing.freeze_support()
    translator = QtCore.QTranslator()
    APP = QApplication(sys.argv)
    APP.installTranslator(translator)
    WINDOW = MainView(translator)
    WINDOW.show()
    WINDOW.load_content()
    WINDOW.open_startdialog()
    sys.exit(APP.exec_())
