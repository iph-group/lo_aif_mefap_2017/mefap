from PySide2.QtCore import QCoreApplication
from PySide2.QtWidgets import QDialog, QListWidgetItem

from mefap.factory.weighting import Weighting
from mefap.gui.generated.PrePairwaiseComparisonDialog import Ui_PrePairwiseComparisonDialog


class PrePairwiseComparisonDialog(QDialog):
    """
    In this dialog the user can make a preselection of the attributs he wants to compare in the pairwise comparison.
    """

    def __init__(self, weighting: Weighting) -> None:
        super().__init__()

        # loading Ui
        self.ui = Ui_PrePairwiseComparisonDialog()
        self.ui.setupUi(self)

        self.attr_name = {}
        self.weighting = weighting

        for attr, _ in self.weighting.__dict__.items():
            self.attr_name[QCoreApplication.translate("EvaluationDialog", attr, None)] = attr
            QListWidgetItem(QCoreApplication.translate("EvaluationDialog", attr, None), self.ui.listWidget_left)

        self.ui.pushButton_right.clicked.connect(self.move_right)
        self.ui.pushButton_left.clicked.connect(self.move_left)

    def move_right(self) -> None:
        """
        Moves the selected element to the right list. (no criteria)
        :return: None
        """
        curr_item = self.ui.listWidget_left.currentItem()
        if curr_item:
            self.ui.listWidget_right.addItem(QListWidgetItem(curr_item.text()))
            self.ui.listWidget_left.takeItem(self.ui.listWidget_left.row(curr_item))
            del curr_item

    def move_left(self) -> None:
        """
        Moves the selected element to the left list. (criteria)
        :return: None
        """
        curr_item = self.ui.listWidget_right.currentItem()
        if curr_item:
            self.ui.listWidget_left.addItem(QListWidgetItem(curr_item.text()))
            self.ui.listWidget_right.takeItem(self.ui.listWidget_right.row(curr_item))
            del curr_item
