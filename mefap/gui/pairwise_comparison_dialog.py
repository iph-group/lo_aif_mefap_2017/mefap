from PySide2.QtCore import QCoreApplication
from PySide2.QtWidgets import QDialog

from mefap.factory.weighting import Weighting
from mefap.gui.generated.PairwaiseComparisonDialog import Ui_pairwise_comparison_dialog


class PairwiseComparisonDialog(QDialog):
    """
    This dialog runs through a pairwise comparison dialog of all weighting options.
    """

    def __init__(self, weighting: Weighting, criteria_list, attr_name_dict) -> None:
        super().__init__()

        # loading Ui
        self.ui = Ui_pairwise_comparison_dialog()
        self.ui.setupUi(self)

        self.attr_name_dict = attr_name_dict

        self.weighting = weighting

        self.not_weighted = []
        self.weights_count = {}
        self.weights1 = []
        self.weights2 = []
        for attr, _ in self.weighting.__dict__.items():
            if QCoreApplication.translate("EvaluationDialog", attr, None) in criteria_list:
                self.weights_count[QCoreApplication.translate("EvaluationDialog", attr, None)] = 0
                self.weights1.append(QCoreApplication.translate("EvaluationDialog", attr, None))
                self.weights2.append(QCoreApplication.translate("EvaluationDialog", attr, None))
            else:
                self.not_weighted.append(QCoreApplication.translate("EvaluationDialog", attr, None))

        self.weights2.pop(0)

        self.max_c = int((len(self.weights1) ** 2 - len(self.weights1)) / 2)
        self.count = 0

        self.ui.count_label.setText("{}/{}".format(self.count, self.max_c))

        self.ui.left_pushButton.setText(QCoreApplication.translate("EvaluationDialog", self.weights1.pop(0), None))
        self.ui.left_pushButton.clicked.connect(self.clicked_left)

        self.ui.right_pushButton.setText(QCoreApplication.translate("EvaluationDialog", self.weights2.pop(0), None))
        self.ui.right_pushButton.clicked.connect(self.clicked_right)

    def clicked_left(self) -> None:
        """
        Counts the click on the left button and loads the next comparison.
        :return: None
        """
        self.weights_count[self.ui.left_pushButton.text()] += 1
        self.count += 1
        self.ui.count_label.setText("{}/{}".format(self.count, self.max_c))
        self.next_comparison()
        if self.count >= self.max_c:
            self.calc_weights()

    def clicked_right(self):
        """
        Counts the click on the right button and loads the next comparison.
        :return: None
        """
        self.weights_count[self.ui.right_pushButton.text()] += 1
        self.count += 1
        self.ui.count_label.setText("{}/{}".format(self.count, self.max_c))
        self.next_comparison()
        if self.count >= self.max_c:
            self.calc_weights()

    def next_comparison(self) -> None:
        """
        Load the next comparison.
        :return: None
        """
        if self.weights2:
            self.ui.right_pushButton.setText(QCoreApplication.translate("EvaluationDialog", self.weights2.pop(0), None))
        elif len(self.weights1) > 1:
            self.ui.left_pushButton.setText(QCoreApplication.translate("EvaluationDialog", self.weights1.pop(0), None))
            self.weights2 = self.weights1[:]
            self.ui.right_pushButton.setText(QCoreApplication.translate("EvaluationDialog", self.weights2.pop(0), None))

    def calc_weights(self) -> None:
        """
        Calculates the weighting from the pairwise comparison and closes the dialog.
        :return: None
        """
        for key, value in self.weights_count.items():
            setattr(self.weighting, self.attr_name_dict[key], round((value / self.max_c), 2))
        for attr in self.not_weighted:
            setattr(self.weighting, self.attr_name_dict[attr], 0)
        c = 0
        max_attr = ''
        min_attr = ''
        max_val = 0
        min_val = 1
        for attr, val in self.weighting.__dict__.items():
            c += val
            if val > max_val:
                max_val = val
                max_attr = attr
            if 0 < val < min_val:
                min_val = val
                min_attr = attr
        if c < 1:
            setattr(self.weighting, min_attr, min_val + (1 - c))
        elif c > 1:
            setattr(self.weighting, max_attr, max_val - (c - 1))
        c = 0
        for attr, val in self.weighting.__dict__.items():
            c += val
        self.accept()
