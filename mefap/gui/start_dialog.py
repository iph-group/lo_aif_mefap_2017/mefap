import logging
import os

import PySide2
from PySide2.QtCore import Qt
from PySide2.QtGui import QIcon, QPixmap
from PySide2.QtWidgets import QDialog

from mefap.gui.generated.StartDialog import Ui_start_dialog


class StartDialog(QDialog):
    """Dialog which is starting with MainWindow where the user can start a new factory layout
    or load an saved factory layout"""

    def __init__(self, parent) -> None:
        super().__init__()
        self.ui = Ui_start_dialog()
        self.ui.setupUi(self)
        self.parent = parent
        self.ui.load_layout_button.clicked.connect(self.get_file)
        self.ui.new_layout_button.clicked.connect(self.open_factorysizedialog)

        self.application_path = os.path.dirname(os.path.abspath(__file__))

        uk_name = "../resources/united-kingdom-flag-icon-32.png"
        ger_name = "../resources/germany-flag-icon-32.png"
        self.ui.language_comboBox.addItem(QIcon(os.path.join(self.application_path, ger_name)), self.tr("German"))
        self.ui.language_comboBox.addItem(QIcon(os.path.join(self.application_path, uk_name)), self.tr("English"))

        self.ui.language_comboBox.currentIndexChanged.connect(self.change_language)

        if not self.parent.started:
            application_path = os.path.dirname(os.path.abspath(__file__))

            logo_path = os.path.join(application_path, '../resources/IPH_Logo_jpg_web.jpg')
            self.parent.ui.label.setPixmap(QPixmap(logo_path))
            self.change_language()
            self.parent.started = True

    def get_file(self) -> None:
        """
        This method is updating the opening inside the Optimization object
        of the FactoryModel to random.
        :return: None
        """
        logging.info("Opening FileDialog for loading factory")
        self.parent.load_file(parentdialog=self)

    def open_factorysizedialog(self) -> None:
        """
        Opens FactorySizeDialog from Mainview.
        :return: None
        """
        self.parent.open_factorysizedialog(parentdialog=self)

    def change_language(self) -> None:
        """
        Changes Gui to selected language
        :return: None
        """
        if self.ui.language_comboBox.currentIndex() == 0:
            self.parent.change_language(os.path.join(self.application_path, '../resources/i18n/de-DE'))
        elif self.ui.language_comboBox.currentIndex() == 1:
            self.parent.change_language(os.path.join(self.application_path, '../resources/i18n/en-GB'))
        self.ui.retranslateUi(self)

    def keyPressEvent(self, arg__1: PySide2.QtGui.QKeyEvent) -> None:
        """
        Switch between buttons with the up and down arrow.
        :param arg__1:
        :return: None
        """
        if arg__1.key() == Qt.Key_Up:
            self.ui.new_layout_button.setDefault(True)
        elif arg__1.key() == Qt.Key_Down:
            self.ui.load_layout_button.setDefault(True)
        super().keyPressEvent(arg__1)
