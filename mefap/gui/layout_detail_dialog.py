import copy
import logging

from PySide2.QtWidgets import QDialog, QVBoxLayout, QDialogButtonBox

from mefap.gui.compare_layout import CompareLayoutScene
from mefap.gui.compare_view import CompareView
from mefap.gui.factory_compare_dialog import FactoryCompareDialog
from mefap.gui.generated.LayoutDetailDialog import Ui_LayoutDetailDialog, Qt


class LayoutDetailDialog(QDialog):
    """
    Dialog to show more information about a chosen layout.
    """

    def __init__(self, factory, layout, facilities, path, evaluation, index):
        super().__init__()
        self.ui = Ui_LayoutDetailDialog()
        self.ui.setupUi(self)

        self.factory = factory
        self.index = index

        self.view = CompareView(self)
        facilities_list = copy.deepcopy(factory.facility_list)
        for facility in facilities:
            facilities_list[facility.index_num].curr_position = facility.curr_position
            facilities_list[
                facility.index_num].curr_source_position = facility.curr_source_position
            facilities_list[facility.index_num].curr_sink_position = facility.curr_sink_position
            facilities_list[facility.index_num].curr_rotation = facility.curr_rotation
            facilities_list[facility.index_num].curr_mirror = facility.curr_mirror
        scene = CompareLayoutScene(layout, facilities_list, factory)
        scene.init_grid()
        scene.load_facilities()
        self.view.setScene(scene)

        layout = QVBoxLayout()
        self.setLayout(layout)

        layout.addWidget(self.view)
        layout.addWidget(FactoryCompareDialog([evaluation], index=index))
        self.buttonBox = QDialogButtonBox()
        self.buttonBox.setOrientation(Qt.Horizontal)
        self.buttonBox.setStandardButtons(QDialogButtonBox.Cancel)
        self.buttonBox.addButton("Layout übernehmen", QDialogButtonBox.AcceptRole)
        layout.addWidget(self.buttonBox)
        self.buttonBox.accepted.connect(self.apply)
        self.buttonBox.rejected.connect(self.reject)

    def fit_view(self) -> None:
        """
        Fits content of graphics view in given place.
        :return: None
        """
        self.view.fitInView(0, 0, self.factory.layout.shape[1] * self.factory.cell_zoom,
                            self.factory.layout.shape[0] * self.factory.cell_zoom, Qt.KeepAspectRatio)

    def apply(self) -> None:
        """
        Sets layout to main layout in MainView.
        :return: None
        """
        logging.info("Loading layout {} into MainView".format(self.index))
        self.factory.layout = self.factory.layouts[self.index]

        for facility in self.factory.layouts_facility_data[self.index]:
            self.factory.facility_list[
                facility.index_num].curr_position = facility.curr_position
            self.factory.facility_list[
                facility.index_num].curr_source_position = facility.curr_source_position
            self.factory.facility_list[
                facility.index_num].curr_sink_position = facility.curr_sink_position
            self.factory.facility_list[
                facility.index_num].curr_rotation = facility.curr_rotation
            self.factory.facility_list[facility.index_num].curr_mirror = facility.curr_mirror

        self.factory.path_list = self.factory.layouts_path_data[self.index]
        self.factory.quamfab = self.factory.layouts_evaluation_data[self.index]
        self.accept()
