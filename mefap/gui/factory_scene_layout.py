import logging
from typing import List

import matplotlib
from PySide2.QtCore import Qt, QRect
from PySide2.QtGui import QPen
from PySide2.QtWidgets import QGraphicsScene, QGraphicsSimpleTextItem, QGraphicsItem, QMessageBox, QGraphicsRectItem

from mefap.evaluation.eval_environmental import re_calc_noise_matrix
from mefap.factory.factory_model import FactoryModel
from mefap.factory.media_availability import MediaAvailability
from mefap.factory.restriction import Restriction
from mefap.factory.restrictive_area import RestrictiveArea
from mefap.factory.util.light_level import LightLevel
from mefap.gui.factory_cell import FactoryCell
from mefap.gui.factory_facility import FactoryFacility
from mefap.gui.util.colors import GREY_CELL, MEDIA_CELL, MAXIMAL_LIGHT_CELL, OFF_LIGHT_CELL, \
    VERY_DARK_LIGHT_CELL, DARK_LIGHT_CELL, NORMAL_LIGHT_CELL, BRIGHT_LIGHT_CELL, \
    VERY_BRIGHT_LIGHT_CELL, VERY_DARK_BROWN, DARK_BROWN, BROWN, LIGHT_BROWN, VERY_LIGHT_BROWN, \
    BRIGHT_BROWN, BLACK
from mefap.gui.util.layout_mode import LayoutMode


class FactoryLayoutScene(QGraphicsScene):
    """
    The FactoryLayoutScene inherits the QGraphicsScene and will be shown by the FactoryView.
    It will manage all objects of the factory representation.
    By using different internal modes, this scene can represent
    different views on factory attributs.
    """

    def __init__(self, factory: FactoryModel, mode=LayoutMode.ADD_FACILITIES, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.factory: FactoryModel = factory
        self.mode = mode
        self.cf_mf_toggle = False
        self.cf_mf_current = None
        self.actual_media: MediaAvailability = None
        self.actual_restriction: Restriction = None
        self.cells: List[FactoryCell] = []
        self.facilities: List[FactoryFacility] = []
        self.text_objects: List[QGraphicsSimpleTextItem] = []
        self.sound_boxes: List[QGraphicsRectItem] = []

    def init_grid(self, with_hidden=False) -> None:
        """
        Initialises grid.
        :return: None
        """
        logging.info("Initialising grid")
        self.cells = None
        self.clear()
        self.cells = self.generate_cells(with_hidden=with_hidden)
        for cell in self.cells:
            self.addItem(cell)
            cell.update_path()

    def generate_cells(self, with_hidden=False) -> List[FactoryCell]:
        """
        Generated empty cells for the grid.
        :return: A List of FactoryCells.
        """
        logging.info("Generating Cells")
        cells = []
        for x_cord in range(self.factory.layout.shape[1]):
            for y_cord in range(self.factory.layout.shape[0]):
                hidden = self.factory.layout[y_cord][x_cord] == self.factory.parameter["hidden_cell_value"]
                if not hidden or with_hidden:
                    cell = FactoryCell(x_cord, y_cord, self.factory.parameter,
                                       x_cord * self.factory.cell_zoom,
                                       y_cord * self.factory.cell_zoom,
                                       self.factory.cell_zoom,
                                       self.factory.cell_zoom)
                    pen = cell.pen()
                    pen.setWidth(1)
                    cell.setPen(pen)
                    cells.append(cell)
                    if self.factory.layout[y_cord][x_cord] == self.factory.parameter[
                        "local_road_value"] or self.factory.layout[y_cord][x_cord] == \
                            self.factory.parameter["fixed_road_value"]:
                        cell.color_road()
                    elif self.factory.layout[y_cord][x_cord] == self.factory.parameter["hidden_cell_value"]:
                        cell.color_media(BLACK)
                        cell.gray = True
        return cells

    def show_media(self, media: MediaAvailability) -> None:
        """
        This method should show the availability of the given media.
        :param media: Object which represent a media of the FactoryModel
        :return: None
        """
        logging.info('Show Media ' + media.name)
        self.parent().ui.actual_view_label.setText(self.tr(
            "<h1>Ansicht: " + media.name + "</h1>"))
        self.mode = LayoutMode.MEDIA
        self.switch_mode()
        self.actual_media = media
        self.transparent_facilities()
        for cell in self.cells:
            if media.availability_layout[cell.y_cord][cell.x_cord]:
                cell.setBrush(MEDIA_CELL)
            else:
                cell.setBrush(GREY_CELL)

    def show_cell_characteristics(self) -> None:
        """
        This method should reset the layout to the mode of showing placed facility objects with
        sink and source.
        :return: None
        """
        logging.info('Show cell chatacteristics')
        self.parent().ui.actual_view_label.setText(self.tr(
            "<h1>Ansicht: Cell Info</h1>"))
        self.mode = LayoutMode.INFO
        self.switch_mode()
        self.activate_facilities()
        self.lock_facilities()
        self.update_path()

    def show_layout(self) -> None:
        """
        This method should reset the layout to the mode of showing placed facility objects.
        :return: None
        """
        logging.info('Show Layout / add Facilities')
        self.parent().ui.actual_view_label.setText(self.tr(
            "<h1>Ansicht: Standard</h1>"))
        self.mode = LayoutMode.ADD_FACILITIES
        self.switch_mode()
        self.activate_facilities()
        self.update_path()

    def show_light(self) -> None:
        """
        The method show_light will change the actual internal mode and is showing the representation
        of the FactoryModel attribute illuminance.
        :return: None
        """
        logging.info("Show Lightning")
        self.parent().ui.actual_view_label.setText(self.tr(
            "<h1>Ansicht: Licht</h2>"))
        self.mode = LayoutMode.LIGHT
        self.switch_mode()
        self.transparent_facilities()
        for cell in self.cells:
            if self.factory.illuminance_matrix[cell.y_cord][cell.x_cord] == LightLevel.OFF:
                cell.setBrush(OFF_LIGHT_CELL)
            elif self.factory.illuminance_matrix[cell.y_cord][cell.x_cord] == LightLevel.VERY_DARK:
                cell.setBrush(VERY_DARK_LIGHT_CELL)
            elif self.factory.illuminance_matrix[cell.y_cord][cell.x_cord] == LightLevel.DARK:
                cell.setBrush(DARK_LIGHT_CELL)
            elif self.factory.illuminance_matrix[cell.y_cord][cell.x_cord] == LightLevel.NORMAL:
                cell.setBrush(NORMAL_LIGHT_CELL)
            elif self.factory.illuminance_matrix[cell.y_cord][cell.x_cord] == LightLevel.BRIGHT:
                cell.setBrush(BRIGHT_LIGHT_CELL)
            elif self.factory.illuminance_matrix[cell.y_cord][
                cell.x_cord] == LightLevel.VERY_BRIGHT:
                cell.setBrush(VERY_BRIGHT_LIGHT_CELL)
            elif self.factory.illuminance_matrix[cell.y_cord][cell.x_cord] == LightLevel.MAXIMAL:
                cell.setBrush(MAXIMAL_LIGHT_CELL)

    def change_illuminance(self, cells: List[FactoryCell], mode: LightLevel) -> None:
        """
        The change_illuminance method will change the value inside FactoryModel attribute
        illuminance matrix, based on the given FactoryCells.
        :param cells: This cells were selected inside the FactoryView
        :param mode: The value which will be written into the factory attribute illuminance.
        :return: None
        """
        for cell in cells:
            self.factory.illuminance_matrix[cell.y_cord][cell.x_cord] = mode
        self.show_light()

    def show_restriction(self, restriction: Restriction) -> None:
        """
        If the show_restriction methods is called, the actual internal mode if the scene will
        change and it shows the given restriction.
        :param restriction: The values inside the restriction matrix will be visualied on the scene.
        :return: None
        """
        logging.info('Show Restriction' + restriction.name)
        self.parent().ui.actual_view_label.setText(self.tr(
            "<h1>Ansicht: " + restriction.name + "</h1>"))
        self.mode = LayoutMode.RESTRICTION
        self.switch_mode()
        self.actual_restriction = restriction
        self.transparent_facilities()
        for cell in self.cells:
            text = QGraphicsSimpleTextItem(str(int(restriction.matrix[cell.y_cord][cell.x_cord])),
                                           cell)
            text.setPos(cell.boundingRect().topLeft())
            old_br = text.boundingRect()
            text.setScale(cell.boundingRect().width() / text.boundingRect().width())
            if old_br.height() * text.scale() > cell.boundingRect().height():
                text.setScale(
                    (old_br.height() * text.scale()) / cell.boundingRect().height())
            self.text_objects.append(text)
            if restriction.matrix[cell.y_cord][cell.x_cord] < (restriction.max_value * (1 / 6)):
                cell.setBrush(VERY_DARK_BROWN)
            elif restriction.matrix[cell.y_cord][cell.x_cord] < (restriction.max_value * (1 / 3)):
                cell.setBrush(DARK_BROWN)
            elif restriction.matrix[cell.y_cord][cell.x_cord] < (restriction.max_value * (1 / 2)):
                cell.setBrush(BROWN)
            elif restriction.matrix[cell.y_cord][cell.x_cord] < (restriction.max_value * (2 / 3)):
                cell.setBrush(LIGHT_BROWN)
            elif restriction.matrix[cell.y_cord][cell.x_cord] < (restriction.max_value * (5 / 6)):
                cell.setBrush(VERY_LIGHT_BROWN)
            else:  # restriction.matrix[cell.x_cord][cell.y_cord] < restriction.max_value:
                cell.setBrush(BRIGHT_BROWN)

    def show_restrictive_area(self) -> None:
        """
        This method change the content of this scene for displaying restrictive areas.
        :return: None
        """
        logging.info('Show restrictive areas')
        self.parent().ui.actual_view_label.setText(self.tr(
            "<h1>Ansicht: Restrictive Area's</h1>"))
        self.mode = LayoutMode.AREA
        self.switch_mode()
        self.transparent_facilities()
        for cell in self.cells:
            cell.display_area()

    def show_noise_matrix(self) -> None:
        """
        This method changes the content of this scene for displaying, the noise matrix.
        :return: None
        """
        logging.info('Show noise matrix')
        self.parent().ui.actual_view_label.setText(self.tr(
            "<h1>Ansicht: Noise matrix</h1>"))
        self.mode = LayoutMode.NOISE
        self.switch_mode()
        self.transparent_facilities(noise=True)
        re_calc_noise_matrix(self.factory)
        cmap = matplotlib.cm.get_cmap('RdYlGn')

        for cell in self.cells:
            cell.show_noise(cmap, self.factory.noise_matrix[cell.y_cord][cell.x_cord])

        for wall_list in self.factory.noise_area.get_all_noise_walls().values():
            max_vals = list(map(max, zip(*wall_list)))
            min_vals = list(map(min, zip(*wall_list)))
            pen = QPen()
            pen.setWidth(4)
            sound_box = self.addRect(QRect(min_vals[1] * self.factory.cell_zoom + 2, min_vals[0] * self.factory.cell_zoom + 2,
                                           (max_vals[1] - min_vals[1] + 1) * self.factory.cell_zoom - 4,
                                           (max_vals[0] - min_vals[0] + 1) * self.factory.cell_zoom - 4),
                                     pen)
            self.sound_boxes.append(sound_box)

        for facility in self.facilities:
            facility.setToolTip("")

    def show_mf(self) -> None:
        """
        This method changes the content of this scene for displaying, the material flow matrix.
        :return: None
        """
        logging.info("Show mf out")
        self.parent().ui.actual_view_label.setText(self.tr(
            "<h1>Ansicht: Material flow (out)</h1>"))
        self.mode = LayoutMode.MF
        self.switch_mode()
        self.cf_mf_toggle = False
        self.cf_mf_current = None
        if len(self.facilities) > 0:
            self.display_mf(self.facilities[0])

    def show_cf(self) -> None:
        """
        This method changes the content of this scene for displaying, the communication matrix.
        :return: None
        """
        logging.info("Show cf out")
        self.parent().ui.actual_view_label.setText(self.tr(
            "<h1>Ansicht: Communication flow (out)</h1>"))
        self.mode = LayoutMode.CF
        self.switch_mode()
        self.cf_mf_toggle = False
        self.cf_mf_current = None
        if len(self.facilities) > 0:
            self.display_cf(self.facilities[0])

    def display_mf(self, facility) -> None:
        """
        This method gets all the relevant cells and facilities from the factory model displaying the cf matrix
         and paints them.
        :param facility: The facility which mf path gets painted
        :return: None
        """
        self.update_path()
        ins, ins_index, outs, outs_index = self.factory.get_paths_mf(facility.attributes.index_num)
        if not self.cf_mf_toggle:
            self.transparent_facilities(without=facility.attributes.index_num, without_half=outs_index)
            self.color_path(outs, facility.attributes.color)
        else:
            self.transparent_facilities(without=facility.attributes.index_num, without_half=ins_index)
            self.color_path(ins, facility.attributes.color)
        self.cf_mf_current = facility

    def display_cf(self, facility) -> None:
        """
        This method gets all the relevant cells and facilities from the factory model for displaying the cf matrix
        and paints them.
        :param facility: The facility which cf path gets painted
        :return: None
        """
        self.update_path()
        ins, ins_index, outs, outs_index = self.factory.get_paths_cf(facility.attributes.index_num)
        if not self.cf_mf_toggle:
            self.transparent_facilities(without=facility.attributes.index_num, without_half=outs_index)
            self.color_path(outs, facility.attributes.color)
        else:
            self.transparent_facilities(without=facility.attributes.index_num, without_half=ins_index)
            self.color_path(ins, facility.attributes.color)
        self.cf_mf_current = facility

    def toggle_mf(self) -> None:
        """
        This method toggles between the states of showing the ingoing and outgoing mf connections of the actual selected
        facility.
        :return: None
        """
        if self.cf_mf_toggle:
            logging.info("Show mf out")
            self.parent().ui.actual_view_label.setText(self.tr(
                "<h1>Ansicht: Material flow (out)</h1>"))
            self.cf_mf_toggle = False
            if len(self.facilities) > 0 and self.cf_mf_current:
                self.display_mf(self.cf_mf_current)
            else:
                self.update_path()
                self.transparent_facilities()
        else:
            logging.info("Show mf in")
            self.parent().ui.actual_view_label.setText(self.tr(
                "<h1>Ansicht: Material flow (in)</h1>"))
            self.cf_mf_toggle = True
            if len(self.facilities) > 0 and self.cf_mf_current:
                self.display_mf(self.cf_mf_current)
            else:
                self.update_path()
                self.transparent_facilities()

    def toggle_cf(self) -> None:
        """
        This method toggles between the states of showing the ingoing and outgoing cf connections of the actual selected
        facility.
        :return: None
        """
        if self.cf_mf_toggle:
            logging.info("Show cf out")
            self.parent().ui.actual_view_label.setText(self.tr(
                "<h1>Ansicht: Communication flow (out)</h1>"))
            self.cf_mf_toggle = False
            if len(self.facilities) > 0 and self.cf_mf_current:
                self.display_cf(self.cf_mf_current)
            else:
                self.update_path()
                self.transparent_facilities()
        else:
            logging.info("Show cf in")
            self.parent().ui.actual_view_label.setText(self.tr(
                "<h1>Ansicht: Communication flow (in)</h1>"))
            self.cf_mf_toggle = True
            if len(self.facilities) > 0 and self.cf_mf_current:
                self.display_cf(self.cf_mf_current)
            else:
                self.update_path()
                self.transparent_facilities()

    def change_restrictive_area(self, cells: List[FactoryCell], area: RestrictiveArea) -> None:
        """
        This method is changing cells of this scene to the given RestrictiveArea.
        :param cells: Cells to be changed
        :param area: Restrictive Area which the cell will be changed to
        :return: None
        """
        # old_area = self.factory.restrictive_area.copy()
        self.reset_tooltipp_cells()
        not_fit = False
        not_fit_list = []
        dict_of_areas = dict.fromkeys([area.id for area in self.factory.area_types], 0)
        for cell in cells:
            cell.change_area(area)
        for facility in self.factory.facility_list:
            facility_type_area = self.factory.get_facilitytype_by_name(facility.facility_type).id
            if self.factory.area_exists(facility_type_area):
                dict_of_areas[facility_type_area] += facility.cell_num
        for x_pos in self.factory.restrictive_area:
            for y_pos in x_pos:
                if y_pos in dict_of_areas:
                    dict_of_areas[y_pos] -= 1
        error_keys = []
        for key in dict_of_areas:
            if dict_of_areas[key] > 0:
                error_keys.append(key)
        if error_keys:
            error_keys = [self.factory.get_facilitytype_by_id(key).name for key in error_keys]
            errordialog = QMessageBox()
            errordialog.setText(self.tr("Value error!"))
            errordialog.setInformativeText(
                self.tr(
                    "Some areas {} are not large enough to accommodate all of the related facilities!").format(
                    error_keys))
            errordialog.setIcon(QMessageBox.Warning)
            errordialog.exec_()
        for facility in self.facilities:
            if facility.attributes.facility_type == area.name and not facility.is_allowed_area():
                not_fit_list.append(facility.attributes.name)
                facility.delete(nopath=True)
                not_fit = True
        if not_fit:
            errordialog = QMessageBox()
            errordialog.setText(self.tr("Some facilities will be deleted!"))
            errordialog.setInformativeText(self.tr(
                "Some facilities {} are placed outside their area of use and will be deleted!".format(
                    not_fit_list)))
            errordialog.setIcon(QMessageBox.Warning)
            errordialog.exec_()

    def transparent_facilities(self, noise=False, without=-1, without_half=None) -> None:
        """
        This method sets all Facility objects in the scene in transparency mode (not movable)
        :param without_half: The Indexes of facilities which gets a opacity between normal and transparent.
        :param without: The index of a facility which is getting a normal opacity.
        :param noise: Sets opacity of facility filling to nearly zero.
        :return: None
        """
        for index, facility in enumerate(self.facilities):
            if without >= 0 and facility.attributes.index_num == without:
                facility.setOpacity(1)
                continue
            if without_half and facility.attributes.index_num in without_half:
                facility.setOpacity(0.75)
                continue
            if noise:
                facility.setBrush(Qt.NoBrush)
                facility.setOpacity(1)
            else:
                if facility.brush().style() == Qt.NoBrush:
                    facility.make_coloring()
                facility.setOpacity(0.4)
            facility.setFlag(QGraphicsItem.ItemIsMovable, False)
            facility.generate_sink_source()

    def lock_facilities(self) -> None:
        """
        This method sets all Facility objects in the scene to not movable.
        :return: None
        """
        for facility in self.facilities:
            facility.setFlag(QGraphicsItem.ItemIsMovable, False)

    def activate_facilities(self) -> None:
        """
        This method set all Facility objects in the scene in active mode (movable)
        :return: None
        """
        for facility in self.facilities:
            facility.setOpacity(1)
            facility.make_coloring()
            facility.setFlag(QGraphicsItem.ItemIsMovable, True)
            facility.generate_sink_source()
            if facility.toolTip() == "":
                facility.make_tool_tip()

    def load_facilities(self) -> None:
        """
        This method will be called at loading a existing factory.
        Depending on the layout matrix the already placed facilities will be shown.
        :return: None
        """
        positions = self.factory.get_upper_left_of_placed_facilities()
        for position in positions:
            for facility in self.factory.facility_list:
                if facility.index_num == position[1]:
                    for cell in self.cells:
                        if cell.x_cord == position[0][0] and cell.y_cord == position[0][1]:
                            if self.factory.parameter["color_facilities_by_sound"]:
                                facilityitem = FactoryFacility(facility,
                                                               cell.boundingRect().topLeft().x() + 1.5,
                                                               cell.boundingRect().topLeft().y() + 1.5,
                                                               facility.cell_width1 * cell.boundingRect().width() - facility.cell_width1 - 2,
                                                               facility.cell_width2 * cell.boundingRect().height() - facility.cell_width2 - 2,
                                                               color_sound=self.factory.facility_characteristics[facility.index_num].noise_exposure)
                            else:
                                facilityitem = FactoryFacility(facility,
                                                               cell.boundingRect().topLeft().x() + 1.5,
                                                               cell.boundingRect().topLeft().y() + 1.5,
                                                               facility.cell_width1 * cell.boundingRect().width() - facility.cell_width1 - 2,
                                                               facility.cell_width2 * cell.boundingRect().height() - facility.cell_width2 - 2)
                            self.addItem(facilityitem)
                            facilityitem.rotate_after_loading()
                            self.facilities.append(facilityitem)
                            self.update_path()
                            break
                    break

    def remove_text_object(self) -> None:
        """
        The remove_text_object methods will remove all active text objects from the scene.
        :return: None
        """
        for text in self.text_objects:
            self.removeItem(text)
        self.text_objects.clear()

    def reset(self) -> None:
        """
        Resets the layout completely.
        :return: None
        """
        self.mode = LayoutMode.SHOW
        self.actual_media: MediaAvailability = None
        self.actual_restriction: Restriction = None
        self.cells.clear()
        self.facilities.clear()
        self.text_objects.clear()
        self.clear()

    def reset_tooltipp_cells(self) -> None:
        """
        Sets the tooltip of all cells to empty strings.
        :return: None
        """
        for cell in self.cells:
            cell.setToolTip("")

    def propagate_up_facility_list(self) -> None:
        """
        method to propagate information up.
        :return: None
        """
        self.parent().sidebar.set_facilities_table(self.facilities)

    def update_path(self) -> None:
        """
        Colors all the to the right road value.
        :return: None
        """
        for cell in self.cells:
            cell.update_path()

    def color_path(self, path_list, color) -> None:
        """
        Colors all positions in the path list to the given color.
        :param path_list: Positions which get painted.
        :param color: The color which the positions get painted.
        :return: None
        """
        cells_idx = []
        for path in path_list:
            for element in path:
                if element not in cells_idx:
                    cells_idx.append(element)

        for cell in self.cells:
            position = (cell.y_cord, cell.x_cord)
            if position in cells_idx:
                cell.color_media(color)

    def remove_sound_boxes(self) -> None:
        """

        """
        for box in self.sound_boxes:
            self.removeItem(box)
        self.sound_boxes.clear()

    def switch_mode(self) -> None:
        """
        Clears internal setting of this layout, when switching between display modes.
        :return: None
        """
        self.reset_tooltipp_cells()
        self.parent().sidebar.remove_noise_legend()
        self.actual_media = None
        self.actual_restriction = None
        self.remove_text_object()
        self.remove_sound_boxes()
