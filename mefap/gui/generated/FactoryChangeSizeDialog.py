# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'FactoryChangeSizeDialog.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_FactoryChangeSizeDialog(object):
    def setupUi(self, FactoryChangeSizeDialog):
        if not FactoryChangeSizeDialog.objectName():
            FactoryChangeSizeDialog.setObjectName(u"FactoryChangeSizeDialog")
        FactoryChangeSizeDialog.resize(540, 159)
        self.verticalLayout = QVBoxLayout(FactoryChangeSizeDialog)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.label_2 = QLabel(FactoryChangeSizeDialog)
        self.label_2.setObjectName(u"label_2")

        self.horizontalLayout.addWidget(self.label_2)

        self.spinBox_factory_width = QSpinBox(FactoryChangeSizeDialog)
        self.spinBox_factory_width.setObjectName(u"spinBox_factory_width")
        self.spinBox_factory_width.setMinimum(0)
        self.spinBox_factory_width.setMaximum(99999)

        self.horizontalLayout.addWidget(self.spinBox_factory_width)

        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.horizontalLayout.addItem(self.verticalSpacer)

        self.label = QLabel(FactoryChangeSizeDialog)
        self.label.setObjectName(u"label")

        self.horizontalLayout.addWidget(self.label)

        self.spinBox_factory_length = QSpinBox(FactoryChangeSizeDialog)
        self.spinBox_factory_length.setObjectName(u"spinBox_factory_length")
        self.spinBox_factory_length.setReadOnly(False)
        self.spinBox_factory_length.setButtonSymbols(QAbstractSpinBox.UpDownArrows)
        self.spinBox_factory_length.setKeyboardTracking(True)
        self.spinBox_factory_length.setMinimum(0)
        self.spinBox_factory_length.setMaximum(99999)

        self.horizontalLayout.addWidget(self.spinBox_factory_length)

        self.verticalLayout.addLayout(self.horizontalLayout)

        self.label_warn = QLabel(FactoryChangeSizeDialog)
        self.label_warn.setObjectName(u"label_warn")

        self.verticalLayout.addWidget(self.label_warn)

        self.buttonBox = QDialogButtonBox(FactoryChangeSizeDialog)
        self.buttonBox.setObjectName(u"buttonBox")
        self.buttonBox.setOrientation(Qt.Horizontal)
        self.buttonBox.setStandardButtons(QDialogButtonBox.Cancel | QDialogButtonBox.Ok)

        self.verticalLayout.addWidget(self.buttonBox)

        self.retranslateUi(FactoryChangeSizeDialog)
        self.buttonBox.accepted.connect(FactoryChangeSizeDialog.accept)
        self.buttonBox.rejected.connect(FactoryChangeSizeDialog.reject)

        QMetaObject.connectSlotsByName(FactoryChangeSizeDialog)

    # setupUi

    def retranslateUi(self, FactoryChangeSizeDialog):
        FactoryChangeSizeDialog.setWindowTitle(
            QCoreApplication.translate("FactoryChangeSizeDialog", u"Change facory size", None))
        self.label_2.setText(QCoreApplication.translate("FactoryChangeSizeDialog", u"Factory width [m]", None))
        self.label.setText(QCoreApplication.translate("FactoryChangeSizeDialog", u"Factory length [m]", None))
        self.label_warn.setText("")
    # retranslateUi
