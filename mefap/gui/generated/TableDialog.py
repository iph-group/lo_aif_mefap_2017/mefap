# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'TableDialog.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_table_dialog(object):
    def setupUi(self, table_dialog):
        if not table_dialog.objectName():
            table_dialog.setObjectName(u"table_dialog")
        table_dialog.resize(874, 620)
        self.verticalLayout = QVBoxLayout(table_dialog)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.tab_widget = QTabWidget(table_dialog)
        self.tab_widget.setObjectName(u"tab_widget")
        self.tab = QWidget()
        self.tab.setObjectName(u"tab")
        self.vertical_layout_2 = QVBoxLayout(self.tab)
        self.vertical_layout_2.setObjectName(u"vertical_layout_2")
        self.materialflow_table_widget = QTableWidget(self.tab)
        self.materialflow_table_widget.setObjectName(u"materialflow_table_widget")

        self.vertical_layout_2.addWidget(self.materialflow_table_widget)

        self.tab_widget.addTab(self.tab, "")
        self.tab_2 = QWidget()
        self.tab_2.setObjectName(u"tab_2")
        self.vertical_layout_3 = QVBoxLayout(self.tab_2)
        self.vertical_layout_3.setObjectName(u"vertical_layout_3")
        self.communicationflow_table_widget = QTableWidget(self.tab_2)
        self.communicationflow_table_widget.setObjectName(u"communicationflow_table_widget")

        self.vertical_layout_3.addWidget(self.communicationflow_table_widget)

        self.tab_widget.addTab(self.tab_2, "")
        self.tab_3 = QWidget()
        self.tab_3.setObjectName(u"tab_3")
        self.vertical_layout_4 = QVBoxLayout(self.tab_3)
        self.vertical_layout_4.setObjectName(u"vertical_layout_4")
        self.facilitiy_table_widget = QTableWidget(self.tab_3)
        self.facilitiy_table_widget.setObjectName(u"facilitiy_table_widget")

        self.vertical_layout_4.addWidget(self.facilitiy_table_widget)

        self.tab_widget.addTab(self.tab_3, "")
        self.tab_4 = QWidget()
        self.tab_4.setObjectName(u"tab_4")
        self.verticalLayout_2 = QVBoxLayout(self.tab_4)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.tableWidget_media = QTableWidget(self.tab_4)
        self.tableWidget_media.setObjectName(u"tableWidget_media")

        self.verticalLayout_2.addWidget(self.tableWidget_media)

        self.tab_widget.addTab(self.tab_4, "")
        self.tab_5 = QWidget()
        self.tab_5.setObjectName(u"tab_5")
        self.verticalLayout_3 = QVBoxLayout(self.tab_5)
        self.verticalLayout_3.setObjectName(u"verticalLayout_3")
        self.tableWidget_enviroment = QTableWidget(self.tab_5)
        self.tableWidget_enviroment.setObjectName(u"tableWidget_enviroment")

        self.verticalLayout_3.addWidget(self.tableWidget_enviroment)

        self.tab_widget.addTab(self.tab_5, "")

        self.verticalLayout.addWidget(self.tab_widget)

        self.horizontalLayout_2 = QHBoxLayout()
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.horizontalLayout_2.setContentsMargins(-1, 0, -1, -1)
        self.add_fac_pushButton = QPushButton(table_dialog)
        self.add_fac_pushButton.setObjectName(u"add_fac_pushButton")
        self.add_fac_pushButton.setEnabled(True)
        self.add_fac_pushButton.setCheckable(False)

        self.horizontalLayout_2.addWidget(self.add_fac_pushButton)

        self.rem_fac_pushButton = QPushButton(table_dialog)
        self.rem_fac_pushButton.setObjectName(u"rem_fac_pushButton")

        self.horizontalLayout_2.addWidget(self.rem_fac_pushButton)

        self.add_media_pushButton = QPushButton(table_dialog)
        self.add_media_pushButton.setObjectName(u"add_media_pushButton")

        self.horizontalLayout_2.addWidget(self.add_media_pushButton)

        self.delete_media_pushButton = QPushButton(table_dialog)
        self.delete_media_pushButton.setObjectName(u"delete_media_pushButton")

        self.horizontalLayout_2.addWidget(self.delete_media_pushButton)

        self.button_box = QDialogButtonBox(table_dialog)
        self.button_box.setObjectName(u"button_box")
        self.button_box.setOrientation(Qt.Horizontal)
        self.button_box.setStandardButtons(QDialogButtonBox.Cancel|QDialogButtonBox.Save)

        self.horizontalLayout_2.addWidget(self.button_box)


        self.verticalLayout.addLayout(self.horizontalLayout_2)


        self.retranslateUi(table_dialog)
        self.button_box.accepted.connect(table_dialog.accept)
        self.button_box.rejected.connect(table_dialog.reject)

        self.tab_widget.setCurrentIndex(4)


        QMetaObject.connectSlotsByName(table_dialog)
    # setupUi

    def retranslateUi(self, table_dialog):
        table_dialog.setWindowTitle(QCoreApplication.translate("table_dialog", u"Factory information", None))
        self.tab_widget.setTabText(self.tab_widget.indexOf(self.tab), QCoreApplication.translate("table_dialog", u"Material flow", None))
        self.tab_widget.setTabText(self.tab_widget.indexOf(self.tab_2), QCoreApplication.translate("table_dialog", u"Communication flow", None))
        self.tab_widget.setTabText(self.tab_widget.indexOf(self.tab_3), QCoreApplication.translate("table_dialog", u"Machine park", None))
        self.tab_widget.setTabText(self.tab_widget.indexOf(self.tab_4), QCoreApplication.translate("table_dialog", u"Medien", None))
        self.tab_widget.setTabText(self.tab_widget.indexOf(self.tab_5), QCoreApplication.translate("table_dialog", u"Umgebungsfaktoren", None))
#if QT_CONFIG(tooltip)
        self.add_fac_pushButton.setToolTip(QCoreApplication.translate("table_dialog", u"A new facility can be added here.", None))
#endif // QT_CONFIG(tooltip)
        self.add_fac_pushButton.setText(QCoreApplication.translate("table_dialog", u"Add Facility", None))
#if QT_CONFIG(tooltip)
        self.rem_fac_pushButton.setToolTip(QCoreApplication.translate("table_dialog", u"All marked facilities can be deleted here.", None))
#endif // QT_CONFIG(tooltip)
        self.rem_fac_pushButton.setText(QCoreApplication.translate("table_dialog", u"Delete Facility", None))
#if QT_CONFIG(tooltip)
        self.add_media_pushButton.setToolTip(QCoreApplication.translate("table_dialog", u"A new medium can be added to the factory and named here.", None))
#endif // QT_CONFIG(tooltip)
        self.add_media_pushButton.setText(QCoreApplication.translate("table_dialog", u"Add Media", None))
#if QT_CONFIG(tooltip)
        self.delete_media_pushButton.setToolTip(QCoreApplication.translate("table_dialog", u"All marked media can be deleted here.", None))
#endif // QT_CONFIG(tooltip)
        self.delete_media_pushButton.setText(QCoreApplication.translate("table_dialog", u"Delete Media", None))
    # retranslateUi

