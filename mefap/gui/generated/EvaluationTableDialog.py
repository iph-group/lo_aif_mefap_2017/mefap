# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'EvaluationTableDialog.ui'
##
## Created by: Qt User Interface Compiler version 5.15.0
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import (QCoreApplication, QDate, QDateTime, QMetaObject,
    QObject, QPoint, QRect, QSize, QTime, QUrl, Qt)
from PySide2.QtGui import (QBrush, QColor, QConicalGradient, QCursor, QFont,
    QFontDatabase, QIcon, QKeySequence, QLinearGradient, QPalette, QPainter,
    QPixmap, QRadialGradient)
from PySide2.QtWidgets import *


class Ui_evaluation_table_dialog(object):
    def setupUi(self, evaluation_table_dialog):
        if not evaluation_table_dialog.objectName():
            evaluation_table_dialog.setObjectName(u"evaluation_table_dialog")
        evaluation_table_dialog.resize(771, 471)
        self.verticalLayout = QVBoxLayout(evaluation_table_dialog)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.table_eval = QTableWidget(evaluation_table_dialog)
        self.table_eval.setObjectName(u"table_eval")

        self.verticalLayout.addWidget(self.table_eval)

        self.buttonBox = QDialogButtonBox(evaluation_table_dialog)
        self.buttonBox.setObjectName(u"buttonBox")
        self.buttonBox.setOrientation(Qt.Horizontal)
        self.buttonBox.setStandardButtons(QDialogButtonBox.Ok)

        self.verticalLayout.addWidget(self.buttonBox)


        self.retranslateUi(evaluation_table_dialog)
        self.buttonBox.accepted.connect(evaluation_table_dialog.accept)
        self.buttonBox.rejected.connect(evaluation_table_dialog.reject)

        QMetaObject.connectSlotsByName(evaluation_table_dialog)
    # setupUi

    def retranslateUi(self, evaluation_table_dialog):
        evaluation_table_dialog.setWindowTitle(QCoreApplication.translate("evaluation_table_dialog", u"Evaluation", None))
    # retranslateUi

