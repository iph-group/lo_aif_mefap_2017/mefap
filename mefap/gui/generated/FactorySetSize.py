# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'FactorySetSize.ui'
##
## Created by: Qt User Interface Compiler version 5.15.1
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_set_size_dialog(object):
    def setupUi(self, set_size_dialog):
        if not set_size_dialog.objectName():
            set_size_dialog.setObjectName(u"set_size_dialog")
        set_size_dialog.resize(674, 480)
        self.vertical_layout = QVBoxLayout(set_size_dialog)
        self.vertical_layout.setObjectName(u"vertical_layout")
        self.button_box = QDialogButtonBox(set_size_dialog)
        self.button_box.setObjectName(u"button_box")
        self.button_box.setOrientation(Qt.Horizontal)
        self.button_box.setStandardButtons(QDialogButtonBox.Cancel|QDialogButtonBox.Ok)

        self.vertical_layout.addWidget(self.button_box)


        self.retranslateUi(set_size_dialog)
        self.button_box.accepted.connect(set_size_dialog.accept)
        self.button_box.rejected.connect(set_size_dialog.reject)

        QMetaObject.connectSlotsByName(set_size_dialog)
    # setupUi

    def retranslateUi(self, set_size_dialog):
        set_size_dialog.setWindowTitle(QCoreApplication.translate("set_size_dialog", u"Set size", None))
    # retranslateUi

