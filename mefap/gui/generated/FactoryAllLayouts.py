# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'FactoryAllLayouts.ui'
##
## Created by: Qt User Interface Compiler version 5.15.1
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_Factory_all_layouts(object):
    def setupUi(self, Factory_all_layouts):
        if not Factory_all_layouts.objectName():
            Factory_all_layouts.setObjectName(u"Factory_all_layouts")
        Factory_all_layouts.resize(584, 403)
        self.verticalLayout = QVBoxLayout(Factory_all_layouts)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.listWidget = QListWidget(Factory_all_layouts)
        self.listWidget.setObjectName(u"listWidget")

        self.verticalLayout.addWidget(self.listWidget)

        self.buttonBox = QDialogButtonBox(Factory_all_layouts)
        self.buttonBox.setObjectName(u"buttonBox")
        self.buttonBox.setOrientation(Qt.Horizontal)
        self.buttonBox.setStandardButtons(QDialogButtonBox.Cancel|QDialogButtonBox.Ok)

        self.verticalLayout.addWidget(self.buttonBox)


        self.retranslateUi(Factory_all_layouts)
        self.buttonBox.accepted.connect(Factory_all_layouts.accept)
        self.buttonBox.rejected.connect(Factory_all_layouts.reject)

        QMetaObject.connectSlotsByName(Factory_all_layouts)
    # setupUi

    def retranslateUi(self, Factory_all_layouts):
        Factory_all_layouts.setWindowTitle(QCoreApplication.translate("Factory_all_layouts", u"All Layouts", None))
    # retranslateUi

