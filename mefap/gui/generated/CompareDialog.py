# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'CompareDialog.ui'
##
## Created by: Qt User Interface Compiler version 5.15.0
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import (QCoreApplication, QDate, QDateTime, QMetaObject,
    QObject, QPoint, QRect, QSize, QTime, QUrl, Qt)
from PySide2.QtGui import (QBrush, QColor, QConicalGradient, QCursor, QFont,
    QFontDatabase, QIcon, QKeySequence, QLinearGradient, QPalette, QPainter,
    QPixmap, QRadialGradient)
from PySide2.QtWidgets import *


class Ui_compare_dialog(object):
    def setupUi(self, compare_dialog):
        if not compare_dialog.objectName():
            compare_dialog.setObjectName(u"compare_dialog")
        compare_dialog.resize(871, 700)
        self.verticalLayout = QVBoxLayout(compare_dialog)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.label = QLabel(compare_dialog)
        self.label.setObjectName(u"label")
        self.label.setAlignment(Qt.AlignCenter)

        self.verticalLayout.addWidget(self.label)

        self.scrollArea = QScrollArea(compare_dialog)
        self.scrollArea.setObjectName(u"scrollArea")
        self.scrollArea.setWidgetResizable(True)
        self.scrollAreaWidgetContents = QWidget()
        self.scrollAreaWidgetContents.setObjectName(u"scrollAreaWidgetContents")
        self.scrollAreaWidgetContents.setGeometry(QRect(0, 0, 845, 595))
        self.verticalLayout_2 = QVBoxLayout(self.scrollAreaWidgetContents)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.gridLayout = QGridLayout()
        self.gridLayout.setObjectName(u"gridLayout")

        self.verticalLayout_2.addLayout(self.gridLayout)

        self.scrollArea.setWidget(self.scrollAreaWidgetContents)

        self.verticalLayout.addWidget(self.scrollArea)

        self.buttonBox = QDialogButtonBox(compare_dialog)
        self.buttonBox.setObjectName(u"buttonBox")
        self.buttonBox.setStandardButtons(QDialogButtonBox.Ok)

        self.verticalLayout.addWidget(self.buttonBox)


        self.retranslateUi(compare_dialog)

        QMetaObject.connectSlotsByName(compare_dialog)
    # setupUi

    def retranslateUi(self, compare_dialog):
        compare_dialog.setWindowTitle(QCoreApplication.translate("compare_dialog", u"Results", None))
        self.label.setText(QCoreApplication.translate("compare_dialog", u"<h1>Results</h1>\n"
"", None))
    # retranslateUi

