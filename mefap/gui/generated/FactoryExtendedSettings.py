# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'FactoryExtendedSettingswiuFGQ.ui'
##
## Created by: Qt User Interface Compiler version 6.2.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide2.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide2.QtWidgets import (QAbstractButton, QApplication, QCheckBox, QComboBox,
    QDialog, QDialogButtonBox, QDoubleSpinBox, QFormLayout,
    QLabel, QRadioButton, QSizePolicy, QSpinBox,
    QTabWidget, QVBoxLayout, QWidget)

class Ui_FactoryExtendedSettings(object):
    def setupUi(self, FactoryExtendedSettings):
        if not FactoryExtendedSettings.objectName():
            FactoryExtendedSettings.setObjectName(u"FactoryExtendedSettings")
        FactoryExtendedSettings.resize(714, 610)
        FactoryExtendedSettings.setSizeGripEnabled(False)
        FactoryExtendedSettings.setModal(False)
        self.verticalLayout = QVBoxLayout(FactoryExtendedSettings)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.tabWidget = QTabWidget(FactoryExtendedSettings)
        self.tabWidget.setObjectName(u"tabWidget")
        self.tab_algo = QWidget()
        self.tab_algo.setObjectName(u"tab_algo")
        self.formLayout_2 = QFormLayout(self.tab_algo)
        self.formLayout_2.setObjectName(u"formLayout_2")
        self.label = QLabel(self.tab_algo)
        self.label.setObjectName(u"label")

        self.formLayout_2.setWidget(0, QFormLayout.LabelRole, self.label)

        self.label_2 = QLabel(self.tab_algo)
        self.label_2.setObjectName(u"label_2")

        self.formLayout_2.setWidget(1, QFormLayout.LabelRole, self.label_2)

        self.label_3 = QLabel(self.tab_algo)
        self.label_3.setObjectName(u"label_3")

        self.formLayout_2.setWidget(2, QFormLayout.LabelRole, self.label_3)

        self.doubleSpinBox_empty_cell_costs = QDoubleSpinBox(self.tab_algo)
        self.doubleSpinBox_empty_cell_costs.setObjectName(u"doubleSpinBox_empty_cell_costs")
        self.doubleSpinBox_empty_cell_costs.setMaximum(100.000000000000000)

        self.formLayout_2.setWidget(0, QFormLayout.FieldRole, self.doubleSpinBox_empty_cell_costs)

        self.doubleSpinBox_local_road_costs = QDoubleSpinBox(self.tab_algo)
        self.doubleSpinBox_local_road_costs.setObjectName(u"doubleSpinBox_local_road_costs")
        self.doubleSpinBox_local_road_costs.setMaximum(100.000000000000000)

        self.formLayout_2.setWidget(1, QFormLayout.FieldRole, self.doubleSpinBox_local_road_costs)

        self.doubleSpinBox_fixed_road_costs = QDoubleSpinBox(self.tab_algo)
        self.doubleSpinBox_fixed_road_costs.setObjectName(u"doubleSpinBox_fixed_road_costs")
        self.doubleSpinBox_fixed_road_costs.setMaximum(100.000000000000000)

        self.formLayout_2.setWidget(2, QFormLayout.FieldRole, self.doubleSpinBox_fixed_road_costs)

        self.tabWidget.addTab(self.tab_algo, "")
        self.tab_media = QWidget()
        self.tab_media.setObjectName(u"tab_media")
        self.formLayout = QFormLayout(self.tab_media)
        self.formLayout.setObjectName(u"formLayout")
        self.label_4 = QLabel(self.tab_media)
        self.label_4.setObjectName(u"label_4")

        self.formLayout.setWidget(0, QFormLayout.LabelRole, self.label_4)

        self.doubleSpinBox_media_avail_factor = QDoubleSpinBox(self.tab_media)
        self.doubleSpinBox_media_avail_factor.setObjectName(u"doubleSpinBox_media_avail_factor")
        self.doubleSpinBox_media_avail_factor.setMaximum(1.000000000000000)
        self.doubleSpinBox_media_avail_factor.setSingleStep(0.100000000000000)

        self.formLayout.setWidget(0, QFormLayout.FieldRole, self.doubleSpinBox_media_avail_factor)

        self.tabWidget.addTab(self.tab_media, "")
        self.tab_lightning = QWidget()
        self.tab_lightning.setObjectName(u"tab_lightning")
        self.formLayout_3 = QFormLayout(self.tab_lightning)
        self.formLayout_3.setObjectName(u"formLayout_3")
        self.label_5 = QLabel(self.tab_lightning)
        self.label_5.setObjectName(u"label_5")

        self.formLayout_3.setWidget(0, QFormLayout.LabelRole, self.label_5)

        self.comboBox_road_illuminance = QComboBox(self.tab_lightning)
        self.comboBox_road_illuminance.addItem("")
        self.comboBox_road_illuminance.addItem("")
        self.comboBox_road_illuminance.addItem("")
        self.comboBox_road_illuminance.addItem("")
        self.comboBox_road_illuminance.addItem("")
        self.comboBox_road_illuminance.addItem("")
        self.comboBox_road_illuminance.addItem("")
        self.comboBox_road_illuminance.setObjectName(u"comboBox_road_illuminance")
        self.comboBox_road_illuminance.setMinimumSize(QSize(200, 0))

        self.formLayout_3.setWidget(0, QFormLayout.FieldRole, self.comboBox_road_illuminance)

        self.tabWidget.addTab(self.tab_lightning, "")
        self.tab_quiet = QWidget()
        self.tab_quiet.setObjectName(u"tab_quiet")
        self.formLayout_4 = QFormLayout(self.tab_quiet)
        self.formLayout_4.setObjectName(u"formLayout_4")
        self.label_6 = QLabel(self.tab_quiet)
        self.label_6.setObjectName(u"label_6")

        self.formLayout_4.setWidget(0, QFormLayout.LabelRole, self.label_6)

        self.label_7 = QLabel(self.tab_quiet)
        self.label_7.setObjectName(u"label_7")

        self.formLayout_4.setWidget(1, QFormLayout.LabelRole, self.label_7)

        self.spinBox_sound_pressure_reduction = QSpinBox(self.tab_quiet)
        self.spinBox_sound_pressure_reduction.setObjectName(u"spinBox_sound_pressure_reduction")
        self.spinBox_sound_pressure_reduction.setMinimumSize(QSize(54, 0))
        self.spinBox_sound_pressure_reduction.setMaximum(100)

        self.formLayout_4.setWidget(0, QFormLayout.FieldRole, self.spinBox_sound_pressure_reduction)

        self.spinBox_sound_propagation_conditions = QSpinBox(self.tab_quiet)
        self.spinBox_sound_propagation_conditions.setObjectName(u"spinBox_sound_propagation_conditions")
        self.spinBox_sound_propagation_conditions.setMinimumSize(QSize(54, 0))
        self.spinBox_sound_propagation_conditions.setMaximum(100)

        self.formLayout_4.setWidget(1, QFormLayout.FieldRole, self.spinBox_sound_propagation_conditions)

        self.label_15 = QLabel(self.tab_quiet)
        self.label_15.setObjectName(u"label_15")

        self.formLayout_4.setWidget(2, QFormLayout.LabelRole, self.label_15)

        self.checkBox_noise_calc_wall_corner = QCheckBox(self.tab_quiet)
        self.checkBox_noise_calc_wall_corner.setObjectName(u"checkBox_noise_calc_wall_corner")

        self.formLayout_4.setWidget(2, QFormLayout.FieldRole, self.checkBox_noise_calc_wall_corner)

        self.label_17 = QLabel(self.tab_quiet)
        self.label_17.setObjectName(u"label_17")

        self.formLayout_4.setWidget(4, QFormLayout.LabelRole, self.label_17)

        self.label_18 = QLabel(self.tab_quiet)
        self.label_18.setObjectName(u"label_18")

        self.formLayout_4.setWidget(6, QFormLayout.LabelRole, self.label_18)

        self.label_19 = QLabel(self.tab_quiet)
        self.label_19.setObjectName(u"label_19")

        self.formLayout_4.setWidget(7, QFormLayout.LabelRole, self.label_19)

        self.label_20 = QLabel(self.tab_quiet)
        self.label_20.setObjectName(u"label_20")

        self.formLayout_4.setWidget(8, QFormLayout.LabelRole, self.label_20)

        self.label_21 = QLabel(self.tab_quiet)
        self.label_21.setObjectName(u"label_21")

        self.formLayout_4.setWidget(9, QFormLayout.LabelRole, self.label_21)

        self.label_22 = QLabel(self.tab_quiet)
        self.label_22.setObjectName(u"label_22")

        self.formLayout_4.setWidget(10, QFormLayout.LabelRole, self.label_22)

        self.label_23 = QLabel(self.tab_quiet)
        self.label_23.setObjectName(u"label_23")

        self.formLayout_4.setWidget(11, QFormLayout.LabelRole, self.label_23)

        self.label_24 = QLabel(self.tab_quiet)
        self.label_24.setObjectName(u"label_24")

        self.formLayout_4.setWidget(12, QFormLayout.LabelRole, self.label_24)

        self.label_25 = QLabel(self.tab_quiet)
        self.label_25.setObjectName(u"label_25")

        self.formLayout_4.setWidget(13, QFormLayout.LabelRole, self.label_25)

        self.label_26 = QLabel(self.tab_quiet)
        self.label_26.setObjectName(u"label_26")

        self.formLayout_4.setWidget(14, QFormLayout.LabelRole, self.label_26)

        self.label_27 = QLabel(self.tab_quiet)
        self.label_27.setObjectName(u"label_27")

        self.formLayout_4.setWidget(15, QFormLayout.LabelRole, self.label_27)

        self.label_28 = QLabel(self.tab_quiet)
        self.label_28.setObjectName(u"label_28")

        self.formLayout_4.setWidget(16, QFormLayout.LabelRole, self.label_28)

        self.radioButton_noise_flat_room = QRadioButton(self.tab_quiet)
        self.radioButton_noise_flat_room.setObjectName(u"radioButton_noise_flat_room")

        self.formLayout_4.setWidget(7, QFormLayout.FieldRole, self.radioButton_noise_flat_room)

        self.radioButton_noise_semi_diffuse = QRadioButton(self.tab_quiet)
        self.radioButton_noise_semi_diffuse.setObjectName(u"radioButton_noise_semi_diffuse")

        self.formLayout_4.setWidget(6, QFormLayout.FieldRole, self.radioButton_noise_semi_diffuse)

        self.spinBox_noise_encapsulation = QSpinBox(self.tab_quiet)
        self.spinBox_noise_encapsulation.setObjectName(u"spinBox_noise_encapsulation")
        self.spinBox_noise_encapsulation.setMinimumSize(QSize(54, 0))
        self.spinBox_noise_encapsulation.setMaximum(999)

        self.formLayout_4.setWidget(9, QFormLayout.FieldRole, self.spinBox_noise_encapsulation)

        self.spinBox_noise_barrier_isolation = QSpinBox(self.tab_quiet)
        self.spinBox_noise_barrier_isolation.setObjectName(u"spinBox_noise_barrier_isolation")
        self.spinBox_noise_barrier_isolation.setMinimumSize(QSize(54, 0))
        self.spinBox_noise_barrier_isolation.setMaximum(999)

        self.formLayout_4.setWidget(10, QFormLayout.FieldRole, self.spinBox_noise_barrier_isolation)

        self.spinBox_noise_factory_wall_isolation = QSpinBox(self.tab_quiet)
        self.spinBox_noise_factory_wall_isolation.setObjectName(u"spinBox_noise_factory_wall_isolation")
        self.spinBox_noise_factory_wall_isolation.setMinimumSize(QSize(54, 0))
        self.spinBox_noise_factory_wall_isolation.setMaximum(999)

        self.formLayout_4.setWidget(11, QFormLayout.FieldRole, self.spinBox_noise_factory_wall_isolation)

        self.doubleSpinBox_noise_absorption_coefficient = QDoubleSpinBox(self.tab_quiet)
        self.doubleSpinBox_noise_absorption_coefficient.setObjectName(u"doubleSpinBox_noise_absorption_coefficient")
        self.doubleSpinBox_noise_absorption_coefficient.setMinimumSize(QSize(54, 0))
        self.doubleSpinBox_noise_absorption_coefficient.setMaximum(1.000000000000000)
        self.doubleSpinBox_noise_absorption_coefficient.setSingleStep(0.010000000000000)

        self.formLayout_4.setWidget(13, QFormLayout.FieldRole, self.doubleSpinBox_noise_absorption_coefficient)

        self.doubleSpinBox_noise_absorption_floor = QDoubleSpinBox(self.tab_quiet)
        self.doubleSpinBox_noise_absorption_floor.setObjectName(u"doubleSpinBox_noise_absorption_floor")
        self.doubleSpinBox_noise_absorption_floor.setMinimumSize(QSize(54, 0))
        self.doubleSpinBox_noise_absorption_floor.setMaximum(1.000000000000000)
        self.doubleSpinBox_noise_absorption_floor.setSingleStep(0.010000000000000)

        self.formLayout_4.setWidget(14, QFormLayout.FieldRole, self.doubleSpinBox_noise_absorption_floor)

        self.doubleSpinBox_noise_absorption_ceil = QDoubleSpinBox(self.tab_quiet)
        self.doubleSpinBox_noise_absorption_ceil.setObjectName(u"doubleSpinBox_noise_absorption_ceil")
        self.doubleSpinBox_noise_absorption_ceil.setMinimumSize(QSize(54, 0))
        self.doubleSpinBox_noise_absorption_ceil.setMaximum(1.000000000000000)
        self.doubleSpinBox_noise_absorption_ceil.setSingleStep(0.010000000000000)

        self.formLayout_4.setWidget(15, QFormLayout.FieldRole, self.doubleSpinBox_noise_absorption_ceil)

        self.doubleSpinBox_noise_absorption_wall = QDoubleSpinBox(self.tab_quiet)
        self.doubleSpinBox_noise_absorption_wall.setObjectName(u"doubleSpinBox_noise_absorption_wall")
        self.doubleSpinBox_noise_absorption_wall.setMinimumSize(QSize(54, 0))
        self.doubleSpinBox_noise_absorption_wall.setMaximum(1.000000000000000)
        self.doubleSpinBox_noise_absorption_wall.setSingleStep(0.010000000000000)

        self.formLayout_4.setWidget(16, QFormLayout.FieldRole, self.doubleSpinBox_noise_absorption_wall)

        self.label_29 = QLabel(self.tab_quiet)
        self.label_29.setObjectName(u"label_29")

        self.formLayout_4.setWidget(5, QFormLayout.LabelRole, self.label_29)

        self.radioButton_none = QRadioButton(self.tab_quiet)
        self.radioButton_none.setObjectName(u"radioButton_none")

        self.formLayout_4.setWidget(5, QFormLayout.FieldRole, self.radioButton_none)

        self.label_30 = QLabel(self.tab_quiet)
        self.label_30.setObjectName(u"label_30")

        self.formLayout_4.setWidget(3, QFormLayout.LabelRole, self.label_30)

        self.checkBox_color_facilities_by_noise_exposure = QCheckBox(self.tab_quiet)
        self.checkBox_color_facilities_by_noise_exposure.setObjectName(u"checkBox_color_facilities_by_noise_exposure")

        self.formLayout_4.setWidget(3, QFormLayout.FieldRole, self.checkBox_color_facilities_by_noise_exposure)

        self.tabWidget.addTab(self.tab_quiet, "")
        self.tab_5 = QWidget()
        self.tab_5.setObjectName(u"tab_5")
        self.formLayout_5 = QFormLayout(self.tab_5)
        self.formLayout_5.setObjectName(u"formLayout_5")
        self.label_8 = QLabel(self.tab_5)
        self.label_8.setObjectName(u"label_8")

        self.formLayout_5.setWidget(0, QFormLayout.LabelRole, self.label_8)

        self.spinBox_lrs_search_steps = QSpinBox(self.tab_5)
        self.spinBox_lrs_search_steps.setObjectName(u"spinBox_lrs_search_steps")
        self.spinBox_lrs_search_steps.setMaximum(9999999)

        self.formLayout_5.setWidget(0, QFormLayout.FieldRole, self.spinBox_lrs_search_steps)

        self.label_9 = QLabel(self.tab_5)
        self.label_9.setObjectName(u"label_9")

        self.formLayout_5.setWidget(1, QFormLayout.LabelRole, self.label_9)

        self.spinBox_lrs_step_size = QSpinBox(self.tab_5)
        self.spinBox_lrs_step_size.setObjectName(u"spinBox_lrs_step_size")
        self.spinBox_lrs_step_size.setMinimumSize(QSize(54, 0))
        self.spinBox_lrs_step_size.setMaximum(500)

        self.formLayout_5.setWidget(1, QFormLayout.FieldRole, self.spinBox_lrs_step_size)

        self.label_10 = QLabel(self.tab_5)
        self.label_10.setObjectName(u"label_10")

        self.formLayout_5.setWidget(2, QFormLayout.LabelRole, self.label_10)

        self.spinBox_mees_facility_range_from = QSpinBox(self.tab_5)
        self.spinBox_mees_facility_range_from.setObjectName(u"spinBox_mees_facility_range_from")
        self.spinBox_mees_facility_range_from.setMinimumSize(QSize(54, 0))
        self.spinBox_mees_facility_range_from.setMinimum(2)
        self.spinBox_mees_facility_range_from.setMaximum(500)

        self.formLayout_5.setWidget(2, QFormLayout.FieldRole, self.spinBox_mees_facility_range_from)

        self.label_11 = QLabel(self.tab_5)
        self.label_11.setObjectName(u"label_11")

        self.formLayout_5.setWidget(3, QFormLayout.LabelRole, self.label_11)

        self.spinBox_mees_facility_range_to = QSpinBox(self.tab_5)
        self.spinBox_mees_facility_range_to.setObjectName(u"spinBox_mees_facility_range_to")
        self.spinBox_mees_facility_range_to.setMinimumSize(QSize(54, 0))
        self.spinBox_mees_facility_range_to.setMaximum(500)

        self.formLayout_5.setWidget(3, QFormLayout.FieldRole, self.spinBox_mees_facility_range_to)

        self.label_12 = QLabel(self.tab_5)
        self.label_12.setObjectName(u"label_12")

        self.formLayout_5.setWidget(5, QFormLayout.LabelRole, self.label_12)

        self.doubleSpinBox_mees_area_difference = QDoubleSpinBox(self.tab_5)
        self.doubleSpinBox_mees_area_difference.setObjectName(u"doubleSpinBox_mees_area_difference")
        self.doubleSpinBox_mees_area_difference.setMinimumSize(QSize(54, 0))
        self.doubleSpinBox_mees_area_difference.setMaximum(1.000000000000000)
        self.doubleSpinBox_mees_area_difference.setSingleStep(0.100000000000000)

        self.formLayout_5.setWidget(5, QFormLayout.FieldRole, self.doubleSpinBox_mees_area_difference)

        self.label_13 = QLabel(self.tab_5)
        self.label_13.setObjectName(u"label_13")

        self.formLayout_5.setWidget(6, QFormLayout.LabelRole, self.label_13)

        self.spinBox_fas_order = QSpinBox(self.tab_5)
        self.spinBox_fas_order.setObjectName(u"spinBox_fas_order")
        self.spinBox_fas_order.setMinimumSize(QSize(54, 0))
        self.spinBox_fas_order.setMaximum(1)

        self.formLayout_5.setWidget(6, QFormLayout.FieldRole, self.spinBox_fas_order)

        self.label_14 = QLabel(self.tab_5)
        self.label_14.setObjectName(u"label_14")

        self.formLayout_5.setWidget(4, QFormLayout.LabelRole, self.label_14)

        self.spinBox_mees_step_size = QSpinBox(self.tab_5)
        self.spinBox_mees_step_size.setObjectName(u"spinBox_mees_step_size")
        self.spinBox_mees_step_size.setMinimumSize(QSize(54, 0))
        self.spinBox_mees_step_size.setMaximum(500)

        self.formLayout_5.setWidget(4, QFormLayout.FieldRole, self.spinBox_mees_step_size)

        self.label_151 = QLabel(self.tab_5)
        self.label_151.setObjectName(u"label_151")

        self.formLayout_5.setWidget(7, QFormLayout.LabelRole, self.label_151)

        self.spinBox_termination = QSpinBox(self.tab_5)
        self.spinBox_termination.setObjectName(u"spinBox_termination")
        self.spinBox_termination.setMinimumSize(QSize(54, 0))
        self.spinBox_termination.setMinimum(1)
        self.spinBox_termination.setMaximum(999)

        self.formLayout_5.setWidget(7, QFormLayout.FieldRole, self.spinBox_termination)

        self.label_16 = QLabel(self.tab_5)
        self.label_16.setObjectName(u"label_16")

        self.formLayout_5.setWidget(8, QFormLayout.LabelRole, self.label_16)

        self.spinBox_number_of_experiments = QSpinBox(self.tab_5)
        self.spinBox_number_of_experiments.setObjectName(u"spinBox_number_of_experiments")
        self.spinBox_number_of_experiments.setMinimumSize(QSize(54, 0))
        self.spinBox_number_of_experiments.setMinimum(1)
        self.spinBox_number_of_experiments.setMaximum(999)

        self.formLayout_5.setWidget(8, QFormLayout.FieldRole, self.spinBox_number_of_experiments)

        self.tabWidget.addTab(self.tab_5, "")

        self.verticalLayout.addWidget(self.tabWidget)

        self.buttonBox = QDialogButtonBox(FactoryExtendedSettings)
        self.buttonBox.setObjectName(u"buttonBox")
        self.buttonBox.setOrientation(Qt.Horizontal)
        self.buttonBox.setStandardButtons(QDialogButtonBox.Cancel|QDialogButtonBox.Ok)

        self.verticalLayout.addWidget(self.buttonBox)


        self.retranslateUi(FactoryExtendedSettings)
        self.buttonBox.accepted.connect(FactoryExtendedSettings.accept)
        self.buttonBox.rejected.connect(FactoryExtendedSettings.reject)

        self.tabWidget.setCurrentIndex(3)


        QMetaObject.connectSlotsByName(FactoryExtendedSettings)
    # setupUi

    def retranslateUi(self, FactoryExtendedSettings):
        FactoryExtendedSettings.setWindowTitle(QCoreApplication.translate("FactoryExtendedSettings", u"Extended Settings", None))
        self.label.setText(QCoreApplication.translate("FactoryExtendedSettings", u"Empty cell costs", None))
        self.label_2.setText(QCoreApplication.translate("FactoryExtendedSettings", u"Local road costs", None))
        self.label_3.setText(QCoreApplication.translate("FactoryExtendedSettings", u"Fixed road costs", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_algo), QCoreApplication.translate("FactoryExtendedSettings", u"A*-Algorithmus", None))
        self.label_4.setText(QCoreApplication.translate("FactoryExtendedSettings", u"media availability factor", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_media), QCoreApplication.translate("FactoryExtendedSettings", u"Medienverfuegbarkeit", None))
        self.label_5.setText(QCoreApplication.translate("FactoryExtendedSettings", u"Road illuminance", None))
        self.comboBox_road_illuminance.setItemText(0, QCoreApplication.translate("FactoryExtendedSettings", u"Kein Licht [< 100lx]", None))
        self.comboBox_road_illuminance.setItemText(1, QCoreApplication.translate("FactoryExtendedSettings", u"Sehr wenig Licht [100 - 200 lx]", None))
        self.comboBox_road_illuminance.setItemText(2, QCoreApplication.translate("FactoryExtendedSettings", u"Wenig Licht 200 - 300lx]", None))
        self.comboBox_road_illuminance.setItemText(3, QCoreApplication.translate("FactoryExtendedSettings", u"Normales Licht [300 - 500 lx]", None))
        self.comboBox_road_illuminance.setItemText(4, QCoreApplication.translate("FactoryExtendedSettings", u"Helles Licht [500 - 750 lx]", None))
        self.comboBox_road_illuminance.setItemText(5, QCoreApplication.translate("FactoryExtendedSettings", u"Sehr helles Licht [750 - 1000 lx]", None))
        self.comboBox_road_illuminance.setItemText(6, QCoreApplication.translate("FactoryExtendedSettings", u"maximales Licht [1000 - 1500 lx]", None))

        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_lightning), QCoreApplication.translate("FactoryExtendedSettings", u"Beleuchtung", None))
        self.label_6.setText(QCoreApplication.translate("FactoryExtendedSettings", u"sound pressure reduction", None))
        self.label_7.setText(QCoreApplication.translate("FactoryExtendedSettings", u"Sound propagation conditions", None))
        self.label_15.setText(QCoreApplication.translate("FactoryExtendedSettings", u"Sound reflection on walls", None))
        self.checkBox_noise_calc_wall_corner.setText("")
        self.label_17.setText(QCoreApplication.translate("FactoryExtendedSettings", u"<h2>Sound propagation</h2>", None))
        self.label_18.setText(QCoreApplication.translate("FactoryExtendedSettings", u"Semi diffuse", None))
        self.label_19.setText(QCoreApplication.translate("FactoryExtendedSettings", u"Flat room", None))
        self.label_20.setText(QCoreApplication.translate("FactoryExtendedSettings", u"<h2>Sound reduction</h2>\n"
"\n"
"", None))
        self.label_21.setText(QCoreApplication.translate("FactoryExtendedSettings", u"Encapsulation", None))
        self.label_22.setText(QCoreApplication.translate("FactoryExtendedSettings", u"Barrier isolation", None))
        self.label_23.setText(QCoreApplication.translate("FactoryExtendedSettings", u"Factory wall isolation", None))
        self.label_24.setText(QCoreApplication.translate("FactoryExtendedSettings", u"<h2>Sound absorption</h2>\n"
"", None))
        self.label_25.setText(QCoreApplication.translate("FactoryExtendedSettings", u"Absorption coefficient", None))
        self.label_26.setText(QCoreApplication.translate("FactoryExtendedSettings", u"Absorption floor", None))
        self.label_27.setText(QCoreApplication.translate("FactoryExtendedSettings", u"Absorption ceil", None))
        self.label_28.setText(QCoreApplication.translate("FactoryExtendedSettings", u"Absorption wall", None))
        self.radioButton_noise_flat_room.setText("")
        self.radioButton_noise_semi_diffuse.setText("")
        self.label_29.setText(QCoreApplication.translate("FactoryExtendedSettings", u"None", None))
        self.radioButton_none.setText("")
        self.label_30.setText(QCoreApplication.translate("FactoryExtendedSettings", u"Color facilties by noise exposure", None))
        self.checkBox_color_facilities_by_noise_exposure.setText("")
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_quiet), QCoreApplication.translate("FactoryExtendedSettings", u"Ruhe", None))
        self.label_8.setText(QCoreApplication.translate("FactoryExtendedSettings", u"lrs search steps", None))
        self.label_9.setText(QCoreApplication.translate("FactoryExtendedSettings", u"lrs step size", None))
        self.label_10.setText(QCoreApplication.translate("FactoryExtendedSettings", u"mees facility range from", None))
        self.label_11.setText(QCoreApplication.translate("FactoryExtendedSettings", u"mees facility range to", None))
        self.label_12.setText(QCoreApplication.translate("FactoryExtendedSettings", u"mees area difference", None))
        self.label_13.setText(QCoreApplication.translate("FactoryExtendedSettings", u"fas order", None))
        self.label_14.setText(QCoreApplication.translate("FactoryExtendedSettings", u"mees step size", None))
        self.label_151.setText(QCoreApplication.translate("FactoryExtendedSettings", u"termination criterion", None))
        self.label_16.setText(QCoreApplication.translate("FactoryExtendedSettings", u"Number of experiments", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_5), QCoreApplication.translate("FactoryExtendedSettings", u"Parameter der Optimierung", None))
    # retranslateUi

