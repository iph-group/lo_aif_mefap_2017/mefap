import logging
from typing import List, Tuple

from PySide2.QtWidgets import QGraphicsScene, QGraphicsSimpleTextItem, QGraphicsItem

from mefap.gui.compare_facility import CompareFacility
from mefap.gui.factory_cell import FactoryCell
from mefap.gui.factory_facility import FactoryFacility


class CompareLayoutScene(QGraphicsScene):
    """
    The FactoryLayoutScene inherits the QGraphicsScene and will be shown by the FactoryView.
    It will manage all objects of the factory representation.
    By using different internal modes, this scene can represent
    different views on factory attributs.
    """

    def __init__(self, grid, facilities, factory, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.cells: List[FactoryCell] = []
        self.facilities: List[FactoryFacility] = []
        self.text_objects: List[QGraphicsSimpleTextItem] = []

        self.factory = factory

        self.parameter = factory.parameter
        self.layout = grid
        self.facility_list = facilities

        self.cell_size = factory.cell_size
        self.cell_zoom = factory.cell_zoom

        self.cell_size = factory.cell_size
        self.cell_zoom = factory.cell_zoom

    def init_grid(self) -> None:
        """
        Initialises grid.
        :return: None
        """
        logging.info("Initialising grid")
        self.cells = None
        self.clear()
        self.cells = self.generate_cells()
        for cell in self.cells:
            self.addItem(cell)
            cell.update_path_with_cell_value(self.layout[cell.y_cord][cell.x_cord])

    def generate_cells(self) -> List[FactoryCell]:
        """
        Generated empty cells for the grid.
        :return: A List of FactoryCells.
        """
        logging.info("Generating Cells")
        cells = []
        for y_cord in range(self.layout.shape[0]):
            for x_cord in range(self.layout.shape[1]):
                if self.layout[y_cord][x_cord] != self.parameter["hidden_cell_value"]:
                    cell = FactoryCell(x_cord, y_cord, self.factory.parameter,
                                       x_cord * self.cell_zoom,
                                       y_cord * self.cell_zoom,
                                       self.cell_zoom,
                                       self.cell_zoom)
                    pen = cell.pen()
                    pen.setWidth(1)
                    cell.setPen(pen)
                    cells.append(cell)
        return cells

    def transparent_facilities(self) -> None:
        """
        This method sets all Facility objects in the scene in transparency mode (not movable)
        :return: None
        """
        for facility in self.facilities:
            facility.setOpacity(0.4)
            facility.setFlag(QGraphicsItem.ItemIsMovable, False)
            facility.generate_sink_source()

    def lock_facilities(self) -> None:
        """
        This method sets all Facility objects in the scene to not movable.
        :return: None
        """
        for facility in self.facilities:
            facility.setFlag(QGraphicsItem.ItemIsMovable, False)

    def activate_facilities(self) -> None:
        """
        This method set all Facility objects in the scene in active mode (movable)
        :return: None
        """
        for facility in self.facilities:
            facility.setOpacity(1)
            facility.setFlag(QGraphicsItem.ItemIsMovable, True)
            facility.generate_sink_source()

    def load_facilities(self) -> None:
        """
        This method will be called at loading a existing factory.
        Depending on the layout matrix the already placed facilities will be shown.
        :return: None
        """
        positions = self.get_upper_left_of_placed_facilities()
        for position in positions:
            for facility in self.facility_list:
                if facility.index_num == position[1]:
                    for cell in self.cells:
                        if cell.x_cord == position[0][0] and cell.y_cord == position[0][1]:
                            facilityitem = CompareFacility(facility, self.cell_size, self.cell_zoom,
                                                           cell.boundingRect().topLeft().x() + 1.5,
                                                           cell.boundingRect().topLeft().y() + 1.5,
                                                           facility.cell_width1 * cell.boundingRect().width() - facility.cell_width1 - 2,
                                                           facility.cell_width2 * cell.boundingRect().height() - facility.cell_width2 - 2)
                            self.addItem(facilityitem)
                            facilityitem.rotate_after_loading()
                            self.facilities.append(facilityitem)
                            facilityitem.snap_to_grid()
                            break
                    break

    def get_upper_left_of_placed_facilities(self) -> List[Tuple[Tuple[int, int], int]]:
        """
        Returns a list of all upper left position for each facility
        :return: List of Tuples. The Tuple contains the position and the index of the facility.
        """
        result: List[Tuple[Tuple[int, int], int]] = []
        found: List[int] = []
        for row_idx, row in enumerate(self.layout):
            for column_idx, column in enumerate(row):
                if column >= 0 and int(column) not in found:
                    result.append(((column_idx, row_idx), int(column)))
                    found.append(int(column))
        return result
