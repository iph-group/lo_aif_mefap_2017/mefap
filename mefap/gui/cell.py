from PySide2.QtGui import QBrush
from PySide2.QtWidgets import QGraphicsRectItem

from mefap.gui.util.colors import EMPTY_CELL


class Cell(QGraphicsRectItem):
    """
    This Object representing a cell on the grid.
    Its x and y coordinates define where in the grid the cell resides.
    """

    def __init__(self, x_c, y_c, *args, **kwargs):
        QGraphicsRectItem.__init__(self, *args, **kwargs)

        # Position inside the layout matrix
        self.x_cord = x_c
        self.y_cord = y_c

        # Color of this cell
        self.setBrush(QBrush(EMPTY_CELL))
