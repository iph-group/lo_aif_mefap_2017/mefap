import math
import random

from PySide2.QtCore import Slot
from PySide2.QtWidgets import QDialog, QInputDialog, QDialogButtonBox

from mefap.factory.facility_attributs import FacilityAttributes
from mefap.factory.facility_characteristics import FacilityCharacteristics
from mefap.factory.factory_model import FactoryModel
from mefap.factory.util.light_level import LightLevel
from mefap.gui.generated.AddFacilityDialog import Ui_AddFacilityDialog


class FactoryAddFacilityDialog(QDialog):
    """
    In this Dialog the user can add a Facility to the factory.
    """

    def __init__(self, factory: FactoryModel) -> None:
        super().__init__()
        self.ui = Ui_AddFacilityDialog()
        self.ui.setupUi(self)

        self.factory = factory

        self.attribute = None
        self.characteristics = None

        self.build_type_combo_box()
        self.ui.comboBox_type.activated.connect(self.type_combo_activated)

        self.ui.buttonBox.button(QDialogButtonBox.Save).setDisabled(True)

        self.ui.lineEdit_name.textChanged.connect(self.check_valid_name)

    def accept(self):
        """
        On accepting of this dialog the 2 needed classes for a facility getting created.
        :return:
        """
        self.build_attributes()
        self.build_characteristics()
        super().accept()

    def build_attributes(self) -> None:
        """
        Builds a FacilityAttributes object from the given user input.
        :return: None
        """
        self.attribute = FacilityAttributes(self.factory.cell_size)
        self.attribute.index_num = self.factory.get_next_facility_index()
        if len(self.ui.lineEdit_name.text()) == 0:
            self.attribute.name = str(random.randint(10000000, 99999999))
        else:
            self.attribute.name = self.ui.lineEdit_name.text()
        self.attribute.facility_type = self.ui.comboBox_type.currentText()
        self.attribute.department = self.ui.spinBox_department.value()
        self.attribute.area = self.ui.doubleSpinBox_width.value() * self.ui.doubleSpinBox_lenght.value()
        self.attribute.width1 = self.ui.doubleSpinBox_width.value()
        self.attribute.width2 = self.ui.doubleSpinBox_lenght.value()
        self.attribute.height = self.ui.doubleSpinBox_height.value()
        self.attribute.floorload = self.ui.doubleSpinBox_floorload.value()
        self.attribute.ceilingload = self.ui.doubleSpinBox_ceilingload.value()
        self.attribute.on_wall = self.ui.checkBox_wall.isChecked()

        self.attribute.cell_width1 = math.ceil(self.attribute.width1 / self.factory.cell_size)
        self.attribute.cell_width2 = math.ceil(self.attribute.width2 / self.factory.cell_size)
        self.attribute.cell_num = self.attribute.cell_width1 * self.attribute.cell_width2

        self.attribute.calc_sink_source_factors()

    def build_characteristics(self) -> None:
        """
        Builds a FacilityCharacteristics object from the given user input.
        :return: None
        """
        self.characteristics = FacilityCharacteristics()
        self.characteristics.noise_sensitivity = self.ui.doubleSpinBox_quiet.value()
        self.characteristics.noise_exposure = self.ui.doubleSpinBox_noise.value()
        self.characteristics.illuminance = LightLevel(self.ui.comboBox_light.currentIndex())
        self.characteristics.vibration_sensitivity = self.ui.comboBox_vibration_sensitivity.currentIndex() + 2
        self.characteristics.vibration_excitation = self.ui.comboBox_vibration_excitation.currentIndex() + 2
        self.characteristics.temperature = self.ui.comboBox_temperatur.currentIndex() - 1
        self.characteristics.cleanliness = self.ui.comboBox_cleanliness.currentIndex() - 1
        self.characteristics.mobility = self.ui.comboBox_mobility.currentIndex() + 1
        self.characteristics.employee_number = self.ui.spinBox_employee.value()

    @Slot(int)
    def type_combo_activated(self, row: int) -> None:
        """
        Activates the add type dialog.
        :param row: Row which gets activated
        :return: None
        """
        if row == self.ui.comboBox_type.count() - 1:
            self.add_facility_type()

    def build_type_combo_box(self) -> None:
        """
        Build the entries auf the facility type combobox.
        :return: None
        """
        self.ui.comboBox_type.clear()
        area_name = [area.name for area in self.factory.area_types if area.name != "None"]
        self.ui.comboBox_type.addItems(area_name)
        self.ui.comboBox_type.addItem('None')
        self.ui.comboBox_type.addItem(self.tr("Add Item"))

    def add_facility_type(self) -> None:
        """
        Adds a new type auf facility to the factory.
        :return: None
        """
        text, ok = QInputDialog.getText(self, self.tr("Add area type"), self.tr("Enter a name for the new area type."))
        if ok:
            self.factory.add_area_type(text)
            self.build_type_combo_box()
        else:
            self.ui.comboBox_type.setCurrentIndex(self.ui.comboBox_type.count() - 2)
            self.ui.comboBox_type.update()

    def check_valid_name(self) -> None:
        names = [fac.name for fac in self.factory.facility_list]
        text = self.ui.lineEdit_name.text()

        add_button = self.ui.buttonBox.button(QDialogButtonBox.Save)

        if text in names or text == '':
            add_button.setDisabled(True)
            self.ui.lineEdit_name.setStyleSheet("QLineEdit { background: rgb(255, 0, 0);}")
        else:
            add_button.setDisabled(False)
            self.ui.lineEdit_name.setStyleSheet(
                "QLineEdit { background: rgb(255, 255, 255);}")
