from PySide2.QtCore import QObject, Signal, Slot


class CalculationOutputReceiver(QObject):
    """
    This class is designed to run as a thread. Its main functin is to emit a signal to the CalculationOutput dialog.
    Every time a information is added to the msg_queue a signal gets emitted.
    """
    new_message_signal = Signal(str)

    def __init__(self, msg_queue, cores, *args, **kwargs):
        QObject.__init__(self, *args, **kwargs)
        self.msg_queue = msg_queue
        self.active_process = cores

    @Slot()
    def run(self) -> None:
        """
        Main event loop.
        :return: None
        """
        while True:
            text = self.msg_queue.get()
            self.new_message_signal.emit(text)
            if text == 'Done!':
                self.active_process -= 1
                if self.active_process == 0:
                    break
