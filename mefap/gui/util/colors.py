
"""
The colors module contains all the colors used
for displaying the FactoryCell inside the FactoryView
"""
# General
from PySide2.QtGui import QColor

BLACK = QColor(0, 0, 0)
IPH_RED = QColor(218, 37, 30)
WHITE = QColor(255, 255, 255)

# Colors for filling cells
EMPTY_CELL = QColor(255, 255, 255)
GREY_CELL = QColor(125, 125, 125)
DARK_GREY_CELL = QColor(75, 75, 75)
MEDIA_CELL = QColor(166, 205, 183)

# colors for lightning
OFF_LIGHT_CELL = QColor(102, 102, 0)
VERY_DARK_LIGHT_CELL = QColor(153, 153, 0)
DARK_LIGHT_CELL = QColor(204, 204, 0)
NORMAL_LIGHT_CELL = QColor(255, 255, 0)
BRIGHT_LIGHT_CELL = QColor(255, 255, 51)
VERY_BRIGHT_LIGHT_CELL = QColor(255, 255, 102)
MAXIMAL_LIGHT_CELL = QColor(255, 255, 204)

# restriction colors
BRIGHT_BROWN = QColor(176, 120, 64)
VERY_LIGHT_BROWN = QColor(163, 98, 32)
LIGHT_BROWN = QColor(150, 75, 0)
BROWN = QColor(131, 66, 0)
DARK_BROWN = QColor(113, 56, 0)
VERY_DARK_BROWN = QColor(94, 47, 0)

# not used facilities in list
FAC_ORANGE = QColor(255, 165, 0)
