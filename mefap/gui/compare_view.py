import PySide2
from PySide2 import QtGui
from PySide2.QtCore import QPoint, Qt
from PySide2.QtWidgets import QGraphicsView, QFrame, QRubberBand


class CompareView(QGraphicsView):
    """
    This QGraphicsView is for showing the CompareLayoutScene in the CompareDialog after the optimisation is done.
    """

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.setDragMode(QGraphicsView.ScrollHandDrag)
        self.setTransformationAnchor(QGraphicsView.AnchorUnderMouse)
        self.setResizeAnchor(QGraphicsView.AnchorUnderMouse)
        self.setFrameShape(QFrame.NoFrame)
        # for rubberband
        self.rubber_band = QRubberBand(QRubberBand.Rectangle, self)
        self.origin = QPoint()
        self.right_mouse_pressed = False
        self.setMinimumHeight(300)
        self.setMinimumWidth(300)

        self.curr_scale = 1

        self.setContextMenuPolicy(Qt.CustomContextMenu)

    def wheelEvent(self, event: QtGui.QWheelEvent) -> None:
        """ This method is responsible for zooming in this scene, a little bit buggy"""
        self.update()
        # Zoom Factor
        zoom_in_factor = 1.15
        zoom_out_factor = 1 / zoom_in_factor

        # Zoom
        if event.angleDelta().y() > 0:
            self.curr_scale *= zoom_in_factor
            self.scale(zoom_in_factor, zoom_in_factor)
        elif self.curr_scale > 0.1:
            self.curr_scale *= zoom_out_factor
            self.scale(zoom_out_factor, zoom_out_factor)

        super().wheelEvent(event)

    def mouseReleaseEvent(self, event: PySide2.QtGui.QMouseEvent):
        """
        If the mid mouse button is presses the layout gets fit into view.
        :param event: The mouse release event.
        :return:
        """
        if event.button() == Qt.MidButton:
            self.fitInView(0, 0,
                           self.scene().factory.layout.shape[1] * self.scene().factory.cell_zoom,
                           self.scene().factory.layout.shape[0] * self.scene().factory.cell_zoom,
                           Qt.KeepAspectRatio)
        super().mouseReleaseEvent(event)
