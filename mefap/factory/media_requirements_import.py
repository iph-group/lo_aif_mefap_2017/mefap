class MediaRequirementsImport:
    """
    This class contains the information about for which facilities a certain medium is required.
    """

    def __init__(self, name, media_data):
        self.name = name
        self.requirements = media_data

    def __repr__(self) -> str:
        return "name:\n{}\nrequirements:\n{}".format(self.name, self.requirements)
