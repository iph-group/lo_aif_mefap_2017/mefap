from enum import Enum


class OpeningType(Enum):
    """
    This Enum class is there to represent the 3 different OpeningType that can be
     used as opening in the optimisation algorithm.
    """
    RANDOM = 0
    RANDOM_BY_SIZE = 1
    SCHMIGALLA = 2
    JUMP_OVER = 3  # ist-layout
