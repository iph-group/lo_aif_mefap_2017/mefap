import numpy as np


def resize_matrix(matrix, row, column, def_val, bools=False):
    """
    Resizes the matrix to the givel values
    :param bools:
    :param matrix:
    :param row:
    :param column:
    :param def_val:
    :return:
    """
    if bools:
        new_arr = np.zeros(shape=(column, row), dtype=bool)
    else:
        new_arr = np.ones(shape=(column, row)) * def_val
    insert_old_values(new_arr, matrix)
    return new_arr


def insert_old_values(matrix_new, matrix_old) -> None:
    """
    Inserts the values of the old matrix into the new one.
    :param matrix_new:
    :param matrix_old:
    :return: None
    """
    for i, x in enumerate(matrix_old):
        if i >= matrix_new.shape[0]:
            break
        for j, y in enumerate(x):
            if j >= matrix_new.shape[1]:
                break
            matrix_new[i][j] = y
