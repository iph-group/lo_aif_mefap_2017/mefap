from enum import Enum


class NeighborhoodSearchType(Enum):
    """
    This Enum class is there to represent the 3 different NeighborhoodSearchType that can be
     used in the optimisation algorithm.
    """
    LocalReallocationSearch = 0
    FreeAreaSearch = 1
    MultipleElementsExchangeSearch = 2
