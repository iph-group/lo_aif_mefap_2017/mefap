from typing import Dict

import numpy as np


class NoiseArea:

    def __init__(self, columns, rows):
        self.noise_area_matrix = np.ones(shape=(columns, rows)) * -1

    def add_noise_area(self, upper_left_x: int, upper_left_y: int, width: int, height: int):
        index = np.amax(self.noise_area_matrix) + 1
        for row in range(height + 1):
            for column in range(width + 1):
                if self.noise_area_matrix[upper_left_y + row][upper_left_x + column] != -1:
                    raise ValueError("There is an already existing noise area on this place.")
        for row in range(height + 1):
            for column in range(width + 1):
                self.noise_area_matrix[upper_left_y + row][upper_left_x + column] = index

    def remove_noise_area_by_position(self, x: int, y: int):
        self.remove_noise_area_by_index(self.noise_area_matrix[y][x])

    def remove_noise_area_by_index(self, index: int):
        self.noise_area_matrix[self.noise_area_matrix == index] = -1

    def get_noise_walls_of_position(self, x: int, y: int) -> np.array:
        return self.get_noise_walls_by_index(self.noise_area_matrix[y][x])

    def get_noise_walls_by_index(self, index: int) -> np.array:
        if index <= -1:
            return np.array([])
        x_list, y_list = np.where(self.noise_area_matrix == index)

        area_tiles = np.column_stack((x_list, y_list))

        min_tile = area_tiles.min(axis=0)
        max_tile = area_tiles.max(axis=0)
        # print(min_tile)
        # print(max_tile)

        corner_tiles = []
        for i in range(min_tile[1], max_tile[1] + 1):
            corner_tiles.append((min_tile[0], i))
            corner_tiles.append((max_tile[0], i))
        for i in range(min_tile[0], max_tile[0] + 1):
            corner_tiles.append((i, min_tile[1]))
            corner_tiles.append((i, max_tile[1]))
        return np.unique(np.array(corner_tiles), axis=0)

    def get_noise_wall_corners_by_index(self, index: int) -> np.array:
        if index <= -1:
            return np.array([])
        x_list, y_list = np.where(self.noise_area_matrix == index)

        area_tiles = np.column_stack((x_list, y_list))

        min_tile = area_tiles.min(axis=0)
        max_tile = area_tiles.max(axis=0)

        corner_list = []
        corner_list.append((min_tile[0], min_tile[1]))  # north east
        corner_list.append((min_tile[0], max_tile[1]))  # north west
        corner_list.append((max_tile[0], max_tile[1]))  # south west
        corner_list.append((max_tile[0], min_tile[1]))  # south east

        return corner_list

    def get_all_noise_walls(self) -> Dict[int, np.array]:
        wall_dict = {}
        for index in np.unique(self.noise_area_matrix):
            if index == -1:
                continue
            else:
                wall_dict[int(index)] = self.get_noise_walls_by_index(index)
        return wall_dict

    def is_in_noise_area(self, x: int, y: int) -> bool:
        return self.noise_area_matrix[y][x] > -1

    def get_noise_area_id(self, x: int, y: int):
        return self.noise_area_matrix[y][x]

    def is_noise_wall(self, x: int, y: int) -> bool:
        noise_area = self.get_noise_walls_of_position(x, y)
        for entry in range(0, noise_area.shape[0], 1):
            if noise_area[entry, 0] == y and noise_area[entry, 1] == x:
                return True

        return False


if __name__ == '__main__':
    na = NoiseArea(10, 10)
    na.add_noise_area(0, 0, 4, 3)
    na.add_noise_area(4, 4, 2, 2)
    # print(na.get_noise_walls_by_index(0))
    print(na.noise_area_matrix)
    #print(na.get_all_noise_walls())
    #print(na.is_noise_wall(5,5))
    print(na.get_noise_walls_of_position(0,0))
