import math
import numpy as np

from PySide2.QtGui import QColor


class FacilityAttributes:
    """
    This class holds all attributes of a facility.
    """

    def __init__(self, cell_width, sheet_factory_objects=None, index=None, excel_load=False, json_load=False,
                 **kwargs) -> None:
        self.index_num = None
        self.name = None
        self.facility_type = None
        self.department = None
        self.area = None
        self.width1 = None
        self.width2 = None
        self.height = None
        self.cell_width1 = None
        self.cell_width2 = None
        self.cell_num = None
        self.floorload = None
        self.ceilingload = None
        self.color = None
        self.on_wall = None
        self.cell_width1 = None
        self.cell_width2 = None
        self.cell_num = None

        self.source_position = [0, 0]
        self.sink_position = [0, 0]
        self.source_sequence_factor = None
        self.source_sequence_edge = None
        self.sink_sequence_factor = None
        self.sink_sequence_edge = None

        self.curr_position = [None, None]
        # ToDo: Entscheidung, ob aus Tabelle oder GUI (Tendenz -> GUI, nach Abschluss der Fixierung)
        self.fixed_position = False
        self.curr_source_position = [None, None]
        self.curr_sink_position = [None, None]
        self.curr_rotation = None
        self.curr_mirror = None

        self.noise_propagation_layout = np.empty(shape=(1, 1))  # np.ndarray = None
        self.noise_source_position = [None, None]
        self.noise_encapsulation = None

        if excel_load and not json_load:
            self.excel_load(index, sheet_factory_objects, cell_width)
        elif json_load and not excel_load:
            for key, value in kwargs.items():
                if key == 'color':
                    if value:
                        setattr(self, key, QColor(value[0], value[1], value[2], value[3]))
                    else:
                        setattr(self, key, None)
                else:
                    setattr(self, key, value)

    def excel_load(self, index, sheet_factory_objects, cell_width) -> None:
        """
        Parses all information about the facility from an excel sheet into this
        FacilityAttributes object.
        :param index:
        :param sheet_factory_objects:
        :param cell_width: Cell width of the factory the facility is living in.
        :return: None
        """
        self.index_num = index - 5  # xlTableStartRow

        if sheet_factory_objects.cell(index, 1).value is None:
            self.name = "unbenanntes Fabrikobjekt"
        else:
            self.name = sheet_factory_objects.cell(index, 1).value

        if sheet_factory_objects.cell(index, 2).value is None:
            self.facility_type = "unbenannte-Nutzungsfläche"
        else:
            self.facility_type = sheet_factory_objects.cell(index, 2).value

        if sheet_factory_objects.cell(index, 3).value is None:
            self.department = 0
        else:
            self.department = sheet_factory_objects.cell(index, 3).value

        if sheet_factory_objects.cell(index, 4).value is None:
            self.area = 1
        else:
            self.area = sheet_factory_objects.cell(index, 4).value

        if sheet_factory_objects.cell(index, 5).value is None:
            self.width1 = 1
        else:
            self.width1 = sheet_factory_objects.cell(index, 5).value  # x-Richtung

        if sheet_factory_objects.cell(index, 6).value is None:
            self.width2 = 1
        else:
            self.width2 = sheet_factory_objects.cell(index, 6).value  # y-Richtung

        if sheet_factory_objects.cell(index, 7).value is None:
            self.height = 0
        else:
            self.height = sheet_factory_objects.cell(index, 7).value

        self.cell_width1 = math.ceil(self.width1 / cell_width)  # x-Richtung
        self.cell_width2 = math.ceil(self.width2 / cell_width)  # y-Richtung
        self.cell_num = self.cell_width1 * self.cell_width2
        if self.cell_num % 2 != 0 and self.cell_num != 1:
            print('Ungerade Zellenanzahl bei ' + self.name + " mit: " + str(
                self.cell_num) + " Zellen")

        if sheet_factory_objects.cell(index, 8).value is None:
            self.floorload = 0
        else:
            self.floorload = sheet_factory_objects.cell(index, 8).value

        if sheet_factory_objects.cell(index, 9).value is None:
            self.ceilingload = 0
        else:
            self.ceilingload = sheet_factory_objects.cell(index, 9).value

        self.calc_sink_source_factors()

        if sheet_factory_objects.cell(index, 19).value == 0:
            self.on_wall = False
        elif sheet_factory_objects.cell(index, 19).value == 1:
            self.on_wall = True
        else:
            self.on_wall = False  # print("Fehler: Excel-Import")

        if sheet_factory_objects.cell(index, 20).value == 0:
            self.noise_encapsulation = False
        elif sheet_factory_objects.cell(index, 20).value == 1:
            self.noise_encapsulation = True
        else:
            self.noise_encapsulation = False  # print("Fehler: Excel-Import")

    def calc_sink_source_factors(self) -> None:
        """
        Calculates the factors of sink and source from this facility.
        :return: None
        """
        for iteration in range(0, 2, 1):
            if iteration == 0:
                help_position = self.source_position
                on_edge = False
            elif iteration == 1:
                help_position = self.sink_position
                on_edge = False
            # calculate current sourcePos or sinkPos
            if self.cell_width2 - 1 > help_position[0] >= 0 and help_position[1] == 0:
                origin_sequence_factor = 0
                if help_position[0] == 0 and help_position[1] == 0:
                    on_edge = True

            elif help_position[0] == 0 and help_position[1] > 0:
                origin_sequence_factor = 1
                if help_position[0] == 0 and help_position[1] == self.cell_width1 - 1:
                    on_edge = True

            elif help_position[0] > 0 and help_position[1] == self.cell_width1 - 1:
                origin_sequence_factor = 2
                if help_position[0] == self.cell_width2 - 1 and help_position[1] == self.cell_width1 - 1:
                    on_edge = True

            # ToDo: Doppeldefinition von (Y,X) prüfen
            elif help_position[0] == self.cell_width2 - 1 and self.cell_width1 - 1 > \
                    help_position[
                        1] >= 0:
                origin_sequence_factor = 3
                if help_position[0] == self.cell_width2 - 1 and help_position[1] == 0:
                    on_edge = True

            else:
                # facility has only one cell
                origin_sequence_factor = 0
                on_edge = True

            # write data
            if iteration == 0:
                self.source_sequence_factor = origin_sequence_factor
                self.source_sequence_edge = on_edge
            elif iteration == 1:
                self.sink_sequence_factor = origin_sequence_factor
                self.sink_sequence_edge = on_edge

    def __repr__(self) -> str:
        return "name:\n{}\nType:\n{}\nDepartment:\n{}".format(self.name, self.facility_type,
                                                              self.department)
