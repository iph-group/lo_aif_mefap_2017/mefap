class FacilityData:
    """ This class contains layout/facility information of a specific layout variant """

    def __init__(self) -> None:
        self.index_num = None
        self.curr_position = [None, None]
        self.curr_source_position = [None, None]
        self.curr_sink_position = [None, None]
        self.curr_rotation = None
        self.curr_mirror = None
