from mefap.factory.util.light_level import LightLevel


class FacilityCharacteristics:
    """
    This class holds all characteristics of an Facility object. Especially the ones
    concerning their enviroment.
    """
    def __init__(self, sheet_factory_objects=None, index=None, from_json=False, from_excel=False, **kwargs):
        self.noise_sensitivity = None
        self.noise_exposure = None
        self.illuminance = None
        self.vibration_sensitivity = None
        self.vibration_excitation = None
        self.temperature = None
        self.cleanliness = None
        self.mobility = None
        self.employee_number = None
        if from_excel and not from_json:
            self.load_from_excel(sheet_factory_objects, index)
        elif from_json and not from_excel:
            for key, value in kwargs.items():
                setattr(self, key, value)

    def load_from_excel(self, sheet_factory_objects, index) -> None:
        """
        Fills values of this FacilityCharacteristics from an excel sheet.
        :param sheet_factory_objects:
        :param index:
        :return: None
        """
        if not sheet_factory_objects or not index:
            raise AttributeError("If from_excel is choosen, sheet_factory_objects and index must contain values!")

        if sheet_factory_objects.cell(index, 10).value is None:
            self.noise_sensitivity = 0
        else:
            self.noise_sensitivity = sheet_factory_objects.cell(index, 10).value

        if sheet_factory_objects.cell(index, 11).value is None:
            self.noise_exposure = 0
        else:
            self.noise_exposure = sheet_factory_objects.cell(index, 11).value

        if sheet_factory_objects.cell(index, 12).value is None:
            self.illuminance = LightLevel(0)
        else:
            self.illuminance = LightLevel(sheet_factory_objects.cell(index, 12).value)

        if sheet_factory_objects.cell(index, 13).value is None:
            self.vibration_sensitivity = 2
        else:
            self.vibration_sensitivity = sheet_factory_objects.cell(index, 13).value

        if sheet_factory_objects.cell(index, 14).value is None:
            self.vibration_excitation = 2
        else:
            self.vibration_excitation = sheet_factory_objects.cell(index, 14).value

        if sheet_factory_objects.cell(index, 15).value is None:
            self.temperature = - 1
        else:
            self.temperature = sheet_factory_objects.cell(index, 15).value

        if sheet_factory_objects.cell(index, 16).value is None:
            self.cleanliness = - 1
        else:
            self.cleanliness = sheet_factory_objects.cell(index, 16).value

        if sheet_factory_objects.cell(index, 17).value is None:
            self.mobility = 1
        else:
            self.mobility = sheet_factory_objects.cell(index, 17).value

        if sheet_factory_objects.cell(index, 18).value is None:
            self.employee_number = 0
        else:
            self.employee_number = sheet_factory_objects.cell(index, 18).value
