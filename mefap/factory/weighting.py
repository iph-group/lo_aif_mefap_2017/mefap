class Weighting:
    """
    The Weighting class is representing all weighting attributes
    of the optimization, which can be set inside WeightDialog (GUI)
    """

    def __init__(self) -> None:
        super().__init__()
        # Material flow and logistic
        self.material_flow_length = 0.08
        self.no_overlapping = 0.08
        self.route_continuity = 0.08
        self.land_use_degree = 0.08

        # Mutability
        self.media_availability = 0.08
        self.media_compatibility = 0.08

        # Environmental effects
        self.lighting = 0.08
        self.quiet = 0.08
        self.vibration = 0.08
        self.temperature = 0.07
        self.cleanliness = 0.07

        # Communication
        self.direct_communication = 0.07
        self.formal_communication = 0.07
