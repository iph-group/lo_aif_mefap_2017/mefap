import numpy as np


class Restriction:
    """
    The class Restriction is an object which is used to represent the
    restrictions of the factory. For example restrictions are height profile,
    cover capacity and floor capacity.
    """

    def __init__(self, name: str, rows: int, columns: int, max_val) -> None:
        super().__init__()
        self.name = name
        self.matrix = np.full((columns, rows), max_val)
        self.max_value = max_val
