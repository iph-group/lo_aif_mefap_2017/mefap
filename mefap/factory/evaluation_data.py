class EvaluationData:
    """ This class contains evaluation data of a specific layout"""

    def __init__(self) -> None:
        self.weight_sum = None
        # self.universality = [None, None]
        # self.scalability = [None, None]
        # self.modularity = [None, None]
        # self.mobility = [None, None]
        self.media_compatibility = [None, None]
        self.media_availability = [None, None]
        self.mf_distance = [None, None]
        self.mf_overlapping = [None, None]
        self.mf_constancy = [None, None]
        self.area_utilization = [None, None]
        self.illuminance = [None, None]
        self.noise = [None, None]
        self.vibration = [None, None]
        self.temperature = [None, None]
        self.cleanliness = [None, None]
        self.direct_com = [None, None]
        self.formal_com = [None, None]
        # self.informal_com = [None, None]
        # self.transparency = [None, None]
