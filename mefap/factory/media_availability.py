import logging
from typing import List

import numpy as np

from mefap.gui.factory_cell import FactoryCell
from mefap.gui.util.colors import MEDIA_CELL


class MediaAvailability:
    """
    This class holds the information about the availability for a media.
    """

    def __init__(self, name, factory, json_load=False, layout_data=None):
        self.name = name
        if not json_load:
            self.availability_layout = np.zeros(shape=(factory.columns, factory.rows), dtype=bool)
        else:
            self.availability_layout = np.array(layout_data)

    def change_availability(self, cells: List[FactoryCell], availability: bool) -> None:
        """
        This method changes the availability on the positions of the given cells(graphical object).
        :param cells: The cells where the availability changes.
        :param availability: The new value of availability
        :return: None
        """
        logging.info("Changed {} availability".format(self.name))
        for cell in cells:
            self.availability_layout[cell.y_cord][cell.x_cord] = availability
            if availability:
                cell.color_media(MEDIA_CELL)
            else:
                cell.media_graying()

    def __repr__(self) -> str:
        return "name:\n{}\navailability_layout:\n{}".format(self.name, self.availability_layout)
