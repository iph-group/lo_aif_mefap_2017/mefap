from mefap.evaluation.eval_material_flow import eval_material_flow_distance
from mefap.evaluation.eval_material_flow import eval_material_flow_overlapping
from mefap.evaluation.eval_material_flow import eval_material_flow_constancy
from mefap.evaluation.eval_material_flow import eval_area_utilization
from mefap.evaluation.eval_communication import eval_direct_communication
from mefap.evaluation.eval_communication import eval_formal_communication
from mefap.evaluation.eval_media import eval_media_compatibility
from mefap.evaluation.eval_media import eval_media_availability
from mefap.evaluation.eval_environmental import eval_illuminance
from mefap.evaluation.eval_environmental import eval_noise
from mefap.evaluation.eval_environmental import eval_vibration
from mefap.evaluation.eval_environmental import eval_cleanliness
from mefap.evaluation.eval_environmental import eval_temperature


def evaluate_layout(factory, *args):
    """
    evaluate weighted objectives
    :param factory:
    :param args: database or local_database
    :return:
    """
    big_m = pow(3, (factory.rows + factory.columns))
    small_m = 0.1
    square_m = factory.rows * factory.columns
    # eval material flow
    if factory.weighting.material_flow_length > 0:
        eval_material_flow_distance(factory)
    else:
        factory.quamfab.mf_distance[0] = big_m
        factory.quamfab.mf_distance[1] = None

    if factory.weighting.route_continuity > 0:
        eval_material_flow_constancy(factory)
    else:
        factory.quamfab.mf_constancy[0] = big_m
        factory.quamfab.mf_constancy[1] = None

    if factory.weighting.no_overlapping > 0:
        eval_material_flow_overlapping(factory)
    else:
        factory.quamfab.mf_overlapping[0] = big_m
        factory.quamfab.mf_overlapping[1] = None

    if factory.weighting.land_use_degree > 0:
        eval_area_utilization(factory)
    else:
        factory.quamfab.area_utilization[0] = square_m
        factory.quamfab.area_utilization[1] = 0

    # eval communications
    if factory.weighting.direct_communication > 0:
        eval_direct_communication(factory)
    else:
        factory.quamfab.direct_com[0] = big_m
        factory.quamfab.direct_com[1] = None

    if factory.weighting.formal_communication > 0:
        eval_formal_communication(factory)
    else:
        factory.quamfab.formal_com[0] = big_m
        factory.quamfab.formal_com[1] = None

    # eval media / equipment
    if factory.weighting.media_compatibility > 0:
        eval_media_compatibility(factory)
    else:
        factory.quamfab.media_compatibility[0] = square_m
        factory.quamfab.media_compatibility[1] = 0

    if factory.weighting.media_availability > 0:
        eval_media_availability(factory)
    else:
        factory.quamfab.media_availability[0] = small_m
        factory.quamfab.media_availability[1] = None

    # eval environment
    if factory.weighting.lighting > 0:
        eval_illuminance(factory)
    else:
        factory.quamfab.illuminance[0] = square_m
        factory.quamfab.illuminance[1] = 0

    if factory.weighting.quiet > 0:
        eval_noise(factory)
    else:
        factory.quamfab.noise[0] = square_m
        factory.quamfab.noise[1] = 0

    if factory.weighting.vibration > 0:
        eval_vibration(factory)
    else:
        factory.quamfab.vibration[0] = small_m
        factory.quamfab.vibration[1] = None

    if factory.weighting.cleanliness > 0:
        eval_cleanliness(factory)
    else:
        factory.quamfab.cleanliness[0] = small_m
        factory.quamfab.cleanliness[1] = None

    if factory.weighting.temperature > 0:
        eval_temperature(factory)
    else:
        factory.quamfab.temperature[0] = small_m
        factory.quamfab.temperature[1] = None

    # check minimum objective value
    # check local_database
    if args:
        minimum_objective_value(factory, args)
    else:
        minimum_objective_value(factory)

    # calc weight sum
    factory.quamfab.weight_sum = weight_sum_variant(factory.weighting, factory.quamfab)


def evaluate_layout_all_objectives(factory, *args):
    """
    evaluate all objectives
    :param factory:
    :param args:
    :return:
    """
    # eval material flow
    eval_material_flow_distance(factory)
    eval_material_flow_constancy(factory)
    eval_material_flow_overlapping(factory)
    eval_area_utilization(factory)

    # eval communications
    eval_direct_communication(factory)
    eval_formal_communication(factory)

    # eval media / equipment
    eval_media_compatibility(factory)
    eval_media_availability(factory)

    # eval environment
    eval_illuminance(factory)
    eval_noise(factory)
    eval_vibration(factory)
    eval_cleanliness(factory)
    eval_temperature(factory)

    # check minimum objective value
    # check for database and local_database
    if args:
        minimum_objective_value(factory, args)
    else:
        minimum_objective_value(factory)

    # calc weight sum
    factory.quamfab.weight_sum = weight_sum_variant(factory.weighting, factory.quamfab)


def minimum_objective_value(factory, *args):
    # mf_distance
    min_objectives = ["mf_distance", "mf_overlapping", "mf_constancy", "direct_com", "formal_com", "noise"]
    max_objectives = ["media_availability", "vibration", "cleanliness", "temperature"]
    rate_objectives = ["illuminance", "media_compatibility", "area_utilization"]  # "noise",
    """    
    if args:
        local_database = args[0]
    """
    # for objective, value in factory.quamfab.__dict__.items():
    for objective_name, objective_value in vars(factory.quamfab).items():
        objective_min_value = getattr(factory.quamfab_min, objective_name)

        # select case
        if objective_name in min_objectives:

            if objective_min_value[0] is not None:
                if objective_value[0] < objective_min_value[0]:
                    objective_value[1] = 1
                    objective_min_value = objective_value[:]
                    setattr(factory.quamfab, objective_name, objective_value)
                    setattr(factory.quamfab_min, objective_name, objective_min_value)

                    # overwrite old evaluation data (database = args[0], local_database = args[1])
                    if args:
                        for database in args[0]:
                            for variant in database.layouts_evaluation_data:
                                objective_value_variant = getattr(variant, objective_name)
                                objective_value_variant[1] = objective_value[0] / objective_value_variant[0]
                                setattr(variant, objective_name, objective_value_variant)
                                weight_sum_val = weight_sum_variant(factory.weighting, variant)
                                setattr(variant, "weight_sum", weight_sum_val)
                    """
                    # overwrite old evaluation data (database)
                    for variant in database.layouts_evaluation_data:
                        objective_value_variant = getattr(variant, objective_name)
                        objective_value_variant[1] = objective_value[0] / objective_value_variant[0]
                        setattr(variant, objective_name, objective_value_variant)
                        weight_sum_val = weight_sum_variant(factory.weighting, variant)
                        setattr(variant, "weight_sum", weight_sum_val)

                    # overwrite old evaluation data (local_database)
                    if args:
                        for variant in local_database.layouts_evaluation_data:
                            objective_value_variant = getattr(variant, objective_name)
                            objective_value_variant[1] = objective_value[0] / objective_value_variant[0]
                            setattr(variant, objective_name, objective_value_variant)
                            weight_sum_val = weight_sum_variant(factory.weighting, variant)
                            setattr(variant, "weight_sum", weight_sum_val)
                    """
                    # overwrite old evaluation data (factory)
                    for variant in factory.layouts_evaluation_data:
                        objective_value_variant = getattr(variant, objective_name)
                        objective_value_variant[1] = objective_value[0] / objective_value_variant[0]
                        setattr(variant, objective_name, objective_value_variant)
                        weight_sum_val = weight_sum_variant(factory.weighting, variant)
                        setattr(variant, "weight_sum", weight_sum_val)
                else:
                    if objective_value[0] == 0:
                        objective_value[1] = 1
                    else:
                        objective_value[1] = objective_min_value[0] / objective_value[0]  # debug
                    setattr(factory.quamfab, objective_name, objective_value)
            else:
                objective_value[1] = 1
                objective_min_value = objective_value[:]
                setattr(factory.quamfab, objective_name, objective_value)
                setattr(factory.quamfab_min, objective_name, objective_min_value)

        elif objective_name in max_objectives:

            if objective_min_value[0] is not None:
                if objective_value[0] > objective_min_value[0]:
                    objective_value[1] = 1
                    objective_min_value = objective_value[:]
                    setattr(factory.quamfab, objective_name, objective_value)
                    setattr(factory.quamfab_min, objective_name, objective_min_value)

                    # overwrite old evaluation data (database = args[0], local_database = args[1])
                    if args:
                        for database in args[0]:
                            for variant in database.layouts_evaluation_data:
                                objective_value_variant = getattr(variant, objective_name)
                                # todo: division by zero???
                                objective_value_variant[1] = objective_value_variant[0] / objective_value[0]
                                setattr(variant, objective_name, objective_value_variant)
                                weight_sum_val = weight_sum_variant(factory.weighting, variant)
                                setattr(variant, "weight_sum", weight_sum_val)
                    """
                    # overwrite old evaluation data (database)
                    for variant in database.layouts_evaluation_data:
                        objective_value_variant = getattr(variant, objective_name)
                        objective_value_variant[1] = objective_value_variant[0] / objective_value[0]
                        setattr(variant, objective_name, objective_value_variant)
                        weight_sum_val = weight_sum_variant(factory.weighting, variant)
                        setattr(variant, "weight_sum", weight_sum_val)

                    # overwrite old evaluation data (local_database)
                    if args:
                        for variant in local_database.layouts_evaluation_data:
                            objective_value_variant = getattr(variant, objective_name)
                            objective_value_variant[1] = objective_value_variant[0] / objective_value[0]
                            setattr(variant, objective_name, objective_value_variant)
                            weight_sum_val = weight_sum_variant(factory.weighting, variant)
                            setattr(variant, "weight_sum", weight_sum_val)
                    """
                    # overwrite old evaluation data (factory)
                    for variant in factory.layouts_evaluation_data:
                        objective_value_variant = getattr(variant, objective_name)
                        # todo: division by zero???
                        objective_value_variant[1] = objective_value_variant[0] / objective_value[0]
                        setattr(variant, objective_name, objective_value_variant)
                        weight_sum_val = weight_sum_variant(factory.weighting, variant)
                        setattr(variant, "weight_sum", weight_sum_val)
                else:
                    if objective_value[0] == 0:
                        objective_value[1] = 0
                    else:
                        objective_value[1] = objective_value[0] / objective_min_value[0]
                    setattr(factory.quamfab, objective_name, objective_value)
            else:
                objective_value[1] = 1
                objective_min_value = objective_value[:]
                setattr(factory.quamfab, objective_name, objective_value)
                setattr(factory.quamfab_min, objective_name, objective_min_value)

        elif objective_name in rate_objectives:
            # objectives still have rating
            if objective_min_value[0] is not None:
                if objective_value[1] > objective_min_value[1]:
                    # objective_min_value = objective_value
                    objective_min_value = objective_value[:]
                    setattr(factory.quamfab_min, objective_name, objective_min_value)

                    # overwrite old evaluation data (database = args[0], local_database = args[1])
                    if args:
                        for database in args[0]:
                            for variant in database.layouts_evaluation_data:
                                weight_sum_val = weight_sum_variant(factory.weighting, variant)
                                setattr(variant, "weight_sum", weight_sum_val)
                    """
                    # overwrite old evaluation data (database)
                    for variant in database.layouts_evaluation_data:
                        weight_sum_val = weight_sum_variant(factory.weighting, variant)
                        setattr(variant, "weight_sum", weight_sum_val)

                    # overwrite old evaluation data (local_database)
                    if args:
                        for variant in local_database.layouts_evaluation_data:
                            weight_sum_val = weight_sum_variant(factory.weighting, variant)
                            setattr(variant, "weight_sum", weight_sum_val)
                    """
                    # overwrite old evaluation data (factory)
                    for variant in factory.layouts_evaluation_data:
                        weight_sum_val = weight_sum_variant(factory.weighting, variant)
                        setattr(variant, "weight_sum", weight_sum_val)
            else:
                # objective_min_value = objective_value
                objective_min_value = objective_value[:]
                setattr(factory.quamfab_min, objective_name, objective_min_value)


def weight_sum_variant(weights, variant_object):
    # init weight_sum_value
    weight_sum_value = 0

    # eval material flow
    if weights.material_flow_length > 0:
        weight_sum_value = weight_sum_value + variant_object.mf_distance[1] * weights.material_flow_length

    if weights.route_continuity > 0:
        weight_sum_value = weight_sum_value + variant_object.mf_constancy[1] * weights.route_continuity

    if weights.no_overlapping > 0:
        weight_sum_value = weight_sum_value + variant_object.mf_overlapping[1] * weights.no_overlapping

    if weights.land_use_degree > 0:
        weight_sum_value = weight_sum_value + variant_object.area_utilization[1] * weights.land_use_degree

    # eval communications
    if weights.direct_communication > 0:
        weight_sum_value = weight_sum_value + variant_object.direct_com[1] * weights.direct_communication

    if weights.formal_communication > 0:
        weight_sum_value = weight_sum_value + variant_object.formal_com[1] * weights.formal_communication

    # eval media / equipment
    if weights.media_compatibility > 0:
        weight_sum_value = weight_sum_value + variant_object.media_compatibility[1] * weights.media_compatibility

    if weights.media_availability > 0:
        weight_sum_value = weight_sum_value + variant_object.media_availability[1] * weights.media_availability

    # eval environment
    if weights.lighting > 0:
        weight_sum_value = weight_sum_value + variant_object.illuminance[1] * weights.lighting

    if weights.quiet > 0:
        weight_sum_value = weight_sum_value + variant_object.noise[1] * weights.quiet

    if weights.vibration > 0:
        weight_sum_value = weight_sum_value + variant_object.vibration[1] * weights.vibration

    if weights.cleanliness > 0:
        weight_sum_value = weight_sum_value + variant_object.cleanliness[1] * weights.cleanliness

    if weights.temperature > 0:
        weight_sum_value = weight_sum_value + variant_object.temperature[1] * weights.temperature

    """
    weight = 0 / 10
    weight_mf_distance = weight  # 0.6
    weight_mf_overlapping = 0.7  # weight
    weight_mf_constancy = 0.3  # weight  # 0.4
    weight_area_utilization = weight
    weight_direct_com = weight
    weight_formal_com = weight
    weight_media_comp = 0  # weight
    weight_media_avail = weight
    weight_illuminance = weight
    weight_noise = weight  # 0
    weight_vibration = weight
    weight_cleanliness = 0  # weight
    weight_temperature = 0  # weight
    """

    """
    weight_sum_value = variant_object.mf_distance[1] * weights.material_flow_length \
                 + variant_object.mf_constancy[1] * weights.route_continuity \
                 + variant_object.area_utilization[1] * weights.land_use_degree \
                 + variant_object.direct_com[1] * weights.direct_communication \
                 + variant_object.formal_com[1] * weights.formal_communication \
                 + variant_object.media_compatibility[1] * weights.media_compatibility \
                 + variant_object.media_availability[1] * weights.media_availability \
                 + variant_object.illuminance[1] * weights.lighting \
                 + variant_object.noise[1] * weights.quiet \
                 + variant_object.vibration[1] * weights.vibration \
                 + variant_object.cleanliness[1] * weights.cleanliness \
                 + variant_object.mf_overlapping[1] * weights.no_overlapping \
                 + variant_object.temperature[1] * weights.temperatur
    return weight_sum_value
    """

    return weight_sum_value


def update_objective_value(factory, *args):
    # mf_distance
    min_objectives = ["mf_distance", "mf_overlapping", "mf_constancy", "direct_com", "formal_com", "noise"]
    max_objectives = ["media_availability", "vibration", "cleanliness", "temperature"]
    rate_objectives = ["illuminance", "media_compatibility", "area_utilization"]

    # for objective, value in factory.quamfab.__dict__.items():
    for objective_name, objective_value in vars(factory.quamfab).items():

        if objective_name == "weight_sum":  # or objective_name in rate_objectives:
            continue

        # get all values of the objective from all variants
        objective_min_list = [objective_value]
        if args:
            for database in args:
                for variant in database.layouts_evaluation_data:
                    objective_min_list.append(getattr(variant, objective_name))

        # select case
        if objective_name in min_objectives:

            # get minimum
            objective_min_list.sort(key=lambda tup: tup[0])
            objective_min_value = objective_min_list[0][:]
            objective_min_value[1] = 1

            # overwrite old evaluation curr_layout
            if objective_value[0] == 0:
                objective_value[1] = 1
            else:
                objective_value[1] = objective_min_value[0] / objective_value[0]  # debug
            setattr(factory.quamfab, objective_name, objective_value)
            setattr(factory.quamfab_min, objective_name, objective_min_value)

            # overwrite old evaluation data (database = args[0], local_database = args[1])
            if args:
                for database in args:
                    for variant in database.layouts_evaluation_data:
                        objective_value_variant = getattr(variant, objective_name)
                        if objective_value_variant[0] == 0:
                            objective_value_variant[1] = 1
                        else:
                            objective_value_variant[1] = objective_min_value[0] / objective_value_variant[0]
                        setattr(variant, objective_name, objective_value_variant)

            # overwrite old evaluation data (factory)
            for variant in factory.layouts_evaluation_data:
                objective_value_variant = getattr(variant, objective_name)
                if objective_value_variant[0] == 0:
                    objective_value_variant[1] = 1
                else:
                    objective_value_variant[1] = objective_min_value[0] / objective_value_variant[0]
                setattr(variant, objective_name, objective_value_variant)

        elif objective_name in max_objectives:

            # get maximum
            objective_min_list.sort(key=lambda tup: tup[0], reverse=True)
            objective_min_value = objective_min_list[0][:]  # [:]
            objective_min_value[1] = 1

            if objective_value[0] == 0:
                objective_value[1] = 0
            else:
                objective_value[1] = objective_value[0] / objective_min_value[0]
            setattr(factory.quamfab, objective_name, objective_value)
            setattr(factory.quamfab_min, objective_name, objective_min_value)

            # overwrite old evaluation data (database = args[0], local_database = args[1])
            if args:
                for database in args:
                    for variant in database.layouts_evaluation_data:
                        objective_value_variant = getattr(variant, objective_name)
                        if objective_value_variant[0] == 0:
                            objective_value_variant[1] = 0
                        else:
                            objective_value_variant[1] = objective_value_variant[0] / objective_min_value[0]
                        setattr(variant, objective_name, objective_value_variant)

            # overwrite old evaluation data (factory)
            for variant in factory.layouts_evaluation_data:
                objective_value_variant = getattr(variant, objective_name)
                if objective_value_variant[0] == 0:
                    objective_value_variant[1] = 0
                else:
                    objective_value_variant[1] = objective_value_variant[0] / objective_min_value[0]
                setattr(variant, objective_name, objective_value_variant)

        elif objective_name in rate_objectives:
            # objectives still have rating

            # get maximum
            objective_min_list.sort(key=lambda tup: tup[1], reverse=True)
            objective_min_value = objective_min_list[0][:]  # [:]
            # set best to quamfab_min
            setattr(factory.quamfab_min, objective_name, objective_min_value)

    # calc new weight sum
    if args:
        for database in args:
            for variant in database.layouts_evaluation_data:
                weight_sum_val = weight_sum_variant(factory.weighting, variant)
                setattr(variant, "weight_sum", weight_sum_val)

    for variant in factory.layouts_evaluation_data:
        weight_sum_val = weight_sum_variant(factory.weighting, variant)
        setattr(variant, "weight_sum", weight_sum_val)

    # calc weight sum
    factory.quamfab.weight_sum = weight_sum_variant(factory.weighting, factory.quamfab)


def reset_quamfab_min(factory) -> None:
    for objective_name, objective_value in vars(factory.quamfab_min).items():
        if not objective_name == "weight_sum":
            new_objective_value = [None, None]
            setattr(factory.quamfab_min, objective_name, new_objective_value)
        else:
            new_objective_value = None
            setattr(factory.quamfab_min, objective_name, new_objective_value)
