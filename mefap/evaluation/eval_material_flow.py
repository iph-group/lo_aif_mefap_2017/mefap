import logging
import numpy
import math
import numpy as np


def eval_material_flow_distance(factory):
    """ evaluate material flow distance """
    mf_distance = 0
    # get facility
    for facility in range(0, len(factory.facility_list), 1):
        # get facilities from mf_matrix
        for column in range(0, factory.mf_matrix.shape[0], 1):
            # check if connection exists
            if factory.mf_matrix[factory.facility_list[facility].index_num, column] != 0.0 \
                    and factory.facility_list[column].curr_source_position != [None, None] \
                    and factory.facility_list[column].index_num != factory.facility_list[facility].index_num:

                if factory.parameter["no_path_planning"]:
                    mf_distance += factory.path_list[facility].paths[column] * factory.cell_size * \
                                   factory.mf_matrix[facility, column]
                else:
                    mf_distance += len(factory.path_list[facility].paths[column]) * factory.cell_size * \
                                   factory.mf_matrix[facility, column]

    # write data
    factory.quamfab.mf_distance[0] = mf_distance
    factory.quamfab.mf_distance[1] = None


def eval_material_flow_overlapping(factory):
    """ evaluate material flow overlapping """
    crossing_points = []
    not_crossing_points = []
    breaker = False
    mf_overlapping = 0

    # check if path_planning
    if not factory.parameter["no_path_planning"]:

        # iterate over all path_elements
        for from_to in range(0, len(factory.path_list), 1):
            for curr_path in range(0, len(factory.path_list[from_to].paths), 1):
                for element_num in range(1, len(factory.path_list[from_to].paths[curr_path]) - 1, 1):
                    curr_element = factory.path_list[from_to].paths[curr_path][element_num]
                    # check if crossing point is all ready found
                    if curr_element in crossing_points or curr_element in not_crossing_points:
                        continue
                        # todo: debug
                    # generate test triple of path elements
                    curr_before = factory.path_list[from_to].paths[curr_path][element_num - 1]
                    curr_next = factory.path_list[from_to].paths[curr_path][element_num + 1]
                    elements_list = [curr_before, curr_element, curr_next]
                    elements_list_re = [curr_next, curr_element, curr_before]

                    # check all other paths, if they are using the same path_element
                    for try_from_to in range(0, len(factory.path_list), 1):
                        if breaker:
                            breaker = False
                            break
                        for try_path in range(0, len(factory.path_list[try_from_to].paths), 1):
                            # check if test_path equal with curr_path
                            if from_to == try_from_to and curr_path == try_path:
                                continue

                            # check if element is in test_path
                            try:
                                test_element_num = factory.path_list[try_from_to].paths[try_path].index(curr_element)
                            except ValueError:
                                continue
                            # check if element is first or last of a path
                            if test_element_num == 0 \
                                    or test_element_num == len(factory.path_list[try_from_to].paths[try_path]) - 1:
                                if factory.parameter["crossing_with_source"] == 1:
                                    crossing_points.append(curr_element)
                                    breaker = True
                                    break
                                else:
                                    continue
                            else:
                                # generate second element triple
                                try_before = factory.path_list[try_from_to].paths[try_path][test_element_num - 1]
                                try_element = factory.path_list[try_from_to].paths[try_path][test_element_num]
                                try_next = factory.path_list[try_from_to].paths[try_path][test_element_num + 1]
                                try_list = [try_before, try_element, try_next]
                                # check if crossing or not
                                if elements_list != try_list and elements_list_re != try_list:
                                    crossing_points.append(curr_element)
                                    breaker = True
                                    break
                    # not_crossing_points
                    if curr_element not in crossing_points:
                        not_crossing_points.append(curr_element)
                        # todo: debug

        # iterate over all crossing points / intersections and calc overlapping material flows
        for crossing_point in crossing_points:
            mf_intensity_at_crossing = 0
            mf_counter_at_crossing = 0
            for from_to in range(0, len(factory.path_list), 1):
                for try_path in range(0, len(factory.path_list[from_to].paths), 1):
                    # check if path is used for material flow
                    if factory.mf_matrix[from_to, try_path] > 0:
                        # check if element is in test_path
                        try:
                            try_element_num = factory.path_list[from_to].paths[try_path].index(crossing_point)
                        except ValueError:
                            continue
                        # add material flow intensity and count intersections
                        mf_intensity_at_crossing += factory.mf_matrix[from_to, try_path]
                        mf_counter_at_crossing += 1

            mf_overlapping += mf_intensity_at_crossing * (mf_counter_at_crossing / len(crossing_points))

    else:
        # calc all mf_vectors

        mf_intensity_at_crossing = 0
        vector_list = []
        # get facility
        for facility in range(0, len(factory.facility_list), 1):
            # get facilities from mf_matrix
            for column in range(0, factory.mf_matrix.shape[0], 1):
                # check if connection exists
                if factory.mf_matrix[factory.facility_list[facility].index_num, column] != 0.0 \
                        and factory.facility_list[column].curr_source_position != [None, None] \
                        and factory.facility_list[column].index_num != factory.facility_list[facility].index_num:

                    # calc vector between sink and source
                    vector = [[factory.facility_list[column].curr_sink_position[1],
                               factory.facility_list[column].curr_sink_position[0]],
                              [factory.facility_list[facility].curr_source_position[1],
                               factory.facility_list[facility].curr_source_position[0]],
                              factory.mf_matrix[factory.facility_list[facility].index_num, column]]

                    vector_list.append(vector)

        # calc crossing
        while len(vector_list) > 0:
            vector = vector_list[0]
            vector_list.pop(0)
            for compare_vector in vector_list:
                intersection_point = line_intersection((vector[0], vector[1]), (compare_vector[0], compare_vector[1]))
                if intersection_point != [None, None] and \
                        intersection_point != vector[0] and \
                        intersection_point != vector[1]:
                    mf_intensity_at_crossing += vector[2] + compare_vector[2]

        mf_overlapping = mf_intensity_at_crossing / factory.mf_matrix.mean()

    # write data
    factory.quamfab.mf_overlapping[0] = mf_overlapping
    factory.quamfab.mf_overlapping[1] = None


def line_intersection(line1, line2):
    x_diff = (line1[0][0] - line1[1][0], line2[0][0] - line2[1][0])
    y_diff = (line1[0][1] - line1[1][1], line2[0][1] - line2[1][1])

    def det(a, b):
        return a[0] * b[1] - a[1] * b[0]

    div = det(x_diff, y_diff)
    if div == 0:
        return [None, None]

    d = (det(*line1), det(*line2))
    x = det(d, x_diff) / div
    y = det(d, y_diff) / div
    return [x, y]


def eval_material_flow_constancy(factory):
    """ evaluate material flow constancy """
    constancy_count = 0
    mf_constancy = 0
    # check if path_planning
    if not factory.parameter["no_path_planning"]:
        # start loop over all paths
        for from_facility in range(0, len(factory.path_list), 1):
            # check next path
            for to_facility in range(0, len(factory.path_list[from_facility].paths), 1):
                # if len(factory.path_list[from_facility].paths[to_facility]) != 0:
                if len(factory.path_list[from_facility].paths[to_facility]) > 1:
                    path_element_one = factory.path_list[from_facility].paths[to_facility][0]
                    path_element_two = factory.path_list[from_facility].paths[to_facility][1]
                    # set y_flag: True == expansion into y-direction ; False == expansion into x-direction
                    if path_element_one[0] == path_element_two[0] and path_element_one[1] != path_element_two[1]:
                        y_flag = True
                    elif path_element_one[0] != path_element_two[0] and path_element_one[1] == path_element_two[1]:
                        y_flag = False
                    # reset counter
                    constancy_count = 0
                    # check path for direction change
                    for index in range(1, len(factory.path_list[from_facility].paths[to_facility]) - 1, 1):
                        path_element_one = factory.path_list[from_facility].paths[to_facility][index]
                        path_element_two = factory.path_list[from_facility].paths[to_facility][index + 1]
                        if y_flag and path_element_one[0] != path_element_two[0]:
                            y_flag = False
                            constancy_count += 1
                        elif not y_flag and path_element_one[1] != path_element_two[1]:
                            y_flag = True
                            constancy_count += 1

                    mf_constancy += constancy_count * factory.mf_matrix[from_facility, to_facility]
    else:
        # define unit vector parallel to x-axis
        vector_unit_x = [1, 0]
        # get facility
        for facility in range(0, len(factory.facility_list), 1):
            # get facilities from mf_matrix
            for column in range(0, factory.mf_matrix.shape[0], 1):
                # check if connection exists
                if factory.mf_matrix[factory.facility_list[facility].index_num, column] != 0.0 \
                        and factory.facility_list[column].curr_source_position != [None, None] \
                        and factory.facility_list[column].index_num != factory.facility_list[facility].index_num:

                    # calc vector between sink and source
                    vector = [factory.facility_list[column].curr_source_position[1] -
                              factory.facility_list[facility].curr_sink_position[1],
                              factory.facility_list[column].curr_source_position[0] -
                              factory.facility_list[facility].curr_sink_position[0]]
                    # if vector is in opposite direction -> calc vector in other direction
                    if vector[0] < 0:
                        vector = [factory.facility_list[facility].curr_sink_position[1] -
                                  factory.facility_list[column].curr_source_position[1],
                                  factory.facility_list[facility].curr_sink_position[0] -
                                  factory.facility_list[column].curr_source_position[0]]

                    # calc unit vector
                    vector = vector / np.linalg.norm(vector)

                    # calc cross product
                    rad = np.arccos(np.clip(np.dot(vector, vector_unit_x), -1.0, 1.0))
                    angle = math.degrees(rad)
                    # use adjacent angle if bigger than 45°
                    if angle > 45:
                        angle = 90 - angle

                    mf_constancy += angle * factory.mf_matrix[facility, column]

    # write data
    factory.quamfab.mf_constancy[0] = mf_constancy
    factory.quamfab.mf_constancy[1] = None  # mf_constancy_rating


def eval_area_utilization(factory):
    """ evaluate area utilization """
    empty_space_counter = 0
    for row in factory.layout:
        for cell in row:
            if cell == factory.parameter["empty_cell_value"]:
                empty_space_counter += 1

    # calc rating
    area_utilization_rate = (factory.layout.size - empty_space_counter) / factory.layout.size

    # write data
    factory.quamfab.area_utilization[0] = empty_space_counter
    factory.quamfab.area_utilization[1] = area_utilization_rate

# +++QuaMFaB+++
# todo: Evaluierung Eindeutigkeit
# todo: Evaluierung Zugänglichkeit
