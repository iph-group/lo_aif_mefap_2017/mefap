from scipy.spatial import distance
from mefap.evaluation.pre_calculation import pre_calc_centroid


def eval_direct_communication(factory):
    """ evaluate direct communication multiply cf_distance with cf_intensity """
    cf_distance = 0
    # get facility
    for facility in range(0, len(factory.facility_list), 1):
        # get facilities from mf_matrix
        for column in range(0, factory.cf_matrix.shape[0], 1):
            # check if connection exists
            if factory.cf_matrix[factory.facility_list[facility].index_num, column] != 0.0 \
                    and factory.facility_list[column].curr_source_position != [None, None] \
                    and factory.facility_list[column].index_num != factory.facility_list[facility].index_num:

                if factory.parameter["no_path_planning"]:
                    cf_distance += factory.path_list[facility].paths[column] * factory.cell_size * \
                               factory.cf_matrix[facility, column]
                else:
                    cf_distance += len(factory.path_list[facility].paths[column]) * factory.cell_size * \
                               factory.cf_matrix[facility, column]

    # write data
    factory.quamfab.direct_com[0] = cf_distance
    factory.quamfab.direct_com[1] = None  # direct_com_rating


def eval_formal_communication(factory):
    """ evaluate formal communication by sum distances from meeting room to facilities"""
    # identify facilities with facility_type meeting
    meeting_room_list = []
    meeting_room_distance = 0
    for facility in factory.facility_list:
        if facility.facility_type != "Meeting":
            continue
        else:
            meeting_room_list.append(facility.index_num)

    # calc distance from each facility to each meeting room
    for facility in range(0, len(factory.facility_list), 1):
        centroid_facility = pre_calc_centroid(factory, facility)
        meeting_room_distance_list = []
        for meeting_room in meeting_room_list:
            centroid_meeting_room = pre_calc_centroid(factory, meeting_room)
            meeting_room_distance_list.append(distance.euclidean(centroid_facility, centroid_meeting_room)
                                              * factory.cell_size)
        if not meeting_room_list == []:
            # search shortest distance and add to overall distance
            meeting_room_distance = meeting_room_distance + min(meeting_room_distance_list)

    # write data
    factory.quamfab.formal_com[0] = meeting_room_distance
    factory.quamfab.formal_com[1] = None  # direct_com_rating

# +++QuaMFaB+++
# Evaluierung Informelle Kommunikation
# Evaluierung Transparenz
