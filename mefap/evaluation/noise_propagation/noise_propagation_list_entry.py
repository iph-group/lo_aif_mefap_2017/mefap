import numpy as np


class NoisePropagationEntry:
    def __init__(self, x, y, columns, rows):
        self.cell = [y, x]
        self.layout = np.zeros(shape=(columns, rows))
