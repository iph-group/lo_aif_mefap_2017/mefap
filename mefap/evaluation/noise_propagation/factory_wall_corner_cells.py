import copy


def wall_and_corner_cells(floor_space, noise_area):
    wall_list = []
    corner_list = []

    noise_walls = noise_area.get_all_noise_walls()

    # search for wall_cells
    for y_index in range(0, floor_space.shape[0], 1):
        for x_index in range(0, floor_space.shape[1], 1):
            if y_index == 0 or \
                    x_index == 0 or \
                    y_index == floor_space.shape[0] - 1 or \
                    x_index == floor_space.shape[1] - 1 or \
                    (y_index > 0 and floor_space[y_index - 1, x_index] == -50) or \
                    (y_index < floor_space.shape[0] - 1 and floor_space[y_index + 1, x_index] == -50) or \
                    (x_index > 0 and floor_space[y_index, x_index - 1] == -50) or \
                    (x_index < floor_space.shape[1] - 1 and floor_space[y_index, x_index + 1] == -50) or \
                    (noise_area.is_in_noise_area(x_index - 1, y_index) and not noise_area.is_in_noise_area(x_index, y_index)) or \
                    (noise_area.is_in_noise_area(x_index + 1, y_index) and not noise_area.is_in_noise_area(x_index, y_index)) or \
                    (noise_area.is_in_noise_area(x_index, y_index - 1) and not noise_area.is_in_noise_area(x_index, y_index)) or \
                    (noise_area.is_in_noise_area(x_index, y_index + 1) and not noise_area.is_in_noise_area(x_index, y_index)):
                wall_list.append([y_index, x_index])

    # add all noise_wall cells to wall_list
    for wall_index in noise_walls:
        for wall_cell in range(0, noise_walls[wall_index].shape[0], 1):
            wall_list.append([noise_walls[wall_index][wall_cell, 0], noise_walls[wall_index][wall_cell, 1]])

    # delete duplicates
    cut_list = []
    for cell in wall_list:
        if cell not in cut_list:
            cut_list.append(cell)
    wall_list = copy.deepcopy(cut_list)

    # search for corner_cells
    for y_index in range(0, floor_space.shape[0], 1):
        for x_index in range(0, floor_space.shape[1], 1):
            if (y_index == 0 and x_index == 0) or \
                    (y_index == floor_space.shape[0] - 1 and x_index == 0) or \
                    (y_index == 0 and x_index == floor_space.shape[1] - 1) or \
                    (y_index == floor_space.shape[0] - 1 and x_index == floor_space.shape[1] - 1) or \
                    (y_index > 0 and x_index > 0 and floor_space[y_index - 1, x_index] == -50 and floor_space[y_index, x_index - 1] == -50) or \
                    (y_index > 0 and x_index < floor_space.shape[1] - 1 and floor_space[y_index - 1, x_index] == -50 and floor_space[y_index, x_index + 1] == -50) or \
                    (y_index < floor_space.shape[0] - 1 and x_index > 0 and floor_space[y_index + 1, x_index] == -50 and floor_space[y_index, x_index - 1] == -50) or \
                    (y_index < floor_space.shape[0] - 1 and x_index < floor_space.shape[1] - 1 and floor_space[y_index + 1, x_index] == -50 and floor_space[y_index, x_index + 1] == -50):
                corner_list.append([y_index, x_index])

    # delete wall_cells from wall_list if there are als corner_cells
    for corner_cell in corner_list:
        for wall_cell in wall_list:
            if corner_cell == wall_cell:
                index = wall_list.index(wall_cell)
                wall_list.pop(index)

    for wall in wall_list:
        wall[0] = int(wall[0])
        wall[1] = int(wall[1])

    for corner in corner_list:
        corner[0] = int(corner[0])
        corner[1] = int(corner[1])

    return wall_list, corner_list
