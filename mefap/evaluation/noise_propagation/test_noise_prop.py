from mefap.evaluation.noise_propagation.factory_wall_corner_cells import wall_and_corner_cells
from mefap.evaluation.noise_propagation.calc_noise_propagation import calc_noise_room_condition
from mefap.optimization.initialization import random_layout_generation_by_facility_size
from mefap.evaluation.eval_environmental import re_calc_noise_matrix
from mefap.optimization.positionFacility import position_facility_on_cell
from mefap.evaluation.eval_environmental import eval_noise


def test_noise(factory):

    # setup parameters
    factory.parameter.update({'noise_absorption_coefficient': 0.15})
    factory.parameter.update({'noise_semi_diffuse': False})
    factory.parameter.update({'noise_factory_wall_isolation': 40})
    factory.parameter.update({'noise_barrier_isolation': 15})
    factory.parameter.update({'noise_calc_wall_corner': False})
    factory.parameter.update({'sound_pressure_reduction': 4})
    factory.parameter.update({'sound_propagation_conditions': 8})
    factory.parameter.update({'noise_flat_room': False})

    # set noise_room_condition
    # print(factory.parameter["noise_room_condition"])
    factory.parameter.update({'noise_room_condition': {-1: 999}})
    # print(factory.parameter["noise_room_condition"]['0'])

    # calc room conditions
    if factory.parameter["noise_semi_diffuse"]:
        calc_noise_room_condition(factory)

    # calc noise conditions
    factory.noise_propagation_matrix.calc_noise_propagation_conditions(factory)

    # evaluate noise
    eval_noise(factory)

    # random_layout_generation_by_facility_size(factory)

    position_facility_on_cell(factory, factory.facility_list[11], 12, 12, 0, 0, False)

    position_facility_on_cell(factory, factory.facility_list[12], 12, 2, 0, 0, False)

    re_calc_noise_matrix(factory)

    breakpoint()
