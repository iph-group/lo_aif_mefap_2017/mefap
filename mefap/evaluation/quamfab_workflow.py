from mefap.optimization.positionPath import try_path_planning_for_layout
from mefap.evaluation.evaluate_layout import evaluate_layout_all_objectives


def quamfab_workflow(factory):
    """
    Function for sole evaluation of a given Layout (GUI planed)
    :param factory: all facilities must be placed and also sinks and sources must be reachable
    :return:
    """

    # path planning -> calc paths and write them do layout und path_list
    if not try_path_planning_for_layout(factory, write_path=True):
        print("Failure in: quamfab-workflow: path_planning")

    # evaluate layout
    # todo: implement other quamfab eval functions
    evaluate_layout_all_objectives(factory)
