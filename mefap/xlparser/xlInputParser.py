import numpy as np
import openpyxl

# TODO: remove unused statements
from mefap.factory.facility_attributs import FacilityAttributes
from mefap.factory.facility_characteristics import FacilityCharacteristics
from mefap.factory.media_availability import MediaAvailability
from mefap.factory.media_requirements_import import MediaRequirementsImport
from mefap.factory.path_elements import PathElements


def parse_xl(file_path, factory):
    """ import xl-file, parse sheets, write information in data structure """
    sheet_factory_objects, sheet_media, sheet_material_flow, sheet_communication = open_excel_file(
        file_path)  # , sheet_parameter

    # import facilities and characteristics
    xl_table_start_row = 5
    xl_table_end_row = sheet_factory_objects.max_row + 1
    for i in range(xl_table_start_row, xl_table_end_row, 1):
        if sheet_factory_objects.cell(i, 1).value is None:
            xl_table_end_row = i + 1
            break
    for indexRow in range(xl_table_start_row, xl_table_end_row, 1):
        fac_act = FacilityAttributes(factory.cell_size, sheet_factory_objects=sheet_factory_objects, index=indexRow,
                                    excel_load=True)
        factory.facility_list.append(fac_act)
        if fac_act.facility_type not in [x.name for x in factory.area_types]:
            factory.add_area_type(fac_act.facility_type)
        factory.facility_characteristics.append(
            FacilityCharacteristics(sheet_factory_objects=sheet_factory_objects, index=indexRow, from_excel=True))

    # path_list anlegen
    empty_path = []
    for meta_facility in factory.facility_list:
        factory.path_list.append(PathElements(meta_facility.index_num))
        for sub_facility in factory.facility_list:
            factory.path_list[meta_facility.index_num].paths.append(empty_path)

    # import materialflow and communicationflow
    factory.mf_matrix = np.array(import_matrix(sheet_material_flow))
    factory.cf_matrix = np.array(import_matrix(sheet_communication))

    # import media requirements
    xl_table_start_col = 2
    xl_table_end_col = sheet_media.max_column + 1
    for i in range(xl_table_start_col, xl_table_end_col, 1):
        if sheet_media.cell(1, i).value is None:
            xl_table_end_col = i
            break

    for indexColumn in range(xl_table_start_col, xl_table_end_col, 1):
        start_row = 3
        last_row = sheet_media.max_row + 1
        for i in range(start_row, last_row, 1):
            if sheet_media.cell(i, 1).value is None:
                last_row = i
                break
        help_matrix = []
        for excel_row in range(start_row, last_row, 1):
            if sheet_media.cell(excel_row, indexColumn).value == 1:
                help_value = True
            elif sheet_media.cell(excel_row, indexColumn).value == 0:
                help_value = False
            else:
                # Fehlerabfangen
                help_value = False  # 'FEHLER: Kein binärer Wert in Tabelle'
            help_matrix.append(help_value)

        factory.media_requirements.append(MediaRequirementsImport(sheet_media.cell(1, indexColumn).value, help_matrix))
        factory.media_availability.append(MediaAvailability(sheet_media.cell(1, indexColumn).value, factory))


def open_excel_file(filePath):
    """ TODO: add docstring"""
    # open workbook
    workbook = openpyxl.load_workbook(filePath)
    # open worksheets
    sheet_names = workbook.get_sheet_names()
    sheet_factory_objects = workbook.get_sheet_by_name(sheet_names[0])
    sheet_media = workbook.get_sheet_by_name(sheet_names[1])
    sheet_materialflow = workbook.get_sheet_by_name(sheet_names[2])
    sheet_communication = workbook.get_sheet_by_name(sheet_names[3])
    # sheet_parameter = workbook.sheet_by_name(sheet_names[4])
    return sheet_factory_objects, sheet_media, sheet_materialflow, sheet_communication  # , sheet_parameter


# load Material- und Kommunikationsflussmatrix
def import_matrix(sheet):
    """ TODO: add docstring """
    last_column = sheet.max_column + 1
    for i in range(1,  last_column, 1):
        if sheet.cell(1, i).value is None:
            last_column = i
            break

    last_row = sheet.max_row + 1
    for i in range(1,  last_row, 1):
        if sheet.cell(i, 1).value is None:
            last_row = i
            break

    start_row = 2
    start_col = 2
    help_matrix = []
    for excel_row in range(start_row, last_row, 1):
        help_matrix_row = []
        for excel_column in range(start_col, last_column, 1):
            value = sheet.cell(excel_row, excel_column).value
            if isinstance(value, (int, float)):
                help_matrix_row.append(float(sheet.cell(excel_row, excel_column).value))
            else:
                help_matrix_row.append(0)
        help_matrix.append(help_matrix_row)
    return help_matrix
