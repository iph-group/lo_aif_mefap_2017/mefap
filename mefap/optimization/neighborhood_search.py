import logging
import random
import numpy as np
import copy
from scipy.spatial import distance
import os

from mefap.optimization.searchFreeCell import search_free_areas_road_around
from mefap.optimization.positionFacility import re_position_facility_on_cell
from mefap.optimization.positionFacility import re_position_facility_all_paths
from mefap.optimization.positionFacility import position_facility_all_paths
from mefap.optimization.positionFacility import position_facility_on_cell
from mefap.optimization.deletePath import delete_all_paths
from mefap.optimization.positionPath import try_path_planning_for_layout
from mefap.optimization.deleteFacility import delete_position_facility
from mefap.optimization.searchFreeCell import search_free_areas_on_wall


def local_reallocation_search(factory, reconstruct_path, *args):
    """ the method is based on the LRS from Bock & Hoeberg 2007 """
    # get facility
    if not args:
        while True:
            curr_facility = factory.facility_list[random.randint(0, len(factory.facility_list) - 1)]
            # check if facility is fixed
            if not curr_facility.fixed_position:
                break
    else:
        curr_facility = factory.facility_list[args[0]]
        if curr_facility.fixed_position:
            return True  # todo: geht das schlauer

    y_start_saver = curr_facility.curr_position[0]
    x_start_saver = curr_facility.curr_position[1]
    rotation_saver = curr_facility.curr_rotation
    mirror_saver = curr_facility.curr_mirror
    # y_start = curr_facility.curr_position[0]
    # x_start = curr_facility.curr_position[1]

    # delete paths
    if reconstruct_path:
        delete_all_paths(factory)

    start_tuple = (y_start_saver, x_start_saver, rotation_saver, mirror_saver)
    start_list = [start_tuple]

    for iterations in range(factory.parameter["lrs_search_steps"]):

        while start_tuple in start_list:  # y_start == y_start_saver and x_start == x_start_saver
            # set position
            y_start = y_start_saver + random.randint(-1 * factory.optimization.lrs_step_size,
                                                     factory.optimization.lrs_step_size)
            x_start = x_start_saver + random.randint(-1 * factory.optimization.lrs_step_size,
                                                     factory.optimization.lrs_step_size)
            # set rotation and mirror
            if factory.optimization.rotation:
                rand_rotation = random.choice([0, 1, 2, 3])
            else:
                rand_rotation = 0
            if factory.optimization.mirror:
                rand_mirror = random.choice([0, 1])
            else:
                rand_mirror = 0

            # set tuple values
            start_tuple = (y_start, x_start, rand_rotation, rand_mirror)
        start_list.append(start_tuple)

        if reconstruct_path:
            try:
                re_position_facility_all_paths(factory, curr_facility, x_start, y_start,
                                               rand_rotation, rand_mirror, write_path=True)
                # todo: generate up to 50 feasible positions -> evaluated -> take the best
                return True
            except:
                pass

        else:
            try:
                re_position_facility_on_cell(factory, curr_facility, x_start, y_start,
                                             rand_rotation, rand_mirror, write_path=True)
                # todo: generate up to 50 feasible positions -> evaluated -> take the best
                return True
            except:
                pass

    # LRS isn´t successful -> restore position
    try:
        try_path_planning_for_layout(factory, write_path=True)
    except:
        pass
    return False


def multiple_elements_exchange_search(factory, reconstruct_path, number_of_facilities, *args):
    """
    function for neighborhood search: exchange multiple facilities in layout
    from three elements upwards, these are swapped in a circle
    :param factory:
    :param reconstruct_path:
    :param number_of_facilities:
    :param args: given facilities, that must be included in the search
    :return: search is successful (True) or not (False)
    """
    # get facility
    # if not args
    equality_checker = True
    while equality_checker:
        # set variables
        element_list = []
        possible_elements = []

        # choose first facility
        if not args:
            while True:
                curr_facility = factory.facility_list[random.randint(0, len(factory.facility_list) - 1)]
                # check if facility is fixed
                if not curr_facility.fixed_position:
                    random_facility = curr_facility.index_num
                    break
        else:
            random_facility = args[0]
            if factory.facility_list[random_facility].fixed_position:
                print("Process {}: Failure in MEES: fixed facility in args".format(os.getpid()))
                return False
        # add to element list
        element_list.append(factory.facility_list[random_facility])

        # get possible facilities
        for facility in range(0, len(factory.facility_list), 1):
            if facility == element_list[0].index_num:
                continue
            if (element_list[0].area - (factory.optimization.mees_area_difference * element_list[0].area) <=
                factory.facility_list[facility].area <=
                element_list[0].area + (factory.optimization.mees_area_difference * element_list[0].area)) and \
                    not factory.facility_list[facility].fixed_position:
                # add to possible elements list
                possible_elements.append(facility)

        if len(possible_elements) < number_of_facilities - 1:
            print("Process {}: Failure in MEES: size of facility is not matching".format(os.getpid()))
            return False

        # choose next facilities
        for iteration in range(1, number_of_facilities, 1):
            if not args or len(args) - 1 < iteration:
                random_number = random.randint(0, len(possible_elements) - 1)
                random_facility = possible_elements[random_number]
                possible_elements.pop(random_number)
            else:
                random_facility = args[iteration]
                if factory.facility_list[random_facility].fixed_position:
                    print("Failure in MEES: fixed facility in args")
                    return False
                elif random_facility not in possible_elements:
                    print("Failure in MEES: size of facilities in args not suitable")
                    return False
                else:
                    if random_facility in possible_elements:
                        random_number = possible_elements.index(random_facility)
                        possible_elements.pop(random_number)
                    else:
                        fail = 1

            # set to elements_list
            element_list.append(factory.facility_list[random_facility])

        else:
            equality_checker = False

    # save facilities data and DELETE facility
    y_start_saver = []
    x_start_saver = []
    rotation_saver = []
    mirror_saver = []
    for element_index in range(0, len(element_list), 1):
        # save position data
        y_start_saver.append(copy.deepcopy(element_list[element_index].curr_position[0]))
        x_start_saver.append(copy.deepcopy(element_list[element_index].curr_position[1]))
        rotation_saver.append(copy.deepcopy(element_list[element_index].curr_rotation))
        mirror_saver.append(copy.deepcopy(element_list[element_index].curr_mirror))
        # delete facility
        delete_position_facility(factory, element_list[element_index])

    # reallocated facilities
    for count_element in range(0, len(element_list), 1):
        # element is shifted to the position of the "next" element
        if count_element < len(element_list) - 1:
            to_element = count_element + 1
        else:
            to_element = 0

        # delete paths
        if reconstruct_path:
            delete_all_paths(factory)

        # avoid that facility is positioned on old place
        start_tuple = (y_start_saver[count_element], x_start_saver[count_element], rotation_saver[count_element],
                       mirror_saver[count_element])
        start_list = [start_tuple]

        # try to reallocate
        position_bool = False
        iterations = 0
        while not position_bool and iterations < factory.parameter["lrs_search_steps"]:
            iterations += 1
            while start_tuple in start_list:  # y_start == y_start_saver and x_start == x_start_saver
                # set position
                y_start = y_start_saver[to_element] + random.randint(-1 * factory.optimization.mees_step_size,
                                                                     factory.optimization.mees_step_size)
                x_start = x_start_saver[to_element] + random.randint(-1 * factory.optimization.mees_step_size,
                                                                     factory.optimization.mees_step_size)
                # set rotation and mirror
                if factory.optimization.rotation:
                    rand_rotation = random.choice([0, 1, 2, 3])
                else:
                    rand_rotation = 0
                if factory.optimization.mirror:
                    rand_mirror = random.choice([0, 1])
                else:
                    rand_mirror = 0

                # set tuple
                start_tuple = (y_start, x_start, rand_rotation, rand_mirror)
            start_list.append(start_tuple)

            if reconstruct_path:
                try:
                    position_facility_all_paths(factory, element_list[count_element], x_start, y_start,
                                               rand_rotation, rand_mirror, write_path=False)
                    position_bool = True
                except:
                    pass

            else:
                try:
                    position_facility_on_cell(factory, element_list[count_element], x_start, y_start,
                                             rand_rotation, rand_mirror, write_path=False)
                    position_bool = True
                except:
                    pass
        if iterations >= 100:
            break
    else:
        if not try_path_planning_for_layout(factory, write_path=True):
            print("Process {}: Failure in: MEES path planning".format(os.getpid()))
        return True

    for element_index in range(0, len(element_list), 1):
        # delete facility
        if element_list[element_index].curr_position != [None, None]:
            delete_position_facility(factory, element_list[element_index])
    for element_index in range(0, len(element_list), 1):
        try:
            position_facility_on_cell(factory, element_list[element_index], x_start_saver[element_index],
                                         y_start_saver[element_index], rotation_saver[element_index],
                                         mirror_saver[element_index], write_path=False)
        except:
            print("Process {}: Error in: re_position_facility_on_cell_paths".format(os.getpid()))
            return False

    try:
        try_path_planning_for_layout(factory, write_path=True)
    except:
        pass
    return False


def free_area_search(factory, reconstruct_path, area_order, *args, **kwargs):
    """
    find all free areas the chosen/given facility will fit in
    :param factory:
    :param reconstruct_path: True = delete all paths for the search | False = paths remain during search
    :param area_order: True = facility size (small to large) | False = position in layout
    :param args: given facilities, that must be included in the search
    :return: search is successful (True) or not (False)
    """
    # get facility
    if not args:
        while True:
            curr_facility = factory.facility_list[random.randint(0, len(factory.facility_list) - 1)]
            # check if facility is fixed
            if not curr_facility.fixed_position:
                break
    else:
        curr_facility = factory.facility_list[args[0]]
        if curr_facility.fixed_position:
            return True

    # delete paths
    if reconstruct_path:
        delete_all_paths(factory)

    # search free areas in layout
    path_width = 0  # before =2; ToDo: Update for new path-facility-definition
    free_areas_list = []
    possible_areas = []

    if not curr_facility.on_wall:
        free_areas_list = search_free_areas_road_around(factory.layout)
    else:
        free_areas_list = search_free_areas_on_wall(factory.layout, curr_facility.cell_width1, curr_facility.cell_width2)

    for free_area in range(0, len(free_areas_list), 1):
        if free_areas_list[free_area].y_shape >= curr_facility.cell_width1 + path_width \
                and free_areas_list[free_area].x_shape >= curr_facility.cell_width2 + path_width:
            possible_areas.append((free_area, free_areas_list[free_area].area))
        elif free_areas_list[free_area].y_shape >= curr_facility.cell_width2 + path_width \
                and free_areas_list[free_area].x_shape >= curr_facility.cell_width1 + path_width:
            possible_areas.append((free_area, free_areas_list[free_area].area))
    if possible_areas:
        # todo: order free areas based on distance to current position
        if area_order:
            # sort areas by size
            possible_areas.sort(key=lambda tup: tup[1])
        else:
            # GA_LC calc euclidean distance from areas position to coordinate origin
            for index in range(len(possible_areas)):
                euclidean = distance.euclidean([0, 0], [free_areas_list[possible_areas[index][0]].y_start_position,
                                               free_areas_list[possible_areas[index][0]].x_start_position])
                # overwrite index
                possible_areas[index] = (possible_areas[index][0], euclidean)

            # sort areas by euclidean distance
            possible_areas.sort(key=lambda tup: tup[1])

        # set rotation and mirror
        if factory.optimization.rotation:
            rotation_range = [0, 1, 2, 3]
            random.shuffle(rotation_range)
            if kwargs:
                facility_rotation = kwargs["old_rotation"]
            else:
                facility_rotation = curr_facility.curr_rotation
            facility_rotation_index = rotation_range.index(facility_rotation)
            rotation_range.pop(facility_rotation_index)
            rotation_range.insert(0, facility_rotation)
        else:
            rotation_range = [0]

        if factory.optimization.mirror:
            mirror_range = [0, 1]
            if curr_facility.curr_mirror == 1:
                mirror_range.sort(reverse=True)
        else:
            mirror_range = [0]

        # try to reposition facility
        for area in range(len(possible_areas)):
            for mirror in mirror_range:
                for rotation in rotation_range:
                    if reconstruct_path:
                        try:
                            re_position_facility_all_paths(factory, curr_facility,
                                                           free_areas_list[possible_areas[area][0]].x_start_position,
                                                           free_areas_list[possible_areas[area][0]].y_start_position,
                                                           rotation, mirror, write_path=True)
                            return True
                        except:
                            pass
                    elif not reconstruct_path and not area_order:
                        # used for ga_lc
                        try:
                            position_facility_on_cell(factory, curr_facility,
                                                      free_areas_list[possible_areas[area][0]].x_start_position,
                                                      free_areas_list[possible_areas[area][0]].y_start_position,
                                                      rotation, mirror, True, True)
                            return True
                        except:
                            pass
                    else:
                        try:
                            re_position_facility_on_cell(factory, curr_facility,
                                                         free_areas_list[possible_areas[area][0]].x_start_position,
                                                         free_areas_list[possible_areas[area][0]].y_start_position,
                                                         rotation, mirror, write_path=True)
                            return True
                        except:
                            pass

        # help_list_area is empty
        # positioning isn´t successful
        if reconstruct_path:
            try:
                try_path_planning_for_layout(factory, write_path=True)
            except:
                pass
        # else:
            # print("Process {}: Failure in: free_area_search: reconstruct path = False".format(os.getpid()))
        return False
