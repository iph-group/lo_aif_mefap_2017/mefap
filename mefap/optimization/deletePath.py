

def delete_paths_from_path_list(factory, curr_facility):
    """ deletes paths connected to a specific facility from path_list """
    for curr_path in range(0, len(factory.path_list[curr_facility.index_num].paths), 1):
        # delete / reset complete path from path_list
        factory.path_list[curr_facility.index_num].paths[curr_path] = []

    for from_to_element in range(0, len(factory.path_list), 1):
        # delete / reset complete path from path_list
        factory.path_list[from_to_element].paths[curr_facility.index_num] = []


def delete_paths_from_layout(factory, curr_facility):
    """ deletes paths connected to a specific facility from layout AND path_list """
    # check if path_planning = True
    if not factory.parameter["no_path_planning"]:
        # detect path
        for curr_path in range(0, len(factory.path_list[curr_facility.index_num].paths), 1):
            # detect path_element
            for path_element in factory.path_list[curr_facility.index_num].paths[curr_path]:
                # check if path_element in another path
                if try_delete_path_element(factory, path_element, curr_facility.index_num):
                    factory.layout[path_element] = factory.parameter["empty_cell_value"]
            factory.path_list[curr_facility.index_num].paths[curr_path] = []

        # detect curr_path
        for from_to_element in range(0, len(factory.path_list), 1):
            # detect path_element
            for path_element in factory.path_list[from_to_element].paths[curr_facility.index_num]:
                # check if path_element in another path
                if try_delete_path_element(factory, path_element, curr_facility.index_num):
                    factory.layout[path_element] = factory.parameter["empty_cell_value"]
            factory.path_list[from_to_element].paths[curr_facility.index_num] = []
    else:
        # if path_planning = False
        delete_paths_from_path_list(factory, curr_facility)


def try_delete_path_element(factory, path_element, facility_index):
    """ checks if cell is also used as path_element by another facility / path """
    if factory.layout[path_element] == factory.parameter["fixed_road_value"]:
        return False

    for from_to_element in range(0, len(factory.path_list), 1):
        for try_path in range(0, len(factory.path_list[from_to_element].paths), 1):
            for try_path_element in factory.path_list[from_to_element].paths[try_path]:
                if try_path_element == path_element and facility_index != from_to_element and \
                        facility_index != try_path and \
                        factory.layout[try_path_element] != factory.parameter["fixed_road_value"]:
                    return False
    return True


def delete_all_paths(factory):
    """ deletes all paths from layout AND path_list """
    for facility_element in factory.facility_list:
        # check if path_planning = True
        if not factory.parameter["no_path_planning"]:
            # detect curr_path
            for curr_path in range(0, len(factory.path_list[facility_element.index_num].paths), 1):
                # detect path_element
                for path_element in factory.path_list[facility_element.index_num].paths[curr_path]:
                    if factory.layout[path_element] != factory.parameter["fixed_road_value"]:
                        factory.layout[path_element] = factory.parameter["empty_cell_value"]
                factory.path_list[facility_element.index_num].paths[curr_path] = []

            # detect curr_path
            for from_to_element in range(0, len(factory.path_list), 1):
                # detect path_element
                for path_element in factory.path_list[from_to_element].paths[facility_element.index_num]:
                    if factory.layout[path_element] != factory.parameter["fixed_road_value"]:
                        factory.layout[path_element] = factory.parameter["empty_cell_value"]
                factory.path_list[from_to_element].paths[facility_element.index_num] = []
        else:
            # path_planning = False
            delete_paths_from_path_list(factory,facility_element)


def try_delete_fixed_road_cell(factory, road_cell):
    """
    check if, fixed_road_cell can be deleted:
    true: delete and set 'empty_cell_value'
    false: set 'local_road_value'
    :param factory:
    :param road_cell:
    :return:
    """
    for from_to_element in range(0, len(factory.path_list), 1):
        for try_path in range(0, len(factory.path_list[from_to_element].paths), 1):
            for try_path_element in factory.path_list[from_to_element].paths[try_path]:
                if try_path_element == road_cell:
                    factory.layout[road_cell] = factory.parameter["local_road_value"]
                    return False
    else:
        factory.layout[road_cell] = factory.parameter["empty_cell_value"]
    return True
