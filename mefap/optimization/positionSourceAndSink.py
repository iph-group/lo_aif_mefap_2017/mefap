

# Methode zum Positionieren von Quelle und Senke
# ToDo: Muss noch implementiert werden
def do_position_source_sink(curr_facility, rotation, mirror):
    curr_facility.curr_source_position, curr_facility.curr_sink_position = \
        calculate_position_source_sink(curr_facility, curr_facility.curr_position[1], curr_facility.curr_position[0],
                                       rotation, mirror)


def calculate_position_source_sink(curr_facility, x_start, y_start, rotation, mirror):
    facility_width1 = curr_facility.cell_width1 - 1
    facility_width2 = curr_facility.cell_width2 - 1
    for iteration in range(0, 2, 1):
        if iteration == 0:
            help_position = curr_facility.source_position
            origin_sequence_factor = curr_facility.source_sequence_factor
        elif iteration == 1:
            help_position = curr_facility.sink_position
            origin_sequence_factor = curr_facility.sink_sequence_factor
        # add new rotation to current sourcePos or sinkPos
        new_sequence_factor = origin_sequence_factor + rotation
        # calculate new sourcePos or sinkPos
        if origin_sequence_factor == 0:
            if new_sequence_factor == 0 or new_sequence_factor == 4:
                if mirror == 0:
                    help_pos = [y_start + help_position[0],
                                x_start]
                else:
                    help_pos = [y_start + help_position[0],
                                x_start + facility_width1]
            elif new_sequence_factor == 1 or new_sequence_factor == 5:
                if mirror == 0:
                    help_pos = [y_start,
                                x_start + facility_width2 - help_position[0]]
                else:
                    help_pos = [y_start,
                                x_start + help_position[0]]
            elif new_sequence_factor == 2 or new_sequence_factor == 6:
                if mirror == 0:
                    help_pos = [y_start + facility_width2 - help_position[0],
                                x_start + facility_width1]
                else:
                    help_pos = [y_start + facility_width2 - help_position[0],
                                x_start]
            elif new_sequence_factor == 3:
                if mirror == 0:
                    help_pos = [y_start + facility_width1,
                                x_start + help_position[0]]
                else:
                    help_pos = [y_start + facility_width1,
                                x_start + facility_width2 - help_position[0]]
        elif origin_sequence_factor == 1:
            if new_sequence_factor == 0 or new_sequence_factor == 4:
                if mirror:
                    help_pos = [y_start + facility_width1 - help_position[1],
                                x_start]
                else:
                    help_pos = [y_start + facility_width1 - help_position[1],
                                x_start + facility_width2]
            elif new_sequence_factor == 1 or new_sequence_factor == 5:
                if mirror == 0:
                    help_pos = [y_start,
                                x_start + help_position[1]]
                else:
                    help_pos = [y_start,
                                x_start + facility_width1 - help_position[1]]
            elif new_sequence_factor == 2 or new_sequence_factor == 6:
                if mirror == 0:
                    help_pos = [y_start + help_position[1],
                                x_start + facility_width2]
                else:
                    help_pos = [y_start + help_position[1],
                                x_start]
            elif new_sequence_factor == 3:
                if mirror == 0:
                    help_pos = [y_start + facility_width2,
                                x_start + facility_width1 - help_position[1]]
                else:
                    help_pos = [y_start + facility_width2,
                                x_start + help_position[1]]
        elif origin_sequence_factor == 2:
            if new_sequence_factor == 0 or new_sequence_factor == 4:
                if mirror == 0:
                    help_pos = [y_start + facility_width2 - help_position[0],
                                x_start]
                else:
                    help_pos = [y_start + facility_width2 - help_position[0],
                                x_start + facility_width1]
            elif new_sequence_factor == 1 or new_sequence_factor == 5:
                if mirror == 0:
                    help_pos = [y_start,
                                x_start + help_position[0]]
                else:
                    help_pos = [y_start,
                                x_start + facility_width2 - help_position[0]]
            elif new_sequence_factor == 2 or new_sequence_factor == 6:
                if mirror == 0:
                    help_pos = [y_start + help_position[0],
                                x_start + facility_width1]
                else:
                    help_pos = [y_start + help_position[0],
                                x_start]
            elif new_sequence_factor == 3:
                if mirror == 0:
                    help_pos = [y_start + facility_width1,
                                x_start + facility_width2 - help_position[0]]
                else:
                    help_pos = [y_start + facility_width1,
                                x_start + help_position[0]]
        elif origin_sequence_factor == 3:
            if new_sequence_factor == 0 or new_sequence_factor == 4:
                if mirror:
                    help_pos = [y_start + help_position[1],
                                x_start]
                else:
                    help_pos = [y_start + help_position[1],
                                x_start + facility_width2]
            elif new_sequence_factor == 1 or new_sequence_factor == 5:
                if mirror == 0:
                    help_pos = [y_start,
                                x_start + facility_width1 - help_position[1]]
                else:
                    help_pos = [y_start,
                                x_start + help_position[1]]
            elif new_sequence_factor == 2 or new_sequence_factor == 6:
                if mirror == 0:
                    help_pos = [y_start + facility_width1 - help_position[1],
                                x_start + facility_width2]
                else:
                    help_pos = [y_start + facility_width1 - help_position[1],
                                x_start]
            elif new_sequence_factor == 3:
                if mirror == 0:
                    help_pos = [y_start + facility_width2,
                                x_start + help_position[1]]
                else:
                    help_pos = [y_start + facility_width2,
                                x_start + facility_width1 - help_position[1]]
        if iteration == 0:
            source_pos = help_pos
        elif iteration == 1:
            sink_pos = help_pos
    return source_pos, sink_pos
