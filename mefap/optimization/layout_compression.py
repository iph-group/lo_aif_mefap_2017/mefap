import os

import numpy as np

from mefap.optimization.positionFacility import position_facility_next_to
from mefap.optimization.positionFacility import re_position_facility_on_cell
from mefap.optimization.deletePath import delete_all_paths
from mefap.optimization.positionPath import try_path_planning_for_layout


def compress_layout(factory):
    """ compress layout into left up corner """
    # delete paths from layout
    delete_all_paths(factory)

    up_facilities_pos = sort_facility_list_up_pos(factory.facility_list)
    left_facilities_pos = sort_facility_list_left_pos(factory.facility_list)
    x_start = 0
    y_start = 0

    breaker = 0
    while breaker < 2:
        if not re_position_facility_on_cell(factory, factory.facility_list[up_facilities_pos[0]], x_start, y_start,
                                            factory.facility_list[up_facilities_pos[0]].curr_rotation,
                                            factory.facility_list[up_facilities_pos[0]].curr_mirror,
                                            write_path=False):
            if re_position_facility_on_cell(factory, factory.facility_list[left_facilities_pos[0]], x_start, y_start,
                                            factory.facility_list[left_facilities_pos[0]].curr_rotation,
                                            factory.facility_list[left_facilities_pos[0]].curr_mirror,
                                            write_path=False):
                up_facilities_pos = sort_facility_list_up_pos(factory.facility_list)
                left_facilities_pos = sort_facility_list_left_pos(factory.facility_list)
        breaker += 1


    # todo: zwei Iterationen ?
    breaker = 0
    while breaker < 1:
        # Search in x-Direction
        up_facilities_pos = sort_facility_list_up_pos(factory.facility_list)
        for index_pos_list in range(0, len(up_facilities_pos) - 1, 1):
            if factory.facility_list[up_facilities_pos[index_pos_list]].curr_position[1] < \
                    factory.facility_list[up_facilities_pos[index_pos_list + 1]].curr_position[1]:
                direction = 'right'
                position_facility_next_to(factory,
                                          factory.facility_list[up_facilities_pos[index_pos_list]],
                                          factory.facility_list[up_facilities_pos[index_pos_list + 1]],
                                          direction,
                                          factory.facility_list[up_facilities_pos[index_pos_list + 1]].curr_rotation,
                                          factory.facility_list[up_facilities_pos[index_pos_list + 1]].curr_mirror,
                                          write_path=False)
            else:
                x_start = 0
                re_position_facility_on_cell(factory, factory.facility_list[up_facilities_pos[index_pos_list + 1]],
                                             x_start,
                                             factory.facility_list[up_facilities_pos[index_pos_list + 1]].curr_position[0],
                                             factory.facility_list[up_facilities_pos[index_pos_list + 1]].curr_rotation,
                                             factory.facility_list[up_facilities_pos[index_pos_list + 1]].curr_mirror,
                                             write_path=False)

        # Search in y-Direction
        left_facilities_pos = sort_facility_list_left_pos(factory.facility_list)
        for index_pos_list in range(0, len(left_facilities_pos) - 1, 1):
            if factory.facility_list[left_facilities_pos[index_pos_list]].curr_position[1] < \
                    factory.facility_list[left_facilities_pos[index_pos_list + 1]].curr_position[1]:
                direction = 'down'
                position_facility_next_to(factory,
                                          factory.facility_list[left_facilities_pos[index_pos_list]],
                                          factory.facility_list[left_facilities_pos[index_pos_list + 1]],
                                          direction,
                                          factory.facility_list[up_facilities_pos[index_pos_list + 1]].curr_rotation,
                                          factory.facility_list[up_facilities_pos[index_pos_list + 1]].curr_mirror,
                                          write_path=False)
            else:
                y_start = 0
                re_position_facility_on_cell(factory, factory.facility_list[up_facilities_pos[index_pos_list + 1]],
                                             factory.facility_list[up_facilities_pos[index_pos_list + 1]].curr_position[1],
                                             y_start,
                                             factory.facility_list[up_facilities_pos[index_pos_list + 1]].curr_rotation,
                                             factory.facility_list[up_facilities_pos[index_pos_list + 1]].curr_mirror,
                                             write_path=False)

    breaker += 1

    # plan all paths
    try:
        try_path_planning_for_layout(factory, write_path=True)
    except:
        print("Process {}: Error in: layout compression".format(os.getpid()))


def sort_facility_list_up_pos(facility_list):
    help_list_1 = []
    help_list_2 = []
    for index in range(0, len(facility_list), 1):
        help_list_1.append(facility_list[index].curr_position[0])
        help_list_2.append(facility_list[index].curr_position[1])
    pos_list = np.lexsort((help_list_2, help_list_1))
    return pos_list


def sort_facility_list_left_pos(facility_list):
    help_list_1 = []
    help_list_2 = []
    for index in range(0, len(facility_list), 1):
        help_list_1.append(facility_list[index].curr_position[0])
        help_list_2.append(facility_list[index].curr_position[1])
    pos_list = np.lexsort((help_list_1, help_list_2))
    return pos_list


def compress_paths(factory):
    # delete all paths
    delete_all_paths(factory)

    # plan all paths
    try:
        try_path_planning_for_layout(factory, write_path=True)
    except:
        print("Process {}: Error in: path compression".format(os.getpid()))
