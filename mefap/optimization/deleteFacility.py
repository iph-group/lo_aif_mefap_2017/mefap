# from mefap.optimization.positionFacility import calc_width_on_rotation


def delete_position_facility(factory, curr_facility):
    if not curr_facility.fixed_position:
        # todo: y_facility_width, x_facility_width = calc_width_on_rotation(curr_facility, curr_facility.curr_rotation)
        if curr_facility.curr_rotation == 0 or curr_facility.curr_rotation == 2:
            y_facility_width = curr_facility.cell_width2
            x_facility_width = curr_facility.cell_width1
        elif curr_facility.curr_rotation == 1 or curr_facility.curr_rotation == 3:
            y_facility_width = curr_facility.cell_width1
            x_facility_width = curr_facility.cell_width2
        for y_index in range(0, y_facility_width, 1):
            for x_index in range(0, x_facility_width, 1):
                factory.layout[curr_facility.curr_position[0] + y_index, curr_facility.curr_position[1] + x_index] \
                    = factory.parameter["empty_cell_value"]
        curr_facility.curr_position = [None, None]
        curr_facility.curr_rotation = None
        curr_facility.curr_mirror = None
        curr_facility.curr_source_position = [None, None]
        curr_facility.curr_sink_position = [None, None]


def delete_position_of_facility_on_cell(factory, curr_facility, x_start_cell, y_start_cell, rotation):
    """ Methode zum löschen der Positionierung einer Facility im Layout """
    if not curr_facility.fixed_position:
        # todo: y_facility_width, x_facility_width = calc_width_on_rotation(curr_facility, rotation)
        if rotation == 0 or rotation == 2:
            y_facility_width = curr_facility.cell_width2
            x_facility_width = curr_facility.cell_width1
        elif rotation == 1 or rotation == 3:
            y_facility_width = curr_facility.cell_width1
            x_facility_width = curr_facility.cell_width2
        for y_index in range(0, y_facility_width, 1):
            for x_index in range(0, x_facility_width, 1):
                factory.layout[y_start_cell + y_index, x_start_cell + x_index] = factory.parameter["empty_cell_value"]
                # ToDo: Löscht richtig?
        curr_facility.curr_position = [None, None]
        curr_facility.curr_rotation = None
        curr_facility.curr_mirror = None


def delete_all_facilities(factory):
    for facility_element in factory.facility_list:
        # delete facility from layout and reset position data
        if facility_element.curr_position != [None, None] and not facility_element.fixed_position:
            delete_position_facility(factory, facility_element)
