# MeFaP
Institution: IPH - Institut für Integrierte Produktion Hannover gGmbH

Autoren: Paul Aurich, Marc Speckmann

Förderhinweis: 
Das IGF-Vorhaben 19666 N der Bundesvereinigung Logistik (BVL) e.V. wurde über die AiF im Rahmen des Programms zur Förderung der Industriellen Gemeinschaftsforschung (IGF) vom Bundesministerium für Wirtschaft und Energie (BMWi) aufgrund eines Beschlusses des Deutschen Bundestages gefördert.

Website: mefap.iph-hannover.de

## Translation

For Updating all translations just run the bash script translate_project
ATTENTION: When new folders are added to the project, they must also be added in the script.
